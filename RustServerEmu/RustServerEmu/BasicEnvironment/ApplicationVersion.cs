using System;
using System.Diagnostics;

namespace RustServerEmu.BasicEnvironment
{
    public class ApplicationVersion
    {
        static ApplicationVersion()
        {
            FileVersionInfo fileVersionInfo = FileVersionInfo.GetVersionInfo(Process.GetCurrentProcess().MainModule.FileName);
            string[] splited = fileVersionInfo.ProductVersion.Split('-');
            if (splited.Length >= 3)
            {
                ApplicationVersion.Version = splited[0];
                ApplicationVersion.Branch = splited[2];
                ApplicationVersion.VersionTime = UnixTimeStampToDateTime(long.Parse(splited[1]));
            }
        }

        public static string Version { get; }
        public static string Branch { get; }
        public static DateTime VersionTime { get; }
        
        static DateTime UnixTimeStampToDateTime( long unixTimeStamp )
        {
            System.DateTime dtDateTime = new DateTime(1970,1,1,0,0,0,0,System.DateTimeKind.Utc);
            dtDateTime = dtDateTime.AddSeconds( unixTimeStamp ).ToLocalTime();
            return dtDateTime;
        }

        public static void ShowHeaderInConsole()
        {
            Console.WriteLine("####################");
            Console.WriteLine("# Name: Rust Server Emu");
            Console.WriteLine("# Version: " + ApplicationVersion.Version);
            Console.WriteLine("# Branch: " + ApplicationVersion.Branch);
            Console.WriteLine("# Date: " + ApplicationVersion.VersionTime);
            Console.WriteLine("####################");
            Console.Title = "Application - Version: " + ApplicationVersion.Version + "; Branch: " + ApplicationVersion.Branch;
        }
    }
}