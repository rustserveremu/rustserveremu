using Facepunch;
using Network;
using ProtoBuf;
using RustServerEmu.AppEnvironment;
using RustServerEmu.Data;
using RustServerEmu.Extension;
using RustServerEmu.GObject.GameObject.HeldObject;
using RustServerEmu.Struct;
using UnityEngine;
using Debug = Tefor.Engine.Debug;

namespace RustServerEmu.GObject.GameObject
{
    public class BaseEntity : BaseNetworkable
    {
        public Vector3 Position { get; private set; } = Vector3.zero;
        public Vector3 Rotation { get; private set; } = Vector3.zero;
        public EEntityFlags Flags { get; set; } = 0;

        
        public override void SendMeTo(object target = null)
        {
            if (target == null)
                target = this.Net.ConnectionOwner;
            if (target.IsValidTarget() == false)
                return;
            using (Entity entity = Pool.Get<Entity>())
            {
                entity.baseNetworkable = Pool.Get<ProtoBuf.BaseNetworkable>();
                entity.baseNetworkable.uid = this.Net.UID;
                entity.baseNetworkable.prefabID = this.Net.PrefabID;
                entity.baseNetworkable.group = 0;
                
                if (this.Net.ParentUID != 0)
                {
                    entity.parent = Pool.Get<ParentInfo>();
                    entity.parent.uid = this.Net.ParentUID;
                    entity.parent.bone = this.Net.ParentBone;
                }
                
                entity.baseEntity = Pool.Get<ProtoBuf.BaseEntity>();
                entity.baseEntity.pos = this.Position;
                entity.baseEntity.rot = this.Rotation;
                entity.baseEntity.flags = (int)this.Flags;
                target.SendMessage_Entities(entity);
            }
        }

        public virtual T CreateCropse<T>(uint prefabID) where T : BaseCorpse
        {
            T result = NetworkManager.Instance.Create<T>();
            result.Net.PrefabID = prefabID;
            result.SetPositionAndRotation(this.Position, this.Rotation, false);
            return result;
        }

        public override void OnRPCMessage(Message message, ERPCMessage rpcMethod)
        {
            switch (rpcMethod)
            {
                case ERPCMessage.BaseEntity_BroadcastSignalFromClient:
                    this.OnRPCMessage_BroadcastSignalFromClient(message);
                    break;
                default:
                    base.OnRPCMessage(message, rpcMethod);
                    break;
            }
        }

        public virtual void OnRPCMessage_BroadcastSignalFromClient(Message message)
        {
            ESignal signal = (ESignal)message.read.Int32();
//            Debug.LogDebug($"BroadcastSignalFromClient in {this.GetType().Name}[{this.Net.UID}] - " + signal);
            string arg = message.read.String();
            this.SendMessage_RPCMessage(new SendInfo(this.Net.ListViewToMe)
            {
                method = SendMethod.Unreliable,
                priority = Priority.Immediate
            }, message.connection, ERPCMessage.BaseEntity_SignalFromServerEx, (int)signal, arg);
        }

        public virtual void SetPositionAndRotation(Vector3 pos, Vector3 rot, bool networkUpdate = true)
        {
            this.Position = pos;
            this.Rotation = rot;
            if (networkUpdate)
                this.SendMePositionTo(this.Net.ListViewToMe);
        }

        public virtual void SendMePositionTo(object target = null)
        {
            (target ?? this.Net.ConnectionOwner).SendMessage_Position(this);
        }

        
        public virtual void OnAttacked(HitInfo hitInfo)
        {
        }
    }
}