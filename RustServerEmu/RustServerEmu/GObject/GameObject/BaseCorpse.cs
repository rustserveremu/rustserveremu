using Facepunch;
using ProtoBuf;
using RustServerEmu.Extension;

namespace RustServerEmu.GObject.GameObject
{
    public class BaseCorpse : BaseCombatEntity
    {
        public uint ParentEntityUID { get; set; } = 0;

        public override void SendMeTo(object target = null)
        {
            if (target == null)
                target = this.Net.ConnectionOwner;
            if (target.IsValidTarget() == false)
                return;
            using (Entity entity = Pool.Get<Entity>())
            {
                entity.baseNetworkable = Pool.Get<ProtoBuf.BaseNetworkable>();
                entity.baseNetworkable.uid = this.Net.UID;
                entity.baseNetworkable.prefabID = this.Net.PrefabID;
                entity.baseNetworkable.group = 0;
                
                if (this.Net.ParentUID != 0)
                {
                    entity.parent = Pool.Get<ParentInfo>();
                    entity.parent.uid = this.Net.ParentUID;
                    entity.parent.bone = this.Net.ParentBone;
                }
                
                entity.baseEntity = Pool.Get<ProtoBuf.BaseEntity>();
                entity.baseEntity.pos = this.Position;
                entity.baseEntity.rot = this.Rotation;
                entity.baseEntity.flags = (int)this.Flags;
                entity.baseEntity.time = 20000;

                entity.baseCombat = Tefor.Engine.Pool.Get<BaseCombat>();
                entity.baseCombat.health = this.Health;
                entity.baseCombat.state = (int)this.LifeState;

                entity.corpse = Pool.Get<Corpse>();
                entity.corpse.parentID = this.ParentEntityUID;
                
                target.SendMessage_Entities(entity);
            }
        }
    }
}