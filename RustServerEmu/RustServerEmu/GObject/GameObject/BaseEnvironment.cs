using System;
using ProtoBuf;
using RustServerEmu.AppEnvironment;
using RustServerEmu.Data;
using RustServerEmu.Database;
using RustServerEmu.Extension;
using Tefor.Engine;
using Environment = ProtoBuf.Environment;
using Pool = Facepunch.Pool;

namespace RustServerEmu.GObject.GameObject
{
    public class BaseEnvironment : BaseEntity
    {
        public static BaseEnvironment InstanceEnvironment { get; private set; }

        public Environment CurrentData { get; } = Pool.Get<Environment>();
        public DateTime CurrentTime = DateTime.Now;
        private float TimerPerSecond = 0f;

        public override void Awake()
        {
            base.Awake();
            InstanceEnvironment = this;
            this.Net.PrefabID = (uint)EPrefabID.BaseEnvironment;
            int startHour = GamePlayDefinition.Instance.Configuration.DatetimeStartHour;
            if (startHour > 23)
            {
                startHour = 23;
                Debug.LogWarning("[BaseEnvironment::Awake] Detected start hour > 23 in GamePlay Configuration");
            }

            this.CurrentTime = new DateTime(2000, 01, 01, startHour, 0, 0);
            this.CurrentData.dateTime = this.CurrentTime.ToBinary();
        }

        void OnFixedUpdate(float deltaTime)
        {
            this.TimerPerSecond += deltaTime;
            if (this.TimerPerSecond >= 3f)
            {
                this.TimerPerSecond = 0f;
                this.CurrentTime = this.CurrentTime.AddSeconds((3f * GamePlayDefinition.Instance.Configuration.DatetimeScale));
                this.CurrentData.dateTime = this.CurrentTime.ToBinary();
                this.SendMeTo(this.Net.ListViewToMe);
            }
        }

        public override void OnDespawn()
        {
            base.OnDespawn();
            InstanceEnvironment = null;
            this.CurrentData.Dispose();
        }

        public void SetCurrentHour(int hour)
        {
            if (hour > 23)
                hour = 23;
            this.CurrentTime = new DateTime(this.CurrentTime.Year, this.CurrentTime.Month, this.CurrentTime.Day, hour, 0, 0);
        }

        public override void SendMeTo(object target = null)
        {
            if (target == null)
                target = this.Net.ConnectionOwner;
            if (target.IsValidTarget() == false)
                return;
            using (Entity entity = Pool.Get<Entity>())
            {
                entity.baseNetworkable = Pool.Get<ProtoBuf.BaseNetworkable>();
                entity.baseNetworkable.uid = this.Net.UID;
                entity.baseNetworkable.prefabID = this.Net.PrefabID;
                entity.baseNetworkable.group = 0;
                
                if (this.Net.ParentUID != 0)
                {
                    entity.parent = Pool.Get<ParentInfo>();
                    entity.parent.uid = this.Net.ParentUID;
                    entity.parent.bone = this.Net.ParentBone;
                }
                
                entity.baseEntity = Pool.Get<ProtoBuf.BaseEntity>();
                entity.baseEntity.pos = this.Position;
                entity.baseEntity.rot = this.Rotation;
                entity.baseEntity.flags = (int)this.Flags;
                entity.environment = this.CurrentData;
                target.SendMessage_Entities(entity);
            }
        }
    }
}