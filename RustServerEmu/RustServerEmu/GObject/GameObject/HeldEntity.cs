using System;
using ProtoBuf;
using RustServerEmu.Data;
using RustServerEmu.Extension;
using Tefor.Engine;
using BaseProjectile = RustServerEmu.GObject.GameObject.HeldObject.BaseProjectile;
using Item = RustServerEmu.GObject.Component.Item;
using Pool = Facepunch.Pool;

namespace RustServerEmu.GObject.GameObject
{
    public class HeldEntity : BaseEntity
    {
        public Item ParentItem { get; private set; }
        public BasePlayer ParentPlayer => this.ParentItem?.ParentContainer?.PlayerOwner;
        public uint HeldIDInPlayer { get; private set; } 

        public virtual void SetParentItem(Item item)
        {
            if (this.Net.PrefabID == 0)
            {
                if (item.HeldEntity != null)
                {
                    this.Net.PrefabID = item.ItemDefinition.HeldDefinition.PrefabID;
                    this.Net.ParentBone = item.ItemDefinition.HeldDefinition.ParentBone;
                    this.ParentItem = item;
                    this.HeldIDInPlayer = this.ParentItem.UID;
                }
                else
                    Debug.LogError("[HeldEntity::SetParentItem]: You parent item is not have heldEntity!");
            } else
                throw new Exception("[HeldEntity::SetParentItem]: You has already initialized this held entity!");
        }

        public override void Awake()
        {
            base.Awake();
            this.SetHeldState(false, false);
        }

        public void SetHeldState(bool state, bool networkUpdate = true)
        {
            this.SetFlag(EEntityFlags.Reserved4, state == true, false, false);
            this.SetFlag(EEntityFlags.Disabled, state == false, false, networkUpdate);
        }
        
        public override void SendMeTo(object target = null)
        {
            if (target == null)
                target = this.Net.ConnectionOwner;
            if (target.IsValidTarget() == false)
                return;
            using (Entity entity = Pool.Get<Entity>())
            {
                entity.baseNetworkable = Pool.Get<ProtoBuf.BaseNetworkable>();
                entity.baseNetworkable.uid = this.Net.UID;
                entity.baseNetworkable.prefabID = this.Net.PrefabID;
                entity.baseNetworkable.group = 0;
                
                entity.baseEntity = Pool.Get<ProtoBuf.BaseEntity>();
                entity.baseEntity.pos = this.Position;
                entity.baseEntity.rot = this.Rotation;
                entity.baseEntity.flags = (int)this.Flags;
                
                entity.parent = Pool.Get<ParentInfo>();
                entity.parent.bone = this.Net.ParentBone;
                entity.parent.uid = this.ParentItem?.ParentContainer?.PlayerOwner?.Net.UID ?? 0;

                entity.heldEntity = Pool.Get<ProtoBuf.HeldEntity>();
                entity.heldEntity.itemUID = this.ParentItem.UID;
                
                target.SendMessage_Entities(entity);
            }
        }
        
        public virtual void ServerCommand(string text, GameObject.BasePlayer player)
        {
            
        }
    }
}