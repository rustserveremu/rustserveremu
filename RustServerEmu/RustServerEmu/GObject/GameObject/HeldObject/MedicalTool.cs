using System;
using System.Collections.Generic;
using System.Reflection;
using Network;
using RustServerEmu.AppEnvironment;
using RustServerEmu.Data;
using RustServerEmu.Database;
using RustServerEmu.Extension;
using UnityEngine;

namespace RustServerEmu.GObject.GameObject.HeldObject
{
    public class MedicalTool : AttackEntity
    {
        public override void OnRPCMessage(Message message, ERPCMessage rpcMethod)
        {
            switch (rpcMethod)
            {
                case ERPCMessage.MedicalTool_UseSelf:
                    this.OnRPCMessage_UseSelf(message);
                    break;
                case ERPCMessage.MedicalTool_UseOther:
                    this.OnRPCMessage_UseOther(message);
                    break;
                default:
                    base.OnRPCMessage(message, rpcMethod);
                    break;
            }
        }

        protected void OnRPCMessage_UseSelf(Message message)
        {
            this.UseTo(this.ParentPlayer);
        }

        protected void OnRPCMessage_UseOther(Message message)
        {
            uint playerID = message.read.UInt32();
            if (NetworkManager.Instance.ListSpawnedNetworkables.TryGetValue(playerID, out BaseNetworkable networkable) && networkable is BasePlayer targetPlayer)
            {
                if (Vector3.Distance(targetPlayer.Position, this.ParentPlayer.Position) < 4f)
                    this.UseTo(targetPlayer);
            }
        }

        public void UseTo(BasePlayer playerTarget)
        {
            if (this.ParentPlayer.Net.ConnectionOwner != null && this.ParentPlayer.Net.ConnectionOwner.connected == true)
                this.ParentPlayer.SendMessage_RPCMessage(new SendInfo(this.ParentPlayer.Net.ConnectionOwner), null, ERPCMessage.MedicalTool_Reset);

            if (this.ParentItem.ItemDefinition.HeldDefinition.MedicalDefinition.CanRevive && playerTarget.HasPlayerFlag(EPlayerFlags.Wounded))
                playerTarget.SetPlayerFlag(EPlayerFlags.Wounded, false);
            
            this.ParentItem.OnItemCMD_Consume("consume", playerTarget);
        }

        public override void ServerCommand(string text, BasePlayer player)
        {
            this.UseTo(player);
        }
    }
}