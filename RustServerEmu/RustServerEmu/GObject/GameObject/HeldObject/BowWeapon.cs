using Network;
using RustServerEmu.Data;
using RustServerEmu.Database;

namespace RustServerEmu.GObject.GameObject.HeldObject
{
    public class BowWeapon : BaseProjectile
    {
        public override void OnRPCMessage(Message message, ERPCMessage rpcMethod)
        {
            switch (rpcMethod)
            {
                case ERPCMessage.BowWeapon_BowReload:
                    this.OnRPCMessage_BowReload(message);
                    break;
                default:
                    base.OnRPCMessage(message, rpcMethod);
                    break;
            }
        }

        protected void OnRPCMessage_BowReload(Message message)
        {
            if (this.AmountAmmoMagazine == 0)
            {
                this.OnRPCMessage_StartReload(message);
                base.OnRPCMessage_Reload(message);
            }
        }
    }
}