using Network;
using ProtoBuf;
using RustServerEmu.AppEnvironment;
using RustServerEmu.Data;
using RustServerEmu.Extension;
using RustServerEmu.Extension.ProtoBuf;
using RustServerEmu.Struct;
using UnityEngine;
using Item = RustServerEmu.GObject.Component.Item;

namespace RustServerEmu.GObject.GameObject.HeldObject
{
    public class BaseMelee : AttackEntity
    {
        public override void OnRPCMessage(Message message, ERPCMessage rpcMethod)
        {
            switch (rpcMethod)
            {
                case ERPCMessage.HeldEntity_PlayerAttack:
                    this.OnRPCMessage_PlayerAttack(message);
                    break;
                case ERPCMessage.HeldEntity_CLProject:
                    this.OnRPCMessage_CLProject(message);
                    break;
                default:
                    base.OnRPCMessage(message, rpcMethod);
                    break;
            }
        }

        protected virtual void OnRPCMessage_CLProject(Message message)
        {
            BasePlayer playerOwner = message.connection.ToPlayer();
            using (ProjectileShoot projectileShoot = ProjectileShoot.Deserialize(message.read))
            {
                playerOwner.OnCleanOldProjectile();
                uint projectileObjectPrefabID = this.ParentItem.ItemDefinition.ItemComponentDefinitions?.Projectile?.ProjectileObjectPrefabID ?? 0;
                for (var i = 0; i < projectileShoot.projectiles.Count; i++)
                {
                    playerOwner.OnFiredProjectile(projectileShoot.projectiles[i], this, null);
                    if (projectileObjectPrefabID != 0)
                    {
                        Effect effect = Effect.Instance;
                        effect.InitFromPrefabID(projectileObjectPrefabID, EEffectType.Projectile, projectileShoot.projectiles[i].startPos, projectileShoot.projectiles[i].startVel, message.connection.userid);
                        effect.scale = 1f;
                        effect.number = projectileShoot.projectiles[i].seed;
                        NetworkManager.Instance.ListConnections.SendMessage_Effect(effect);
                    }
                }

                this.ParentItem.ParentContainer.RemoveItem(this.ParentItem);
            }
        }

        protected virtual void OnRPCMessage_PlayerAttack(Message message)
        {
            BasePlayer playerOwner = message.connection.ToPlayer();
            if (playerOwner != null)
            {
                using (PlayerAttack playerAttack = PlayerAttack.Deserialize(message.read))
                {
                    if (playerAttack != null)
                    {
                        HitInfo hitInfo = new HitInfo();
                        hitInfo.LoadFromAttack(playerAttack.attack, true);
                        hitInfo.Initiator = playerOwner;
                        hitInfo.Weapon = this;
                        hitInfo.Predicted = message.connection;
                        hitInfo.DamageAmount = 0;
                        // TODO: Need save melee damage to field and reuse field
                        if (ParentItem.ItemDefinition.HeldDefinition.MeleeDefinition?.DamageTypes != null)
                        {
                            for (int i = 0; i < this.ParentItem.ItemDefinition.HeldDefinition.MeleeDefinition.DamageTypes.Count; i++)
                            {
                                if (hitInfo.DamageAmount == 0)
                                    hitInfo.DamageType = this.ParentItem.ItemDefinition.HeldDefinition.MeleeDefinition.DamageTypes[i].Type;
                                hitInfo.DamageAmount = this.ParentItem.ItemDefinition.HeldDefinition.MeleeDefinition.DamageTypes[i].Damage;
                            }
                        }
                        this.DoAttackShared(hitInfo);
                    }
                }
            }
        }

        public virtual void DoAttackShared(HitInfo hitInfo)
        {
            hitInfo.HitEntity?.OnAttacked(hitInfo);
            if (hitInfo.DoHitEffects == true)
            {
                // TODO: Need release hit effect
            }
            this.UpdateItemCondition(hitInfo);
        }
        
        public void UpdateItemCondition(HitInfo info)
        {
            Item item = this.ParentItem;
            if (item == null && item.ItemDefinition.HeldDefinition.MeleeDefinition?.DamageTypes != null)
               return;
            if (info != null && info.DidHit && info.DidGather == false)
            {
                float num = this.GetConditionLoss();
                float num2 = 0f;
                foreach (var damageType in item.ItemDefinition.HeldDefinition.MeleeDefinition?.DamageTypes)
                {
                    if (damageType.Damage > 0f)
                        num2 += Mathf.Clamp(damageType.Damage - info.DamageAmount, 0f, damageType.Damage);
                }
                num += num2 * 0.2f;
                item.LoseCondition(num);
            }
        }
        
        public virtual float GetConditionLoss()
        {
            return 1f;
        }
    }
}