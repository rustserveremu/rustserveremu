using Network;
using ProtoBuf;
using RustServerEmu.AppEnvironment;
using RustServerEmu.Data;
using RustServerEmu.Extension;
using RustServerEmu.Extension.DefaultCode;
using UnityEngine;
using Debug = Tefor.Engine.Debug;

namespace RustServerEmu.GObject.GameObject.HeldObject
{
	public class Planner : HeldEntity
	{
		public override void OnRPCMessage(Message message, ERPCMessage rpcMethod)
		{
			switch (rpcMethod)
			{
				case ERPCMessage.Planner_DoPlace:
					this.OnRPCMessage_DoPlace(message);
					break;
				default:
					base.OnRPCMessage(message, rpcMethod);
					break;
			}
		}

		protected virtual void OnRPCMessage_DoPlace(Message message)
		{
			using (CreateBuilding createBuilding = CreateBuilding.Deserialize(message.read))
			{
				this.DoBuild(createBuilding);
			}
		}

		protected virtual void DoBuild(CreateBuilding createBuilding)
		{
			// TODO: This is demo code Build, need full rewrite...
			
			BasePlayer ownerPlayer = this.ParentPlayer;
			if (ownerPlayer == null)
				return;
			
			Vector3 position = Vector3.zero;
			Quaternion rotation = Quaternion.identity;

			if (createBuilding.entity == 0 && createBuilding.socket == 0)
			{
				Vector3 eulerAngles = Vector3.zero;
				eulerAngles.x = 0f;
				eulerAngles.z = 0f;
				Vector3 direction = createBuilding.ray.direction;
				direction.y = 0f;
				direction.Normalize();
				Vector3 upwards = createBuilding.normal;
				rotation = QuaternionEx.LookRotationEx(direction, upwards) * QuaternionEx.ToQuaternion(0f, eulerAngles.y, 0f) * createBuilding.rotation.ToQuaternion();
				position = createBuilding.position;
				position -= rotation * Vector3.zero;
			}

			if (createBuilding.entity != 0 && createBuilding.socket == 0)
			{
				// TODO: This code is bad work... Need rewrite
				BaseEntity targetEntity = null;
				if (NetworkManager.Instance.ListSpawnedNetworkables.TryGetValue(createBuilding.entity, out BaseNetworkable networkable))
					targetEntity = (networkable as BaseEntity);
				if (targetEntity == null)
					return;
				
				Vector3 eulerAngles = Vector3.zero;
				eulerAngles.x = 0f;
				eulerAngles.z = 0f;
				Vector3 direction = createBuilding.ray.direction;
				direction.y = 0f;
				direction.Normalize();
				Vector3 upwards = createBuilding.normal;
				rotation = QuaternionEx.LookRotationEx(direction, upwards) * QuaternionEx.ToQuaternion(0f, eulerAngles.y, 0f) * createBuilding.rotation.ToQuaternion();
				position = createBuilding.position;
				position -= rotation * Vector3.zero;
				position = targetEntity.Position - position;
			}

			BuildingBlock block = NetworkManager.Instance.Create<BuildingBlock>();
			block.SetPositionAndRotation(position, rotation.ToVector3(), false);
			block.Net.PrefabID = createBuilding.blockID;
			block.Spawn();

			if (this.ParentItem.ItemDefinition.ItemComponentDefinitions.Deployable != null)
			{
				if (this.ParentItem.Amount > 1)
				{
					this.ParentItem.Amount--;
					this.ParentItem.OnContainerChanged();
				} else
					this.ParentItem.Remove();
			}
		}
	}
}