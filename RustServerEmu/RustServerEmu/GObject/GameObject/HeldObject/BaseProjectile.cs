using System.Linq;
using Facepunch;
using Network;
using ProtoBuf;
using RustServerEmu.AppEnvironment;
using RustServerEmu.Data;
using RustServerEmu.Database;
using RustServerEmu.Extension;
using RustServerEmu.Extension.ProtoBuf;
using RustServerEmu.GObject.Component;
using RustServerEmu.Handlers.ChatCommands;
using UnityEngine;
using Item = RustServerEmu.GObject.Component.Item;

namespace RustServerEmu.GObject.GameObject.HeldObject
{
    public class BaseProjectile : AttackEntity
    {
        public int AmountAmmoMagazine { get; set; } = 0;
        public ItemDefinition ItemDefinitionInMagazine { get; set; } = null;
        public ItemDefinition.MagazineInfo MagazineDefinition { get; private set; } = null;

        public override void SetParentItem(Item item)
        {
            base.SetParentItem(item);
            this.MagazineDefinition = item.ItemDefinition.HeldDefinition?.RangeDefinition?.MagazineDefinition;
            if (this.MagazineDefinition != null)
            {
                this.AmountAmmoMagazine = this.MagazineDefinition.Contents;
                if (ItemDefinition.ListItems.TryGetValue(this.MagazineDefinition.ItemID, out ItemDefinition itemDefaultInMagazine))
                    this.ItemDefinitionInMagazine = itemDefaultInMagazine;
            }
        }

        public override void SendMeTo(object target = null)
        {
            if (target == null)
                target = this.Net.ConnectionOwner;
            if (target.IsValidTarget() == false)
                return;
            using (Entity entity = Pool.Get<Entity>())
            {
                entity.baseNetworkable = Pool.Get<ProtoBuf.BaseNetworkable>();
                entity.baseNetworkable.uid = this.Net.UID;
                entity.baseNetworkable.prefabID = this.Net.PrefabID;
                entity.baseNetworkable.group = 0;

                entity.baseEntity = Pool.Get<ProtoBuf.BaseEntity>();
                entity.baseEntity.pos = this.Position;
                entity.baseEntity.rot = this.Rotation;
                entity.baseEntity.flags = (int) this.Flags;

                entity.parent = Pool.Get<ParentInfo>();
                entity.parent.bone = this.Net.ParentBone;
                entity.parent.uid = this.ParentItem?.ParentContainer?.PlayerOwner?.Net.UID ?? 0;

                entity.heldEntity = Pool.Get<ProtoBuf.HeldEntity>();
                entity.heldEntity.itemUID = this.ParentItem.UID;

                entity.baseProjectile = Pool.Get<ProtoBuf.BaseProjectile>();
                entity.baseProjectile.primaryMagazine = Pool.Get<Magazine>();
                entity.baseProjectile.primaryMagazine.capacity = this.MagazineDefinition?.Capacity ?? 0;
                entity.baseProjectile.primaryMagazine.ammoType = this.ItemDefinitionInMagazine?.ItemID ?? 0;
                entity.baseProjectile.primaryMagazine.contents = this.AmountAmmoMagazine;
                
                target.SendMessage_Entities(entity);
            }
        }

        public override void OnRPCMessage(Message message, ERPCMessage rpcMethod)
        {
            switch (rpcMethod)
            {
                case ERPCMessage.HeldEntity_CLProject:
                    this.OnRPCMessage_CLProject(message);
                    break;
                case ERPCMessage.BaseProjectile_StartReload:
                    this.OnRPCMessage_StartReload(message);
                    break;
                case ERPCMessage.BaseProjectile_Reload:
                    this.OnRPCMessage_Reload(message);
                    break;
                case ERPCMessage.BaseProjectile_SwitchAmmoTo:
                    this.OnRPCMessage_SwitchAmmoTo(message);
                    break;
                case ERPCMessage.BaseProjectile_ServerFractionalReloadInsert:
                    // TODO: Need release slow reload bolt
                    break;
                default:
                    base.OnRPCMessage(message, rpcMethod);
                    break;
            }
        }

        protected virtual void OnRPCMessage_CLProject(Message message)
        {
            BasePlayer playerOwner = message.connection.ToPlayer();
            if (this.AmountAmmoMagazine > 0)
            {
                ItemDefinition ammoType = this.ItemDefinitionInMagazine;
                using (ProjectileShoot projectileShoot = ProjectileShoot.Deserialize(message.read))
                {
                    this.AmountAmmoMagazine--;
                    //Broadcast
                    this.SendMessage_RPCMessage(new SendInfo(NetworkManager.Instance.ListConnections)
                    {
                        method = SendMethod.Unreliable,
                        priority = Priority.Immediate
                    }, message.connection, ERPCMessage.BaseEntity_SignalFromServerEx, (int)ESignal.Attack, "");

                    playerOwner.OnCleanOldProjectile();
                    uint ProjectileObjectPrefabID = this.ItemDefinitionInMagazine?.ItemComponentDefinitions?.Projectile?.ProjectileObjectPrefabID ?? 0;
                    for (var i = 0; i < projectileShoot.projectiles.Count; i++)
                    {
                        playerOwner.OnFiredProjectile(projectileShoot.projectiles[i], this, this.ItemDefinitionInMagazine);
                        if (ProjectileObjectPrefabID != 0)
                            this.CreateProjectileEffectClientside(ProjectileObjectPrefabID, projectileShoot.projectiles[i].startPos, projectileShoot.projectiles[i].startVel, projectileShoot.projectiles[i].seed, message.connection, false, false);
                    }

                    this.UpdateItemCondition();
                }
            }
        }
        
        private void CreateProjectileEffectClientside(uint ProjectilePrefabID, UnityEngine.Vector3 pos, UnityEngine.Vector3 velocity, int seed, Connection sourceConnection, bool silenced = false, bool forceClientsideEffects = false)
        {
            Effect effect = Effect.Instance;
            effect.Clear();
            effect.InitFromPrefabID(ProjectilePrefabID, EEffectType.Projectile, pos, velocity, sourceConnection?.userid ?? 0);
            effect.scale = (silenced ? 0f : 1f);
            if (forceClientsideEffects)
                effect.scale = 2f;
            effect.number = seed;
            NetworkManager.Instance.ListConnections.SendMessage_Effect(effect);
        }

        protected virtual void OnRPCMessage_StartReload(Message message)
        {
            int countCurrentAmmo = this.AmountAmmoMagazine + this.ParentPlayer.Inventory.GetCountAmmoFromItemDefinition(this.ItemDefinitionInMagazine);
            if (countCurrentAmmo <= 0)
            {
                ItemDefinition newItem = this.ParentPlayer.Inventory.GetItemDefinitionAmmo(this.MagazineDefinition.AmmoType);
                if (newItem != null)
                    this.ItemDefinitionInMagazine = newItem;
            }
        }

        protected virtual void OnRPCMessage_Reload(Message message)
        {
            int amountReload = this.AmountAmmoMagazine;
            amountReload = this.ParentPlayer.Inventory.UseAmountFromItemDefinition(this.ItemDefinitionInMagazine, this.MagazineDefinition.Capacity - amountReload) + amountReload;
            this.AmountAmmoMagazine = amountReload;
            this.SendMeTo(this.ParentPlayer.Net.ConnectionOwner);
            this.ParentPlayer.SendMeTo(this.ParentPlayer.Net.ConnectionOwner);
        }

        protected virtual void OnRPCMessage_SwitchAmmoTo(Message message)
        {
            int itemID = message.read.Int32();
            if (itemID == this.ItemDefinitionInMagazine.ItemID)
                return;
            
            Item ammoOld = Item.Create(this.ItemDefinitionInMagazine, this.AmountAmmoMagazine);
            this.ParentPlayer.Inventory.GiveItem(ammoOld);
            this.AmountAmmoMagazine = 0;
            if (ItemDefinition.ListItems.TryGetValue(itemID, out ItemDefinition newAmmoDefinition))
                this.ItemDefinitionInMagazine = newAmmoDefinition;
            this.SendMeTo(this.ParentPlayer.Net.ConnectionOwner);
            this.ParentPlayer.SendMeTo(this.ParentPlayer.Net.ConnectionOwner);
        }
        
        private void UpdateItemCondition()
        {
            float num = 0.25f;
            this.ParentItem.LoseCondition(num + 1); // TODO: 1?! No! Need parse "barrelConditionLoss" in ItemModProjectile in current bullet
            if (this.HasFlag(EEntityFlags.Reserved4))
                this.SendMeTo(this.ParentPlayer?.Net.ConnectionOwner);
        }
    }
}