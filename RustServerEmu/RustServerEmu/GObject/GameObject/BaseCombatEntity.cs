using Facepunch;
using Network;
using ProtoBuf;
using RustServerEmu.AppEnvironment;
using RustServerEmu.Data;
using RustServerEmu.Database;
using RustServerEmu.Extension;
using RustServerEmu.GObject.GameObject.HeldObject;
using RustServerEmu.Struct;
using UnityEngine;

namespace RustServerEmu.GObject.GameObject
{
    public class BaseCombatEntity : BaseEntity
    {
        public float MaxHealth { get; set; } = 100f;
        public float Health { get; set; } = 100f;
        public EBaseCombatLifeState LifeState => (this.Health <= 0f ? EBaseCombatLifeState.Dead : EBaseCombatLifeState.Alive);

        public virtual void Hurt(HitInfo hitInfo)
        {
            if (this.LifeState == EBaseCombatLifeState.Dead)
                return;
            
            if (hitInfo.HitBone != 0 && BoneDefinition.ListBones.TryGetValue(hitInfo.HitBone, out BoneDefinition boneDefinition))
                hitInfo.DamageAmount *= boneDefinition.damageScale;
            
            if (hitInfo.DamageAmount < this.Health)
            {
                this.Health -= hitInfo.DamageAmount;
                if (this.Net.ConnectionOwner?.connected == true)
                    this.SendMeTo(this.Net.ConnectionOwner);
            }
            else
            {
                this.Health = 0f;
                this.OnDie(hitInfo);
            }
        }
        
        public virtual void Hurt(float damage, BaseEntity owner = null, EDamageType damageType = EDamageType.Generic, Vector3 damagePosition = default(Vector3))
        {
            this.Hurt(new HitInfo(owner, this, damageType, damage, damagePosition));
        }

        public override void OnAttacked(HitInfo hitInfo)
        {
            if (this.LifeState == EBaseCombatLifeState.Alive)
                this.DoHitNotify(hitInfo);
            this.Hurt(hitInfo);
        }
        
        public void DoHitNotify(HitInfo info)
        {
            if (info.Initiator != null && info.Initiator is BasePlayer && info.isHeadshot == false && this != info.Initiator)
            {
                bool flag = info.Weapon is BaseMelee;
                bool arg = info.Initiator.Net.ConnectionOwner == info.Predicted;
                this.SendMessage_RPCMessage(new SendInfo(info.Initiator.Net.ConnectionOwner), null, ERPCMessage.BaseCombatEntity_HitNotify, arg);
            }
        }

        public virtual void OnDie(HitInfo hitInfo = null)
        {
            this.OnKilled(hitInfo);
        }

        public virtual void OnKilled(HitInfo hitInfo = null)
        {
            NetworkManager.Instance.Despawn(this);
        }
        
        public override void SendMeTo(object target = null)
        {
            if (target == null)
                target = this.Net.ConnectionOwner;
            if (target.IsValidTarget() == false)
                return;
            using (Entity entity = Pool.Get<Entity>())
            {
                entity.baseNetworkable = Pool.Get<ProtoBuf.BaseNetworkable>();
                entity.baseNetworkable.uid = this.Net.UID;
                entity.baseNetworkable.prefabID = this.Net.PrefabID;
                entity.baseNetworkable.group = 0;
                
                if (this.Net.ParentUID != 0)
                {
                    entity.parent = Pool.Get<ParentInfo>();
                    entity.parent.uid = this.Net.ParentUID;
                    entity.parent.bone = this.Net.ParentBone;
                }
                
                entity.baseEntity = Pool.Get<ProtoBuf.BaseEntity>();
                entity.baseEntity.pos = this.Position;
                entity.baseEntity.rot = this.Rotation;
                entity.baseEntity.flags = (int)this.Flags;

                entity.baseCombat = Tefor.Engine.Pool.Get<BaseCombat>();
                entity.baseCombat.health = this.Health;
                entity.baseCombat.state = (int)this.LifeState;
                target.SendMessage_Entities(entity);
            }
        }
    }
}