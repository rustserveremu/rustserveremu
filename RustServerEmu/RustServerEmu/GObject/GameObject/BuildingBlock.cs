using Facepunch;
using ProtoBuf;
using RustServerEmu.Data;
using RustServerEmu.Extension;
using RustServerEmu.Struct;
using UnityEngine;

namespace RustServerEmu.GObject.GameObject
{
    public class BuildingBlock : BaseCombatEntity
    {
        public EBuildingGrade Grade { get; set; } = EBuildingGrade.Twigs;
        public float Stability = 100f;

        public override void SendMeTo(object target = null)
        {
            if (target == null)
                target = this.Net.ConnectionOwner;
            if (target.IsValidTarget() == false)
                return;
            
            using (Entity entity = Pool.Get<Entity>())
            {
                entity.baseNetworkable = Pool.Get<ProtoBuf.BaseNetworkable>();
                entity.baseNetworkable.uid = this.Net.UID;
                entity.baseNetworkable.prefabID = this.Net.PrefabID;
                entity.baseNetworkable.group = 0;
                
                if (this.Net.ParentUID != 0)
                {
                    entity.parent = Pool.Get<ParentInfo>();
                    entity.parent.uid = this.Net.ParentUID;
                    entity.parent.bone = this.Net.ParentBone;
                }
                
                entity.baseEntity = Pool.Get<ProtoBuf.BaseEntity>();
                entity.baseEntity.pos = this.Position;
                entity.baseEntity.rot = this.Rotation;
                entity.baseEntity.flags = (int)this.Flags;

                entity.baseCombat = Pool.Get<BaseCombat>();
                entity.baseCombat.health = this.Health;
                entity.baseCombat.state = (int)this.LifeState;

                entity.stabilityEntity = Pool.Get<StabilityEntity>();
                entity.stabilityEntity.stability = 100f;
                entity.stabilityEntity.distanceFromGround = 3;

                entity.decayEntity = Pool.Get<DecayEntity>();
                entity.decayEntity.buildingID = this.Net.PrefabID;

                entity.buildingBlock = Pool.Get<ProtoBuf.BuildingBlock>();
                entity.buildingBlock.grade = (int) this.Grade;
                entity.buildingBlock.model = 0;
                
                
                target.SendMessage_Entities(entity);
            }
        }

        public override void OnAttacked(HitInfo hitInfo)
        {
            hitInfo.DamageAmount = 1;
            this.Hurt(hitInfo);
        }

        public override void Hurt(HitInfo hitInfo)
        {
            base.Hurt(hitInfo);
            if (this.LifeState== EBaseCombatLifeState.Alive)
                this.SendMeTo(this.Net.ListViewToMe);
        }
    }
}