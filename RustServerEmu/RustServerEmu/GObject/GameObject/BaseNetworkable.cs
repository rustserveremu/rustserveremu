using System;
using System.Collections;
using System.Collections.Generic;
using Network;
using ProtoBuf;
using RustServerEmu.AppEnvironment;
using RustServerEmu.Data;
using RustServerEmu.Extension;
using RustServerEmu.GObject.Component.BaseNetworkable;
using Tefor.Engine;

namespace RustServerEmu.GObject.GameObject
{
    public class BaseNetworkable : BaseType
    {
        public NetworkInfromation Net { get; } = new NetworkInfromation();

        public virtual void SetConnectionOwner(Connection connection)
        {
            this.Net.ConnectionOwner = connection;
        }
        
        public virtual void SendMeTo(object target = null)
        {
            if (target == null)
                target = this.Net.ConnectionOwner;
            if (target.IsValidTarget() == false)
                return;
            using (Entity entity = Pool.Get<Entity>())
            {
                entity.baseNetworkable = Pool.Get<ProtoBuf.BaseNetworkable>();
                entity.baseNetworkable.uid = this.Net.UID;
                entity.baseNetworkable.prefabID = this.Net.PrefabID;
                entity.baseNetworkable.group = 0;
                
                if (this.Net.ParentUID != 0)
                {
                    entity.parent = Pool.Get<ParentInfo>();
                    entity.parent.uid = this.Net.ParentUID;
                    entity.parent.bone = this.Net.ParentBone;
                }
                target.SendMessage_Entities(entity);
            }
        }

        public virtual void OnRPCMessage(Message message, ERPCMessage rpcMethod)
        {
            switch (rpcMethod)
            {
                default:
                    Debug.LogError($"Connection [{message.connection}] call rpc method from unknown rpcMethod [{rpcMethod}]");
                    break;
            }
        }

        public virtual void Awake()
        {
            
        }
        
        public virtual void Spawn()
        {
            NetworkManager.Instance.ListSpawnedNetworkables.Add(this.Net.UID, this);
            this.SubscribeAllToMe();
        }

        public virtual void OnDespawn()
        {
            this.UnsubscribeAllToMe();
        }

        void OnAwake() => this.Awake();
        void OnDestroy() => this.OnDespawn();
    }
}