using System;
using System.Collections.Generic;
using System.Linq;
using Network;
using ProtoBuf;
using RustServerEmu.AppEnvironment;
using RustServerEmu.BasicEnvironment;
using RustServerEmu.Data;
using RustServerEmu.Database;
using RustServerEmu.Extension;
using RustServerEmu.Extension.DefaultCode;
using RustServerEmu.Extension.ProtoBuf;
using RustServerEmu.GObject.Component;
using RustServerEmu.GObject.GameObject.HeldObject;
using RustServerEmu.Struct;
using Tefor.Engine;
using UnityEngine;
using BaseProjectile = RustServerEmu.GObject.GameObject.HeldObject.BaseProjectile;
using Debug = Tefor.Engine.Debug;
using Item = RustServerEmu.GObject.Component.Item;
using PlayerInventory = RustServerEmu.GObject.Component.BasePlayer.PlayerInventory;
using Pool = Facepunch.Pool;

namespace RustServerEmu.GObject.GameObject
{
    public class BasePlayer : BaseCombatEntity
    {
        public static Dictionary<ulong, BasePlayer> ListAnyBasePlayers { get; } = new Dictionary<ulong, BasePlayer>();
        public static Dictionary<Connection, BasePlayer> ListActivePlayers { get; } = new Dictionary<Connection, BasePlayer>();
        
        public string DisplayName { get; set; }
        public ulong SteamID { get; set; }
        public Vector3 EyePosition { get; private set; }
        public EPlayerModalState ModelStateFlags { get; set; } = EPlayerModalState.Sleeping;
        public EPlayerFlags PlayerFlags { get; set; } = EPlayerFlags.Sleeping;
        public PlayerInventory Inventory { get; private set; }
        public HeldEntity ActiveHeldEntity { get; private set; } = null;
        private PlayerTick LastPlayerTick { get; set; } = new PlayerTick();
        public Dictionary<int,FiredProjectile> ListFiredProjectile = new Dictionary<int,FiredProjectile>();

        public float LostTimeWounded { get; set; } = 40f;
        public DateTime LastWoundedTime { get; set; } = DateTime.MinValue;
        private HitInfo lastHitInfoFromPlayer { get; set; } = null;

        #region [Constructor] Awake
        public override void Awake()
        {
            this.Inventory = new PlayerInventory(this);
            this.Net.PrefabID = (uint)EPrefabID.BasePlayer;
            this.SetPositionAndRotation(GamePlayDefinition.GetRandomSpawn(this).Position, Vector3.zero, false);
            base.Awake();
        }
        #endregion

        public void OnFixedUpdate(float deltaTime)
        {
            this.UpdateWounded(deltaTime);
        }

        private void UpdateWounded(float deltaTime)
        {
            if (this.HasPlayerFlag(EPlayerFlags.Wounded))
            {
                this.LostTimeWounded -= deltaTime;
                if (this.LostTimeWounded <= 0)
                {
                    this.Health = 0f;
                    this.OnDie(new HitInfo(null, this, EDamageType.Bleeding, this.Health));
                }
            }
        }
        
        #region [Method] Spawn
        public override void Spawn()
        {
            base.Spawn();
            this.GiveDefaultItems();
        }
        
        #endregion

        #region [Method] GiveDefaultItems
        public virtual void GiveDefaultItems()
        {
            var defaultInventory = GamePlayDefinition.GetRandomStartInventory();
            for (var i = 0; i < defaultInventory.ContainerBelt.Count; i++)
            {
                var itemInfo = defaultInventory.ContainerBelt[i];
                Item item = Item.Create(itemInfo.ItemID, itemInfo.Amount, itemInfo.SkinID);
                if (item.HeldEntity != null && item.HeldEntity is BaseProjectile baseProjectile)
                    baseProjectile.AmountAmmoMagazine = itemInfo.AmountBullet;
                this.Inventory.ContainerBelt.GiveItem(item);
            }
            
            for (var i = 0; i < defaultInventory.ContainerMain.Count; i++)
            {
                var itemInfo = defaultInventory.ContainerMain[i];
                Item item = Item.Create(itemInfo.ItemID, itemInfo.Amount, itemInfo.SkinID);
                if (item.HeldEntity != null && item.HeldEntity is BaseProjectile baseProjectile)
                    baseProjectile.AmountAmmoMagazine = itemInfo.AmountBullet;
                this.Inventory.ContainerMain.GiveItem(item);
            }
        }
        #endregion

        #region [Method] Hurt

        public override void Hurt(HitInfo hitInfo)
        {
            if (hitInfo.InitiatorPlayer != null)
                this.lastHitInfoFromPlayer = hitInfo;
            base.Hurt(hitInfo);
            if (this.Net.ConnectionOwner?.connected == true)
                this.SendMessage_RPCMessage(new SendInfo(this.Net.ConnectionOwner), null, ERPCMessage.BasePlayer_DirectionalDamage, hitInfo.HitPositionWorld, (int)hitInfo.DamageType);
        }

        #endregion

        public override void OnDie(HitInfo hitInfo)
        {
            // TODO: Need drop item in world
            this.ActiveHeldEntity = null;
            if (this.HasPlayerFlag(EPlayerFlags.Wounded) == false && GamePlayDefinition.Instance.Configuration.PlayerWoundedEnable)
            {
                if (DateTime.Now.Subtract(LastWoundedTime).TotalSeconds > GamePlayDefinition.Instance.Configuration.PlayerWoundedDelay)
                {
                    if (GamePlayDefinition.Instance.Configuration.PlayerWoundedIfHeadshot || hitInfo.isHeadshot == false)
                    {
                        this.Health = 5;
                        this.StartWounded(GamePlayDefinition.Instance.Configuration.PlayerWoundedTime);
                        return;
                    }
                }
            }
            this.OnKilled(hitInfo);
        }

        public override void OnKilled(HitInfo hitInfo = null)
        {
            this.SetPlayerFlag(EPlayerFlags.Wounded, false,false);
            this.SetPlayerFlag(EPlayerFlags.Sleeping, false,false);
            this.SetPlayerFlag(EPlayerFlags.Unused1, false,false);
            this.SetPlayerFlag(EPlayerFlags.Unused2, false,false);
            
            this.Inventory.ClearInventory();
            
            this.Health = 0;
            this.SendMeTo(this.Net.ListFullSubscribers);
            if (this.Net.ConnectionOwner != null)
                this.SendMessage_RPCMessage(new SendInfo(this.Net.ConnectionOwner), null, ERPCMessage.BasePlayer_OnDied);
            
            if (this.lastHitInfoFromPlayer != null && this.lastHitInfoFromPlayer.InitiatorPlayer != null)
            {
                if (GamePlayDefinition.Instance.Configuration.InfoKillInChat)
                {
                    int distance = (int)Vector3.Distance(this.lastHitInfoFromPlayer.PointStart, this.Position);
                    string headShot = (this.lastHitInfoFromPlayer.isHeadshot ? " in <color=red>HeadShot</color>" : "");
                    string killOwner = this.lastHitInfoFromPlayer.InitiatorPlayer.DisplayName;
                    string weapon = string.Empty;
                    if (this.lastHitInfoFromPlayer.Weapon != null)
                        weapon = this.lastHitInfoFromPlayer.Weapon.ParentItem.ItemDefinition.DisplayName;
                    NetworkManager.Instance.ListConnections.SendMessage_ChatMessage($"<color=green>[{this.DisplayName}]</color> was killed by <color=green>[{killOwner}]</color> from weapon <color=blue>{weapon}</color> - <color=blue>[{distance} m]</color>" + headShot);
                }
            }
        }

        #region [Method] OnRespawn

        public virtual void OnRespawn()
        {
            
            Vector3 newPosition = GamePlayDefinition.GetRandomSpawn(this).Position;
            bool isConnected = this.Net.ConnectionOwner != null && this.Net.ConnectionOwner.connected;
            if (isConnected)
            {
                this.StartReceivingSnapshot();
                this.SendMessage_RPCMessage(new SendInfo(this.Net.ConnectionOwner), null, ERPCMessage.BasePlayer_ForcePositionTo, newPosition);
            }
            this.UnsubscribeToAll();
            this.UnsubscribeAllToMe();

            GamePlayDefinition.SpawnDefinition spawnDefinition = GamePlayDefinition.GetRandomSpawn(this);
            this.Net.ConnectionOwner?.SendMessage_ConsoleCommand("echo You spawned for: " + spawnDefinition.SpawnName);
            this.Inventory.ClearInventory();
            this.SetPositionAndRotation(spawnDefinition.Position, Vector3.zero, false);
            this.StartSleeping(false);
            this.LastWoundedTime = DateTime.MinValue;
            this.lastHitInfoFromPlayer = null;
            this.Health = this.MaxHealth;
            this.GiveDefaultItems();

            this.SubscribeToAll();
            this.SubscribeAllToMe();
            
            if (isConnected)
                this.EndReceivingSnapshot();
            else
                this.SendMeTo(this.Net.ListFullSubscribers);
        }

        #endregion

        #region [Method] SetConnectionOwner
        public override void SetConnectionOwner(Connection connection)
        {
            if (connection == null && this.Net.ConnectionOwner != null)
            {
                this.OnDisconnected();
                base.SetConnectionOwner(null);
            }
            else if (connection != null && this.Net.ConnectionOwner == null)
            {
                if (ListActivePlayers.ContainsKey(connection))
                    throw new Exception("This connection have player!");
                base.SetConnectionOwner(connection);
                this.OnConnected();
            }
        }
        
        #endregion

        #region [Method] SendMeTo
        public override void SendMeTo(object target = null)
        {
            if (target == null)
                target = this.Net.ConnectionOwner;
            if (target.IsValidTarget() == false)
                return;
            using (Entity entity = Pool.Get<Entity>())
            {
                entity.baseNetworkable = Pool.Get<ProtoBuf.BaseNetworkable>();
                entity.baseNetworkable.uid = this.Net.UID;
                entity.baseNetworkable.prefabID = this.Net.PrefabID;
                entity.baseNetworkable.group = 0;
                
                if (this.Net.ParentUID != 0)
                {
                    entity.parent = Pool.Get<ParentInfo>();
                    entity.parent.uid = this.Net.ParentUID;
                    entity.parent.bone = this.Net.ParentBone;
                }
                
                entity.baseEntity = Pool.Get<ProtoBuf.BaseEntity>();
                entity.baseEntity.pos = this.Position;
                entity.baseEntity.rot = this.Rotation;
                entity.baseEntity.flags = (int)this.Flags;

                entity.baseCombat = Tefor.Engine.Pool.Get<BaseCombat>();
                entity.baseCombat.health = this.Health;
                entity.baseCombat.state = (int)this.LifeState;

                entity.basePlayer = Pool.Get<ProtoBuf.BasePlayer>();
                entity.basePlayer.health = this.Health;
                entity.basePlayer.userid = this.SteamID;
                entity.basePlayer.name = this.DisplayName;
                entity.basePlayer.playerFlags = (int)this.PlayerFlags;
                entity.basePlayer.modelState = Pool.Get<ModelState>();
                entity.basePlayer.modelState.flags = (int)this.ModelStateFlags;
                entity.basePlayer.modelState.lookDir = this.Rotation;

                entity.basePlayer.inventory = this.Inventory.Parse();
                entity.basePlayer.heldEntity = this.ActiveHeldEntity?.HeldIDInPlayer ?? 0;

                target.SendMessage_Entities(entity);
            }
        }
        #endregion

        #region [Method] OnReceivedTick
        public virtual void OnReceivedTick(Message message)
        {
            if (this.Net.ConnectionOwner?.connected == true)
            {
                bool needSendUpdate = false;
                PlayerTick currentTick = PlayerTick.Deserialize(message.read, this.LastPlayerTick, true);
                this.LastPlayerTick = currentTick.Copy();

                if (currentTick.inputState.buttons != 0 && this.HasPlayerFlag(EPlayerFlags.Sleeping))
                    this.EndSleeping();

                if (this.HasPlayerFlag(EPlayerFlags.Sleeping) == false)
                {
                    Vector3 pos = currentTick.position;
                    Vector3 rot = currentTick.inputState.aimAngles;
                    this.EyePosition = currentTick.eyePos;

                    if ((int) this.ModelStateFlags != currentTick.modelState.flags)
                    {
                        this.ModelStateFlags = (EPlayerModalState) currentTick.modelState.flags;
                        needSendUpdate = true;
                    }


                    if ((currentTick.activeItem != 0 && (this.ActiveHeldEntity == null || this.ActiveHeldEntity.ParentItem.UID != currentTick.activeItem)) || (this.ActiveHeldEntity != null && currentTick.activeItem == 0))
                    {
                        if (this.HasPlayerFlag(EPlayerFlags.Wounded) == false)
                        {
                            this.OnActiveItemChanged(currentTick.activeItem);
                            needSendUpdate = true;
                        }
                    }

                    if (pos != this.Position || rot != this.Rotation)
                        this.SetPositionAndRotation(pos, rot);

                }

                if (needSendUpdate)
                    this.SendMeTo(this.Net.ListViewToMe);

                currentTick.Dispose();
            }
        }
        #endregion

        #region [Method] OnActiveItemChanged
        public virtual void OnActiveItemChanged(uint activeUID)
        {
            if (activeUID == 0)
                this.SetActiveHeldEntity(null);
            else if (Item.ListSpawnedItems.TryGetValue(activeUID, out Item item))
                this.SetActiveHeldEntity(item.HeldEntity);
            else
            {
                if (this.ActiveHeldEntity != null)
                    this.SetActiveHeldEntity(null);
            }
        }
        #endregion

        #region [Method] SetActiveHeldEntity
        public void SetActiveHeldEntity(HeldEntity heldEntity)
        {
            if (this.ActiveHeldEntity != heldEntity)
            {
                this.ActiveHeldEntity?.SetHeldState(false);
                this.ActiveHeldEntity = heldEntity;
                this.SendMeTo(this.Net.ListFullSubscribers);
                if (this.ActiveHeldEntity != null)
                    this.ActiveHeldEntity.SetHeldState(true, true);
            }
        }
        #endregion

        #region [Method] OnConnected
        public virtual void OnConnected()
        {
            ListActivePlayers[this.Net.ConnectionOwner] = this;
            this.DisplayName = this.Net.ConnectionOwner.username;
            this.SteamID = this.Net.ConnectionOwner.userid;
         
            if (this.Net.ConnectionOwner.authLevel >= 1)
                this.SetPlayerFlag(EPlayerFlags.IsAdmin, true, false);
            this.SetPlayerFlag(EPlayerFlags.Connected, true, false);
            
            this.StartReceivingSnapshot();
            
            this.SubscribeToAll();
            this.EndReceivingSnapshot();

            if (GamePlayDefinition.Instance.Configuration.InfoConnectionsInChat)
            {
                NetworkManager.Instance.ListConnections.SendMessage_ChatMessage($"Player <color=green>[{this.DisplayName}]</color> has been connected!");
            }
            
            this.Net.ConnectionOwner.SendMessage_ChatMessage($"Engine: Rust Server Emu\n<b>Version:</b> {ApplicationVersion.Version}\n<b>Branch:</b> {ApplicationVersion.Branch}\n<b>Date:</b> {ApplicationVersion.VersionTime}\n<b>Discord:</b> https://discord.gg/SWz7GgW");
            this.Net.ConnectionOwner.SendMessage_ChatMessage(GamePlayDefinition.Instance.StartMessage);
        }
        #endregion

        #region [Method] OnDisconnected
        
        public virtual void OnDisconnected()
        {
            NetworkManager.Instance.ListConnections.SendMessage_ChatMessage($"Player <color=green>[{this.DisplayName}]</color> has been disconnected!");
            NetworkManager.Instance.Despawn(this);
            if (ListActivePlayers.ContainsKey(this.Net.ConnectionOwner))
                ListActivePlayers.Remove(this.Net.ConnectionOwner);
        }
        
        #endregion

        #region [Method] OnDespawn

        public override void OnDespawn()
        {
            this.UnsubscribeToAll();
            base.OnDespawn();
            this.Inventory.ClearInventory();
            if (ListAnyBasePlayers.ContainsKey(this.SteamID))
                ListAnyBasePlayers.Remove(this.SteamID);
        }

        #endregion

        #region [Method] OnRPCMessage

        public override void OnRPCMessage(Message message, ERPCMessage rpcMethod)
        {
            switch (rpcMethod)
            {
                case ERPCMessage.BasePlayer_OnPlayerLanded:
                    this.OnRPCMessage_OnPlayerLanded(message);
                    break;
                case ERPCMessage.BasePlayer_ClientLoadingComplete:
                    this.OnRPCMessage_ClientLoadingComplete(message);
                    break;
                case ERPCMessage.BasePlayer_UpdateSteamInventory:
                    // TODO: Maybe late release steam inventory.
                    break;
                case ERPCMessage.BasePlayer_MoveItem:
                    this.Inventory.OnRPCMessage_MoveItem(message);
                    break;
                case ERPCMessage.BasePlayer_OnProjectileAttack:
                    this.OnRPCMessage_OnProjectileAttack(message);
                    break;
                case ERPCMessage.BasePlayer_OnProjectileRicochet:
                    // TODO: Maybe not need release? Need check server code...
                    break;
                case ERPCMessage.BasePlayer_OnProjectileUpdate:
                    // TODO: Maybe not need release? Need check server code...
                    break;
                case ERPCMessage.BasePlayer_KeepAlive:
                    this.OnRPCMessage_KeepAlive(message);
                    break;
                case ERPCMessage.BasePlayer_Assist:
                    this.OnRPCMessage_Assist(message);
                    break;
                case ERPCMessage.BasePlayer_ItemCMD:
                    this.Inventory.OnRPCMessage_ItemCMD(message);
                    break;
                default:
                    base.OnRPCMessage(message, rpcMethod);
                    break;
            }
        }
        
        #endregion

        #region [OnRPCMessage] ClientLoadingComplete
        void OnRPCMessage_ClientLoadingComplete(Message message)
        {
            this.SetPlayerFlag(EPlayerFlags.Connected, false);
        }
        #endregion

        #region [OnRPCMessage] OnPlayerLanded

        void OnRPCMessage_OnPlayerLanded(Message message)
        {
            float num = message.read.Float();
            if (float.IsNaN(num) || float.IsInfinity(num))
                return;
            
            float fallHeight = Mathf.InverseLerp(-15f, -100f, num);
            if (fallHeight == 0f)
                return;
            
            // TODO: Need release bleeding from metabolism 
            //this.metabolism.bleeding.Add(num2 * 0.5f);
            
            float damage = fallHeight * 500f;
            this.Hurt(damage, null, EDamageType.Fall, this.Position);
            if (damage > 20f)
            {
                Effect.Instance.Init(EPrefabID.Effect_BasePlayer_FallDamage, EEffectType.Generic, this.Position, default(Vector3), 0);
                this.Net.ListViewToMe.SendMessage_Effect(Effect.Instance);
                this.Net.ConnectionOwner.SendMessage_Effect(Effect.Instance);
            }
        }
        
        #endregion

        public void OnCleanOldProjectile()
        {
            var oldProjectile = this.ListFiredProjectile.Where(p => DateTime.Now.Subtract(p.Value.StartProjectileLife).TotalSeconds > 8).ToArray();
            for (var i = 0; i < oldProjectile.Length; i++)
                this.ListFiredProjectile.Remove(oldProjectile[i].Value.ProjectileID);
        }
        
        public void OnFiredProjectile(ProjectileShoot.Projectile projectile, AttackEntity weapon, ItemDefinition bullet)
        {
            if (this.ListFiredProjectile.TryGetValue(projectile.projectileID, out FiredProjectile oldProjectile))
                this.ListFiredProjectile.Remove(projectile.projectileID);
            this.ListFiredProjectile[projectile.projectileID] = new FiredProjectile(projectile.projectileID, weapon, weapon.ParentItem.ItemDefinition, bullet);
        }

        protected void OnRPCMessage_OnProjectileAttack(Message message)
        {
            using (PlayerProjectileAttack playerProjectileAttack = PlayerProjectileAttack.Deserialize(message.read))
            {
                if (playerProjectileAttack?.playerAttack == null)
                    return;
                if (this.ListFiredProjectile.TryGetValue(playerProjectileAttack.playerAttack.projectileID, out FiredProjectile firedProjectile))
                {
                    HitInfo hitInfo = new HitInfo();
                    hitInfo.LoadFromAttack(playerProjectileAttack.playerAttack.attack, true);
                    hitInfo.Initiator = this;
                    hitInfo.ProjectileID = playerProjectileAttack.playerAttack.projectileID;
                    hitInfo.ProjectileDistance = playerProjectileAttack.hitDistance;
                    hitInfo.ProjectileVelocity = playerProjectileAttack.hitVelocity;
                    hitInfo.Predicted = message.connection;
                    hitInfo.Weapon = firedProjectile.Weapon;
                    if (hitInfo.Weapon is BaseProjectile baseProjectile)
                    {
                        if (BulletDefinition.ListBullets.TryGetValue(firedProjectile.BulletDefinition.ItemID, out BulletDefinition bulletDefinition))
                            hitInfo.DamageAmount = bulletDefinition.BaseDamage;
                        else
                            message.connection.SendMessage_ChatMessage($"<color>[DEBUG]:</color> You not released bullet:\n<b>ItemID:</b> {firedProjectile.BulletDefinition.ItemID}\n<b>Item:</b> {firedProjectile.BulletDefinition.DisplayName}" );

                        if (firedProjectile.WeaponDefinition?.HeldDefinition?.RangeDefinition != null)
                            hitInfo.DamageAmount *= firedProjectile.WeaponDefinition.HeldDefinition.RangeDefinition.DamageScale;
                        
                    } else if (hitInfo.Weapon is BaseMelee)
                    {
                        hitInfo.DamageAmount = 0;
                        // TODO: Need save melee damage to field and reuse field
                        if (hitInfo.Weapon.ParentItem.ItemDefinition.ItemComponentDefinitions.Projectile != null)
                        {
                            var projectileDefinition = hitInfo.Weapon.ParentItem.ItemDefinition.ItemComponentDefinitions.Projectile; 
                            for (int i = 0; i < projectileDefinition.DamageTypes.Count; i++)
                            {
                                if (hitInfo.DamageAmount == 0)
                                    hitInfo.DamageType = projectileDefinition.DamageTypes[i].Type;
                                hitInfo.DamageAmount = projectileDefinition.DamageTypes[i].Damage;
                            }
                        }
                        if (Item.ListSpawnedItems.ContainsKey(hitInfo.Weapon.ParentItem.UID))
                            hitInfo.Weapon.ParentItem.Remove();
                    }

                    if (hitInfo.HitEntity != null)
                    {
                        hitInfo.HitEntity.OnAttacked(hitInfo);
                    }

                    this.ListFiredProjectile.Remove(playerProjectileAttack.playerAttack.projectileID);
                }
            }      
        }


        protected void OnRPCMessage_KeepAlive(Message message)
        {
            if (this.Net.ConnectionOwner != message.connection)
            {
                BasePlayer playerOwner = message.connection.ToPlayer();
                if (playerOwner != null && playerOwner.LifeState == EBaseCombatLifeState.Alive && playerOwner.HasPlayerFlag(EPlayerFlags.Wounded) == false)
                {
                    if (this.LostTimeWounded < 2f)
                        this.LostTimeWounded += 10f;
                }
            }
        }

        protected void OnRPCMessage_Assist(Message message)
        {
            if (this.Net.ConnectionOwner != message.connection)
            {
                BasePlayer playerOwner = message.connection.ToPlayer();
                if (playerOwner != null && playerOwner.LifeState == EBaseCombatLifeState.Alive && playerOwner.HasPlayerFlag(EPlayerFlags.Wounded) == false)
                {
                    this.SetPlayerFlag(EPlayerFlags.Wounded, false);
                    this.SendMeTo(this.Net.ListFullSubscribers);
                }
            }
        }

        public override void OnAttacked(HitInfo hitInfo)
        {
            base.OnAttacked(hitInfo);
            if (this.Net.ConnectionOwner?.connected == true)
            {
                Effect effect = Effect.Instance;
                effect.Init(EPrefabID.Effect_TakeDamageHit, EEffectType.Generic, this.Position, QuaternionEx.ToQuaternion(this.Rotation) * Vector3.forward, 0);
                this.Net.ConnectionOwner.SendMessage_Effect(effect);
            }

            bool isHeadshot = hitInfo.isHeadshot;
            ESignal signalFlinch = isHeadshot ? ESignal.Flinch_Head : ESignal.Flinch_Chest;
            
            if (isHeadshot)
            {
                Effect effectHeadshot = Effect.Instance;
                effectHeadshot.Init(EPrefabID.Effect_Headshot, EEffectType.Generic, new Vector3(0, 2, 0), Vector3.zero, (hitInfo.Initiator.Net?.ConnectionOwner.userid ?? 0));
                effectHeadshot.entity = this.Net.UID;
                this.Net.ListViewToMe.SendMessage_Effect(effectHeadshot);
                this.Net.ConnectionOwner.SendMessage_Effect(effectHeadshot);
            }
            
            this.SendMessage_RPCMessage(new SendInfo(this.Net.ListViewToMe)
            {
                method = SendMethod.Unreliable,
                priority = Priority.Immediate
            }, null, ERPCMessage.BaseEntity_SignalFromServerEx, (int)signalFlinch, string.Empty);
            
            this.SendMessage_RPCMessage(new SendInfo(this.Net.ConnectionOwner)
            {
                method = SendMethod.Unreliable,
                priority = Priority.Immediate
            }, null, ERPCMessage.BaseEntity_SignalFromServerEx, (int)signalFlinch, string.Empty);
        }
    }
}