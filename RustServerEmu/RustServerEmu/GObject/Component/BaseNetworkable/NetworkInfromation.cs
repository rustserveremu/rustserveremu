using System;
using System.Collections.Generic;
using Facepunch;
using Network;

namespace RustServerEmu.GObject.Component.BaseNetworkable
{
    public class NetworkInfromation
    {
        private static uint LastInsertID { get; set; } = 0;
        public static uint GiveUID() => ++LastInsertID;
        
        public uint UID { get; } = ++LastInsertID;
        public string Name { get; set; } = "NoNameObject";
        public uint ParentUID { get; set; } = 0;
        public uint ParentBone { get; set; } = 0;
        
        public uint PrefabID
        {
            get => this.m_prefabID;
            set
            {
                if (this.m_prefabID == 0)
                    this.m_prefabID = value;
                else
                    throw new Exception("PrefabID already initialized!");
            }
        }

        private uint m_prefabID = 0;
        
        public List<Connection> ListViewToMe { get; } = Pool.GetList<Connection>();
        public List<Connection> ListFullSubscribers { get; } = Pool.GetList<Connection>();
        public List<GameObject.BaseNetworkable> ListViewNetworkables { get; } = Pool.GetList<GameObject.BaseNetworkable>();

        public Connection ConnectionOwner { get; set; }
    }
}