using System;
using System.Collections.Generic;
using RustServerEmu.AppEnvironment;
using RustServerEmu.Data;
using RustServerEmu.Database;
using RustServerEmu.Extension;
using RustServerEmu.GObject.Component.BaseNetworkable;
using Tefor.Engine;
using Pool = Facepunch.Pool;

namespace RustServerEmu.GObject.Component
{
    public class ItemContainer
    {
        public static Dictionary<uint, ItemContainer> ListSpawnedItenContainers { get; } = new Dictionary<uint, ItemContainer>();
        public uint UID { get; } = NetworkInfromation.GiveUID();
        public int Capacity { get; private set; } = 0;
        public Item ParentItem { get; private set; }
        public EItemContainerFlags ItemContainerFlags { get; set; } = 0;
        public List<Item> ListItems { get; } = new List<Item>();
        public Dictionary<int, Item> ItemSlots { get; } = new Dictionary<int, Item>();
        public GameObject.BasePlayer PlayerOwner { get; private set; }

        public void Init(int capacity, Item parent, GameObject.BasePlayer playerOwner)
        {
            if (this.Capacity != 0)
                throw new Exception("This Container is Initialized!");
            ListSpawnedItenContainers.Add(this.UID, this);
            this.Capacity = capacity;
            this.ParentItem = parent;
            this.PlayerOwner = playerOwner;
            for (int i = 0; i < this.Capacity; i++)
                this.ItemSlots.Add(i, null);
        }

        public bool GiveItem(Item item, int slot = -1)
        {
            if (item != null)
            {
                if (this.HasItemContainerFlag(EItemContainerFlags.Clothing) == false || item.ItemDefinition.Category == EItemCategory.Attire)
                {
                    if (this.ListItems.Contains(item) == false)
                    {
                        if (this.HasHaveFreeSlot())
                        {
                            if (item.ParentContainer != null && item.ParentContainer != this)
                                item.ParentContainer.RemoveItem(item);

                            if (slot == -1)
                                slot = this.GetFirstFreeSlot();
                            this.ListItems.Add(item);
                            this.ItemSlots[slot] = item;
                            item.ParentContainer = this;
                            item.Slot = slot;

                            item.OnContainerChanged();
                            return true;
                        }
                        else
                            Debug.LogWarning("[ItemContainer::GiveItem]: Not found free slot! TODO: Need release drop item in world!");
                        // TODO: Need release drop item in world

                    }
                    else
                        Debug.LogError("[ItemContainer::GiveItem]: You move item to container = item parent container");
                }
            } else
                Debug.LogError("[ItemContainer::GiveItem]: You give item is null");

            return false;
        }

        public bool RemoveItem(Item item)
        {
            if (this.ListItems.Contains(item))
            {
                this.ListItems.Remove(item);
                item.ParentContainer = null;
                this.ItemSlots[item.Slot] = null;
                item.Slot = -1;
                
                item.OnContainerChanged();
                
                if (this.PlayerOwner != null)
                {
                    if (item.HeldEntity != null &&  this.PlayerOwner.ActiveHeldEntity == item.HeldEntity)
                        this.PlayerOwner.SetActiveHeldEntity(null);

                    
                    if (this.ItemContainerFlags == EItemContainerFlags.Belt || this.ItemContainerFlags == EItemContainerFlags.Clothing)
                        this.PlayerOwner.SendMeTo(this.PlayerOwner.Net.ListFullSubscribers);
                    else
                        this.PlayerOwner.SendMeTo();
                }

                return true;
            }
            return false;
        }

        public int GetFirstFreeSlot()
        {
            foreach (var itemSlot in this.ItemSlots)
                if (itemSlot.Value == null)
                    return itemSlot.Key;
            return -1;
        }

        public bool HasHaveFreeSlot()
        {
            return this.ListItems.Count < this.Capacity;
        }

        public ProtoBuf.ItemContainer Parse()
        {
            ProtoBuf.ItemContainer container = Pool.Get<ProtoBuf.ItemContainer>();
            container.flags = (int)this.ItemContainerFlags;
            container.UID = this.UID;
            container.slots = this.Capacity;
            container.allowedContents = this.Capacity - this.ListItems.Count;
            
            
            container.contents = Pool.GetList<ProtoBuf.Item>();
            for (var i = 0; i < this.ListItems.Count; i++)
                container.contents.Add(this.ListItems[i].Parse());

            container.availableSlots = Pool.GetList<int>();
            foreach (var itemSlot in this.ItemSlots)
            {
                if (itemSlot.Value == null)
                    container.availableSlots.Add(itemSlot.Key);
            }

            return container;
        }

        public void ClearContainer(bool networkUpdate = true)
        {
            for (var i = this.ListItems.Count - 1; i >= 0; i--)
            {
                if (Item.ListSpawnedItems.ContainsKey(this.ListItems[i].UID))
                    Item.ListSpawnedItems.Remove(this.ListItems[i].UID);
                this.ListItems[i].ParentContainer = null;
                this.ListItems[i].Slot = -1;
                if (this.ListItems[i].HeldEntity != null)
                    NetworkManager.Instance.Despawn(this.ListItems[i].HeldEntity);
            }
            
            this.ListItems.Clear();
            for (int i = 0; i < this.Capacity; i++)
                this.ItemSlots[i] = null;

            if (this.PlayerOwner != null)
            {
                this.PlayerOwner.SetActiveHeldEntity(null);
                if (networkUpdate)
                    this.PlayerOwner.SendMeTo(this.PlayerOwner.Net.ListFullSubscribers);
            }
        }
    }
}