using System.Collections.Generic;
using System.Threading;
using Facepunch;
using Network;
using ProtoBuf;
using RustServerEmu.Data;
using RustServerEmu.Database;
using RustServerEmu.Extension;
using UnityEngine;

namespace RustServerEmu.GObject.Component.BasePlayer
{
    public class PlayerInventory
    {
        public GameObject.BasePlayer PlayerOwner { get; }
        public PlayerLoot Loot { get; private set; }
        public ItemContainer ContainerBelt { get; }
        public ItemContainer ContainerWear { get; }
        public ItemContainer ContainerMain { get; }

        public PlayerInventory(GameObject.BasePlayer owner)
        {
            this.PlayerOwner = owner;
            
            this.ContainerMain = new ItemContainer();
            this.ContainerMain.SetItemContainerFlag(EItemContainerFlags.IsPlayer, true);
            this.ContainerMain.Init(24, null, owner);
            this.ContainerBelt = new ItemContainer();
            this.ContainerBelt.SetItemContainerFlag(EItemContainerFlags.IsPlayer, true);
            this.ContainerBelt.SetItemContainerFlag(EItemContainerFlags.Belt, true);
            this.ContainerBelt.Init(6, null, owner);
            this.ContainerWear = new ItemContainer();
            this.ContainerWear.SetItemContainerFlag(EItemContainerFlags.IsPlayer, true);
            this.ContainerWear.SetItemContainerFlag(EItemContainerFlags.Clothing, true);
            this.ContainerWear.Init(7, null, owner);
            this.Loot = new PlayerLoot();
        }

        public bool GiveItem(Item item)
        {
            if (item.ItemDefinition.Category == EItemCategory.Weapon ||
                item.ItemDefinition.Category == EItemCategory.Tool ||
                item.ItemDefinition.Category == EItemCategory.Construction ||
                item.ItemDefinition.Category == EItemCategory.Items ||
                item.ItemDefinition.Category == EItemCategory.Food ||
                item.ItemDefinition.Category == EItemCategory.Medical ||
                item.ItemDefinition.Category == EItemCategory.Traps)
            {
                int freeSlot = this.ContainerBelt.GetFirstFreeSlot();
                if (freeSlot == -1 || this.ContainerBelt.GiveItem(item) == false)
                {
                    freeSlot = this.ContainerMain.GetFirstFreeSlot();
                    if (freeSlot == -1 || this.ContainerMain.GiveItem(item) == false)
                    {
                        if (item.ItemDefinition.Category == EItemCategory.Attire)
                        {
                            freeSlot = this.ContainerWear.GetFirstFreeSlot();
                            if (freeSlot == -1 || this.ContainerWear.GiveItem(item) == false)
                                return false;
                        }
                        else
                            return false;
                    }
                }
            }
            else
            {
                int freeSlot = this.ContainerMain.GetFirstFreeSlot();
                if (freeSlot == -1 || this.ContainerMain.GiveItem(item) == false)
                {
                    freeSlot = this.ContainerBelt.GetFirstFreeSlot();
                    if (freeSlot == -1 || this.ContainerBelt.GiveItem(item) == false)
                    {
                        if (item.ItemDefinition.Category == EItemCategory.Attire)
                        {
                            freeSlot = this.ContainerWear.GetFirstFreeSlot();
                            if (freeSlot == -1 || this.ContainerWear.GiveItem(item) == false)
                                return false;
                        }
                        else
                            return false;
                    }
                }
            }

            return true;
        }

        public bool IsItemInInventory(Item item)
        {
            if (this.ContainerBelt.ListItems.Contains(item) == false)
                if (this.ContainerMain.ListItems.Contains(item) == false)
                    if (this.ContainerWear.ListItems.Contains(item) == false)
                        return false;
            return true;
        }
        
        public Item GetItemFromUID(uint uid)
        {
            if (Item.ListSpawnedItems.TryGetValue(uid, out Item item) && this.IsItemInInventory(item))
                return item;
            return null;
        }

        public void ClearInventory()
        {
            this.ContainerBelt.ClearContainer(false);
            this.ContainerWear.ClearContainer(false);
            this.ContainerMain.ClearContainer(false);
            
            this.PlayerOwner.SendMeTo(this.PlayerOwner.Net.ListFullSubscribers);
        }

        public int UseAmountFromItemDefinition(ItemDefinition itemDefinition, int needAmount)
        {
            int result = 0;

            for (int i = this.ContainerBelt.ListItems.Count - 1; i >= 0 && result < needAmount; i--)
            {
                if (this.ContainerBelt.ListItems[i].ItemDefinition == itemDefinition)
                {
                    if (this.ContainerBelt.ListItems[i].Amount + result > needAmount)
                    {
                        this.ContainerBelt.ListItems[i].Amount -= needAmount - result;
                        result = needAmount;
                    }
                    else
                    {
                        result += this.ContainerBelt.ListItems[i].Amount;
                        this.ContainerBelt.ListItems[i].Remove();
                    }
                }
            }
            
            for (int i = this.ContainerMain.ListItems.Count - 1; i >= 0 && result < needAmount; i--)
            {
                if (this.ContainerMain.ListItems[i].ItemDefinition == itemDefinition)
                {
                    if (this.ContainerMain.ListItems[i].Amount + result > needAmount)
                    {
                        this.ContainerMain.ListItems[i].Amount -= needAmount - result;
                        result = needAmount;
                    }
                    else
                    {
                        result += this.ContainerMain.ListItems[i].Amount;
                        this.ContainerMain.ListItems[i].Remove();
                    }
                }
            }

            if (result > 0)
                this.PlayerOwner.SendMeTo();

            return result;
        }

        public int GetCountAmmoFromItemDefinition(ItemDefinition itemDefinition)
        {
            int result = 0;
            
            for (var i = 0; i < this.ContainerBelt.ListItems.Count; i++)
            {
                if (this.ContainerBelt.ListItems[i].ItemDefinition == itemDefinition)
                    result += this.ContainerBelt.ListItems[i].Amount;
            }
            for (var i = 0; i < this.ContainerMain.ListItems.Count; i++)
            {
                if (this.ContainerMain.ListItems[i].ItemDefinition == itemDefinition)
                    result += this.ContainerMain.ListItems[i].Amount;
            }
            
            return result;
        }

        public ItemDefinition GetItemDefinitionAmmo(EAmmoTypes type)
        {
            List<ItemDefinition> listValidAmmoDefinitions = Pool.GetList<ItemDefinition>();
            ItemDefinition result = null;
            
            foreach (var bullet in BulletDefinition.ListBullets)
            {
                if (bullet.Value.AmmoType == type)
                {
                    if (ItemDefinition.ListItems.TryGetValue(bullet.Value.ItemID, out ItemDefinition itemDefinition))
                    {
                        listValidAmmoDefinitions.Add(itemDefinition);
                    }
                }
            }
            
            for (var i = 0; result == null && i < this.ContainerBelt.ListItems.Count; i++)
            {
                if (listValidAmmoDefinitions.Contains(this.ContainerBelt.ListItems[i].ItemDefinition))
                    result = this.ContainerBelt.ListItems[i].ItemDefinition;
            }
            for (var i = 0; result == null && i < this.ContainerMain.ListItems.Count; i++)
            {
                if (listValidAmmoDefinitions.Contains(this.ContainerMain.ListItems[i].ItemDefinition))
                    result = this.ContainerMain.ListItems[i].ItemDefinition;
            }
            Pool.FreeList(ref listValidAmmoDefinitions);
            return result;
        }

        public void OnRPCMessage_ItemCMD(Message message)
        {
            uint id = message.read.UInt32();
            string text = message.read.String();
            if (Item.ListSpawnedItems.TryGetValue(id, out Item item))
            {
                if (item.HasItemFlag(EItemFlags.IsLocked) || item.ParentContainer.HasItemContainerFlag(EItemContainerFlags.IsLocked))
                    return;
                if (item?.ParentContainer?.PlayerOwner != this.PlayerOwner)
                    return;
                if (text == "drop")
                {
                    // TODO: Need release drop item to world after release world item.
                    int amount = (message.read.unread >= 4 ? message.read.Int32() : item.Amount);
                    if (amount < item.Amount)
                    {
                        item.Amount -= amount;
                        item.OnContainerChanged();
                    } else
                        item.Remove();
                    return;
                }
                item.ServerCommand(text, this.PlayerOwner);
            }
        }

        public void OnRPCMessage_MoveItem(Message message)
        {
            uint itemUID = message.read.UInt32();
            uint targetContainerUID = message.read.UInt32();
            int targetSlot = (int)message.read.Int8();
            int amount = (int)message.read.UInt16();
            Item item = this.GetItemFromUID(itemUID);
            if (item != null)
            {
                if (targetContainerUID == 0)
                {
                    item.Drop(message.connection.ToPlayer().Position + Vector3.up);
                    return;
                }
                if (ItemContainer.ListSpawnedItenContainers.TryGetValue(targetContainerUID, out ItemContainer targetContainer))
                {
                    if (targetContainer.HasItemContainerFlag(EItemContainerFlags.Clothing) == false || item.ItemDefinition.Category == EItemCategory.Attire)
                    {
                        Item itemInTargetSlot = targetContainer.ItemSlots[targetSlot];
                        int oldSlot = item.Slot;
                        ItemContainer oldContainer = item.ParentContainer;
                        oldContainer.RemoveItem(item);
                        if (itemInTargetSlot != null)
                        {
                            targetContainer.RemoveItem(itemInTargetSlot);
                            oldContainer.GiveItem(itemInTargetSlot, oldSlot);
                        }

                        targetContainer.GiveItem(item, targetSlot);
                    }
                } else
                    message.connection.SendMessage_ChatMessage("Target container is not found!");
            } else
                message.connection.SendMessage_ChatMessage("This item not found, maybe not you inventory!");
        }

        public ProtoBuf.PlayerInventory Parse()
        {
            ProtoBuf.PlayerInventory playerInventory = Pool.Get<ProtoBuf.PlayerInventory>();
            playerInventory.invBelt = this.ContainerBelt.Parse();
            playerInventory.invMain = this.ContainerMain.Parse();
            playerInventory.invWear = this.ContainerWear.Parse();
            return playerInventory;
        }
    }
}