using System;
using RustServerEmu.Database;
using RustServerEmu.GObject.GameObject.HeldObject;

namespace RustServerEmu.GObject.Component
{
    public class FiredProjectile
    {
        public DateTime StartProjectileLife { get; } = DateTime.Now;
        public int ProjectileID { get; private set; }
        public AttackEntity Weapon { get; private set; }
        public ItemDefinition WeaponDefinition { get; private set; }
        public ItemDefinition BulletDefinition { get; private set; }

        public FiredProjectile(int projectileId, AttackEntity weapon, ItemDefinition weaponDefinition, ItemDefinition bulletDefinition)
        {
            this.ProjectileID = projectileId;
            this.Weapon = weapon;
            this.WeaponDefinition = weaponDefinition;
            this.BulletDefinition = bulletDefinition;
        }
    }
}