using System.Collections.Generic;
using RustServerEmu.AppEnvironment;
using RustServerEmu.Data;
using RustServerEmu.Database;
using RustServerEmu.Extension;
using RustServerEmu.Extension.ProtoBuf;
using RustServerEmu.GObject.Component.BaseNetworkable;
using RustServerEmu.GObject.GameObject;
using RustServerEmu.GObject.GameObject.HeldObject;
using UnityEngine;
using Debug = Tefor.Engine.Debug;
using Pool = Facepunch.Pool;

namespace RustServerEmu.GObject.Component
{
    public class Item
    {
        public static Dictionary<uint, Item> ListSpawnedItems { get; } = new Dictionary<uint, Item>();
        public static Item Create(ItemDefinition itemDefinition, int amount, ulong skinid = 0)
        {
            if (itemDefinition != null)
            {
                var createdItem = new Item(itemDefinition, amount);
                createdItem.SkinID = skinid;
                return createdItem;
            } else
                Debug.LogError($"[Item::Create]: Received itemDefinition is null!");
            return null;
        }

        public static Item Create(int itemid, int amount, ulong skinid = 0)
        {
            if (ItemDefinition.ListItems.TryGetValue(itemid, out ItemDefinition item))
            {
                return Create(item, amount, skinid);
            }
            else
                Debug.LogError($"[Item::Create]: Item from [{itemid}] itemid - not found!");
            return null;
        }
        
        public uint UID { get; } = NetworkInfromation.GiveUID();
        public ItemDefinition ItemDefinition { get; private set; } = null;
        public ItemContainer ChildContainer { get; private set; } = null;
        public ItemContainer ParentContainer { get; set; }
        public HeldEntity HeldEntity { get; set; }
        public EItemFlags ItemFlags { get; set; } = EItemFlags.None;
        public int Amount { get; set; } = 0;
        public int Slot { get; set; } = 0;
        public ulong SkinID { get; set; } = 0;
        public float Condition { get; set; } = 100;
        public float MaxCondition { get; set; } = 100;
        public bool HasCondition { get; set; } = true;

        private Item(ItemDefinition itemDefinition, int amount)
        {
            ListSpawnedItems.Add(this.UID, this);
            this.ItemDefinition = itemDefinition;
            this.Amount = amount;
            this.HasCondition = this.ItemDefinition.Condition > 0;
            this.Condition = this.MaxCondition = this.ItemDefinition.Condition;
            if (this.ItemDefinition.HeldDefinition != null)
            {
                switch (this.ItemDefinition.HeldDefinition.TypeHeldEntity)
                {
                    case "BaseProjectile":
                        this.HeldEntity = NetworkManager.Instance.Create<BaseProjectile>();
                        break;
                    case "BaseMelee":
                        this.HeldEntity = NetworkManager.Instance.Create<BaseMelee>();
                        break;
                    case "MedicalTool":
                        this.HeldEntity = NetworkManager.Instance.Create<MedicalTool>();
                        break;
                    case "Planner":
                        this.HeldEntity = NetworkManager.Instance.Create<Planner>();
                        break;
                    case "BowWeapon":
                        this.HeldEntity = NetworkManager.Instance.Create<BowWeapon>();
                        break;
                    case "CompoundBowWeapon":
                        this.HeldEntity = NetworkManager.Instance.Create<CompoundBowWeapon>();
                        break;
                    default:
                        this.HeldEntity = NetworkManager.Instance.Create<HeldEntity>();
                        break;
                }
                this.HeldEntity.SetParentItem(this);
                this.HeldEntity.Spawn();
            }
        }

        public virtual void OnContainerChanged()
        {
            if (this.HeldEntity != null)
            {
                if (this.HeldEntity.HasFlag(EEntityFlags.Reserved4))
                    this.HeldEntity.SetHeldState(false, false);
                if (this.HeldEntity.ParentPlayer != null)
                {
                    this.HeldEntity.SendMeTo(this.HeldEntity.ParentPlayer.Net.ConnectionOwner);
                    this.HeldEntity.SendMeTo(this.HeldEntity.ParentPlayer.Net.ListViewToMe);
                }
            }
            
            if (this.ParentContainer?.PlayerOwner != null)
            {
                
                if ( this.ParentContainer.ItemContainerFlags == EItemContainerFlags.Belt ||  this.ParentContainer.ItemContainerFlags == EItemContainerFlags.Clothing)
                    this.ParentContainer?.PlayerOwner.SendMeTo(this.ParentContainer?.PlayerOwner.Net.ListFullSubscribers);
                else
                    this.ParentContainer?.PlayerOwner.SendMeTo();
            }
        }
        
        public ProtoBuf.Item Parse()
        {
            ProtoBuf.Item item = Pool.Get<ProtoBuf.Item>();
            item.contents = this.ChildContainer?.Parse();
            item.UID = this.UID;
            item.itemid = this.ItemDefinition.ItemID;
            item.heldEntity = this.HeldEntity?.Net.UID ?? 0;
            item.flags = (int) this.ItemFlags;
            item.amount = this.Amount;
            item.name = "";
            item.slot = this.Slot;
            item.skinid = this.SkinID;
            item.conditionData = Pool.Get<ProtoBuf.Item.ConditionData>();
            item.conditionData.condition = this.Condition;
            item.conditionData.maxCondition = this.MaxCondition;
            return item;
        }
        
        public void LoseCondition(float amount)
        {
            if (this.HasCondition == false)
                return;
            float condition = this.Condition;
            this.Condition -= amount;
            if (this.Condition <= 0f && this.Condition < condition)
                this.OnBroken();
            else
                ParentContainer?.PlayerOwner?.SendMeTo();
        }
        
        public virtual void OnBroken()
        {
            if (this.HasCondition == false)
                return;
            
            
            if (this.HeldEntity != null)
            {
                this.HeldEntity.SetFlag(EEntityFlags.Broken, true, false, true);
                if (this.ParentContainer?.PlayerOwner?.ActiveHeldEntity == this.HeldEntity)
                {
                    Effect.Instance.Init(EPrefabID.Effect_ItemBreak, EEffectType.Generic, Vector3.zero, Vector3.zero, 0);
                    Effect.Instance.entity = this.ParentContainer.PlayerOwner.Net.UID;
                    this.ParentContainer.PlayerOwner.Net.ListViewToMe.SendMessage_Effect(Effect.Instance);
                    this.ParentContainer.PlayerOwner.Net.ConnectionOwner.SendMessage_Effect(Effect.Instance);
                    
                    this.ParentContainer.PlayerOwner.Net.ConnectionOwner.SendMessage_ChatMessage("Your active item was broken!");
                }
            }
            this.Remove();
        }
        
        public void Remove()
        {
            // TODO: After release item mode - need remove modes
            this.ParentContainer?.RemoveItem(this);
            
            if (this.HeldEntity != null)
            {
                NetworkManager.Instance.Despawn(this.HeldEntity);
                this.HeldEntity = null;
            }

            ListSpawnedItems.Remove(this.UID);
        }

        public void Drop(Vector3 position)
        {
            // TODO: Need release drop item in world
            this.Remove();
        }

        public void ServerCommand(string text, GameObject.BasePlayer playerTarget)
        {
            switch (text)
            {
                case "consume":
                    this.OnItemCMD_Consume(text, playerTarget);
                    break;
                default:
                    this.HeldEntity?.ServerCommand(text, playerTarget);
                    break;
            }
        }

        public void OnItemCMD_Consume(string text, GameObject.BasePlayer playerTarget)
        {
            if (this.ParentContainer.PlayerOwner.ActiveHeldEntity != this.HeldEntity)
                this.ParentContainer.PlayerOwner.SetActiveHeldEntity(null);

            if (this.ItemDefinition.ItemComponentDefinitions.Consumable != null)
            {
                List<ItemDefinition.ItemComponentConsumable.ConsumableEffect> listEffects = this.ItemDefinition.ItemComponentDefinitions.Consumable.Effects;
                for (var i = 0; i < listEffects.Count; i++)
                {
                    if (listEffects[i].Type == EMetabolismType.Health)
                    {
                        playerTarget.Health += (playerTarget.Health + listEffects[i].Amount > playerTarget.MaxHealth ? (playerTarget.MaxHealth - playerTarget.Health) : listEffects[i].Amount);
                    }
                    if (listEffects[i].Type == EMetabolismType.HealthOverTime)
                    {
                        float amount = listEffects[i].Amount / 2;
                        playerTarget.Health += (playerTarget.Health + amount > playerTarget.MaxHealth ? (playerTarget.MaxHealth - playerTarget.Health) : amount);
                    }
                }
                playerTarget.SendMeTo();
            }
            
            this.Amount--;
            if (this.Amount == 0)
                this.Remove();
            else
                this.ParentContainer?.PlayerOwner?.SendMeTo();
        }
    }
}