using System;
using System.IO;
using System.Reflection;
using System.Windows.Forms;
using RustServerEmu.BasicEnvironment;

namespace RustServerEmu
{
    public class Bootstrap
    {
        static void Main()
        {
            ApplicationVersion.ShowHeaderInConsole();
            try
            {
                InitializationDLLResolver();
                if (Directory.Exists(AppDomain.CurrentDomain.BaseDirectory + "/Logs/") == false)
                    Directory.CreateDirectory(AppDomain.CurrentDomain.BaseDirectory + "/Logs/");
                typeof(Bootstrap)
                    .Assembly.GetType(typeof(Bootstrap).Namespace + ".AppEnvironment.AppManager")
                    .GetMethod("Init", BindingFlags.Static | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.CreateInstance)
                    .Invoke(null, new object[0]);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Fatal error in Application: " + ex);
                System.Environment.FailFast(ex.ToString());
            }
        }
        
        static void InitializationDLLResolver()
        {
            if (Directory.Exists(AppDomain.CurrentDomain.BaseDirectory + "/Data/Bin/"))
            {
                AppDomain.CurrentDomain.AppendPrivatePath(AppDomain.CurrentDomain.BaseDirectory + "/Data/Bin/");
                //AppDomain.CurrentDomain.SetupInformation.PrivateBinPath = AppDomain.CurrentDomain.BaseDirectory + "/Data/Bin/";
                AppDomain.CurrentDomain.AssemblyResolve += (s, e) =>
                {
                    var filename = new AssemblyName(e.Name).Name;
                    var path = AppDomain.CurrentDomain.BaseDirectory + "/Data/Bin/" + filename + ".dll";
                    if (File.Exists(path))
                    {
                        byte[] buffer = File.ReadAllBytes(path);
                        return Assembly.Load(buffer);
                    }
                    return null;
                };
            }
            else
            {
                MessageBox.Show("Not found /Data/Bin/ directory!");
                System.Environment.FailFast("Not found /Data/Bin/ directory!");
            }
        }
    }
}