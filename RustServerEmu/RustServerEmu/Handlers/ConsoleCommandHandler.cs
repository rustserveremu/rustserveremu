using System;
using System.Collections.Generic;
using System.Reflection;
using Network;
using Tefor.Engine;

namespace RustServerEmu.Handlers
{
    public class ConsoleCommandHandler
    {
        private static Dictionary<string, ConsoleCommandMethodAttribute> ListCommands { get; } = new Dictionary<string, ConsoleCommandMethodAttribute>();

        internal static void Init()
        {
            Type[] types = typeof(ConsoleCommandHandler).Assembly.GetTypes();
            for (var i = 0; i < types.Length; i++)
            {
                MethodInfo[] listMethods = types[i].GetMethods(BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Static);
                for (var j = 0; j < listMethods.Length; j++)
                {
                    object[] attrs = listMethods[j].GetCustomAttributes(typeof(ConsoleCommandMethodAttribute), true);
                    if (attrs.Length > 0)
                    {
                        ConsoleCommandMethodAttribute attr = (ConsoleCommandMethodAttribute) attrs[0];
                        attr.MethodDelegate = new FastMethodInfo(listMethods[j]);
                        ListCommands[attr.Command] = attr;
                    }
                }
            }
            Debug.LogSuccessful($"[ConsoleCommandHandler]: Loaded {ListCommands.Count} count console commands!");
        }

        public static void RunConsoleCommand(string line, ConsoleCommandHandler.EConsoleCommandOwner owner = EConsoleCommandOwner.Any, Connection connectionOwner = null)
        {
            string[] args = line.Split(' ');
            if (args.Length >= 1 && args[0].Length > 0)
            {
                string command = args[0].ToLower();
                string outline = (args.Length > 1) ? line.Substring(command.Length + 1) : string.Empty;
                if (ListCommands.TryGetValue(command, out ConsoleCommandMethodAttribute methodAttribute))
                {
                    if (methodAttribute.CommandOwner == EConsoleCommandOwner.Any || owner == EConsoleCommandOwner.Any || methodAttribute.CommandOwner == owner)
                    {
                        try
                        {
                            methodAttribute.MethodDelegate.Invoke(null, new object[] {line, outline, args, owner, connectionOwner});
                        }
                        catch (Exception ex)
                        {
                            Debug.LogError($"[ConsoleCommandHandler::RunConsoleCommand]: Exception in command [{line}]:");
                            Debug.LogException(ex);
                        }
                    } else
                        Debug.LogWarning($"[ConsoleCommandHandler::RunConsoleCommand]: Received command [{line}] and not access!");
                } else
                    Debug.LogWarning($"[ConsoleCommandHandler::RunConsoleCommand]: Received command [{line}], and not found command");
            } else
                Debug.LogWarning("[ConsoleCommandHandler::RunConsoleCommand]: Received empty command!");
        }

        public class ConsoleCommandMethodAttribute : Attribute
        {
            public string Command { get; private set; }
            public FastMethodInfo MethodDelegate { get; set; }
            public EConsoleCommandOwner CommandOwner { get; private set; }

            public ConsoleCommandMethodAttribute(string command, EConsoleCommandOwner owner = EConsoleCommandOwner.Any)
            {
                this.Command = command.ToLower();
                this.CommandOwner = owner;
            }
        }

        public enum EConsoleCommandOwner
        {
            Any,
            Client,
            Server
        }
    }
}