using System;
using Network;
using RustServerEmu.Extension;
using RustServerEmu.GObject.GameObject;
using Tefor.Engine;

namespace RustServerEmu.Handlers.ChatCommands
{
    public class PlayerCommands
    {
        [ChatCommandHandler.ChatCommandMethodAttribute("pos")]
        static void OnChatCommand_Player_Pos(string command, string outline, string[] args, Connection connectionOwner)
        {
            try
            {
                if (connectionOwner != null)
                {
                    BasePlayer player = connectionOwner.ToPlayer();
                    if (player != null)
                    {
                        connectionOwner.SendMessage_ChatMessage($"You position: x: {player.Position.x}; y: {player.Position.y}; z: {player.Position.z}".Replace(",", "."));
                    }
                    else
                        Debug.LogWarning("[PlayerCommands::OnChatCommand_Player_Pos]: Player not found!");
                }
                else
                    Debug.LogWarning("[PlayerCommands::OnChatCommand_Player_Pos]: Connection is null!");
            }
            catch (Exception ex)
            {
                Debug.LogError("[PlayerCommands::OnChatCommand_Player_Pos]: Exception:");
                Debug.LogException(ex);
            }
        }
    }
}