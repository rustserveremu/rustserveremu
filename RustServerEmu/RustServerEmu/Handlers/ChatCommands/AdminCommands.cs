using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Network;
using RustServerEmu.AppEnvironment;
using RustServerEmu.Data;
using RustServerEmu.Database;
using RustServerEmu.Extension;
using RustServerEmu.GObject.Component;
using RustServerEmu.GObject.GameObject;
using RustServerEmu.GObject.GameObject.HeldObject;
using Tefor.Engine;
using UnityEngine;

namespace RustServerEmu.Handlers.ChatCommands
{
    public class AdminCommands
    {
        public static BasePlayer LastNPCCreated { get; private set; }

        [ChatCommandHandler.ChatCommandMethodAttribute("spawn.player")]
        static void OnChatCommand_Admin_SpawnPlayer(string command, string outline, string[] args, Connection connectionOwner)
        {
            if (connectionOwner.authLevel >= 1)
            {
                BasePlayer newPlayer = NetworkManager.Instance.Create<BasePlayer>();
                newPlayer.DisplayName = "Noob #" + newPlayer.Net.UID;
                newPlayer.SteamID = newPlayer.Net.UID;
                newPlayer.SetPositionAndRotation(connectionOwner.ToPlayer().Position, connectionOwner.ToPlayer().Rotation, false);
                newPlayer.EndSleeping(false);
                newPlayer.Spawn();
                LastNPCCreated = newPlayer;

                List<Item> list = new List<Item>(newPlayer.Inventory.ContainerBelt.ListItems);
                for (var i = 0; i < list.Count; i++)
                    list[i].Remove();
                newPlayer.Inventory.ContainerBelt.GiveItem(Item.Create(1545779598, 1));

                Timer.Once(2, () => { newPlayer.SetActiveHeldEntity(newPlayer.Inventory.ContainerBelt.ListItems.First().HeldEntity); }, Framework.MainFramework);
            }
        }
        
        [ChatCommandHandler.ChatCommandMethodAttribute("announce")]
        static void OnChatCommand_Admin_Announce(string command, string outline, string[] args, Connection connectionOwner)
        {
            if (connectionOwner.authLevel >= 1)
            {
                NetworkManager.Instance.ListConnections.SendMessage_ChatMessage(outline);
            }
        }
        
        [ChatCommandHandler.ChatCommandMethodAttribute("spawn.create")]
        static void OnChatCommand_Admin_CreateSpawn(string command, string outline, string[] args, Connection connectionOwner)
        {
            if (connectionOwner.authLevel >= 1)
            {
                GamePlayDefinition.SpawnDefinition spawnDefinition = new GamePlayDefinition.SpawnDefinition();
                spawnDefinition.SpawnName = ((args.Length >= 2 && args[1].Length > 1) ? args[1] : "Spawn#" + spawnDefinition.SpawnID);
                spawnDefinition.Position = connectionOwner.ToPlayer().Position;
                GamePlayDefinition.Instance.SpawnList.Add(spawnDefinition);
                GamePlayDefinition.Save();
                connectionOwner.SendMessage_ChatMessage("Spawn point has been added from name: <color=green>" + spawnDefinition.SpawnName + "</color>");
            }
        }
        
        [ChatCommandHandler.ChatCommandMethodAttribute("inventory.save")]
        static void OnChatCommand_Admin_InventorySave(string command, string outline, string[] args, Connection connectionOwner)
        {
            if (connectionOwner.authLevel >= 1)
            {
                GamePlayDefinition.Instance.StartInventory.Clear();
                GamePlayDefinition.Instance.StartInventory.Add(new GamePlayDefinition.StartInventoryDefinition());
                BasePlayer playerOwner = connectionOwner.ToPlayer();
                
                for (var i = 0; i < playerOwner.Inventory.ContainerBelt.ListItems.Count; i++)
                {
                    HeldEntity held = playerOwner.Inventory.ContainerBelt.ListItems[i].HeldEntity;
                    GamePlayDefinition.Instance.StartInventory[0].ContainerBelt.Add(new GamePlayDefinition.StartInventoryItemDefinition
                    {
                        Amount = playerOwner.Inventory.ContainerBelt.ListItems[i].Amount,
                        ItemID = playerOwner.Inventory.ContainerBelt.ListItems[i].ItemDefinition.ItemID,
                        AmountBullet = ((held != null && held is BaseProjectile baseProjectile) ? baseProjectile.AmountAmmoMagazine : 0)
                    });
                }
                
                for (var i = 0; i < playerOwner.Inventory.ContainerMain.ListItems.Count; i++)
                {
                    HeldEntity held = playerOwner.Inventory.ContainerMain.ListItems[i].HeldEntity;
                    GamePlayDefinition.Instance.StartInventory[0].ContainerMain.Add(new GamePlayDefinition.StartInventoryItemDefinition
                    {
                        Amount = playerOwner.Inventory.ContainerMain.ListItems[i].Amount,
                        ItemID = playerOwner.Inventory.ContainerMain.ListItems[i].ItemDefinition.ItemID,
                        AmountBullet = ((held != null && held is BaseProjectile baseProjectile) ? baseProjectile.AmountAmmoMagazine : 0)
                    });
                }
                
                connectionOwner.SendMessage_ChatMessage("Starting inventory clear and added you inventory!");
                
                GamePlayDefinition.Save();
            }
        }
        
        [ChatCommandHandler.ChatCommandMethodAttribute("inventory.add")]
        static void OnChatCommand_Admin_InventoryAdd(string command, string outline, string[] args, Connection connectionOwner)
        {
            if (connectionOwner.authLevel >= 1)
            {
                BasePlayer playerOwner = connectionOwner.ToPlayer();
                var newStartInventory = new GamePlayDefinition.StartInventoryDefinition();
                for (var i = 0; i < playerOwner.Inventory.ContainerBelt.ListItems.Count; i++)
                {
                    HeldEntity held = playerOwner.Inventory.ContainerBelt.ListItems[i].HeldEntity;
                    newStartInventory.ContainerBelt.Add(new GamePlayDefinition.StartInventoryItemDefinition
                    {
                        Amount = playerOwner.Inventory.ContainerBelt.ListItems[i].Amount,
                        ItemID = playerOwner.Inventory.ContainerBelt.ListItems[i].ItemDefinition.ItemID,
                        AmountBullet = ((held != null && held is BaseProjectile baseProjectile) ? baseProjectile.AmountAmmoMagazine : 0)
                    });
                }
                
                for (var i = 0; i < playerOwner.Inventory.ContainerMain.ListItems.Count; i++)
                {
                    HeldEntity held = playerOwner.Inventory.ContainerMain.ListItems[i].HeldEntity;
                    newStartInventory.ContainerMain.Add(new GamePlayDefinition.StartInventoryItemDefinition
                    {
                        Amount = playerOwner.Inventory.ContainerMain.ListItems[i].Amount,
                        ItemID = playerOwner.Inventory.ContainerMain.ListItems[i].ItemDefinition.ItemID,
                        AmountBullet = ((held != null && held is BaseProjectile baseProjectile) ? baseProjectile.AmountAmmoMagazine : 0)
                    });
                }
                
                GamePlayDefinition.Instance.StartInventory.Add(newStartInventory);
                connectionOwner.SendMessage_ChatMessage("Starting inventory has been added you inventory!");
                
                GamePlayDefinition.Save();
            }
        }
        
        [ChatCommandHandler.ChatCommandMethodAttribute("time")]
        static void OnChatCommand_Admin_SetEnv(string command, string outline, string[] args, Connection connectionOwner)
        {
            if (connectionOwner.authLevel >= 1)
            {
                if (args.Length >= 2)
                {
                    if (int.TryParse(args[1], out int hour))
                        BaseEnvironment.InstanceEnvironment.SetCurrentHour(hour);
                }
            }
        }
        
        
        
        [ChatCommandHandler.ChatCommandMethodAttribute("reload")]
        static void OnChatCommand_Admin_Reload(string command, string outline, string[] args, Connection connectionOwner)
        {
            if (connectionOwner.authLevel >= 1)
            {
                typeof(GamePlayDefinition).GetMethod("Init", BindingFlags.Instance | BindingFlags.Static | BindingFlags.NonPublic).Invoke(null, new object[0]);
                connectionOwner.SendMessage_ChatMessage("GamePlay has been reloaded!");
            }
        }
        
    }
}