using System;
using System.Collections.Generic;
using System.Reflection;
using Network;
using Tefor.Engine;

namespace RustServerEmu.Handlers
{
    public class ChatCommandHandler
    {
        private static Dictionary<string, ChatCommandMethodAttribute> ListCommands { get; } = new Dictionary<string, ChatCommandMethodAttribute>();

        internal static void Init()
        {
            Type[] types = typeof(ChatCommandHandler).Assembly.GetTypes();
            for (var i = 0; i < types.Length; i++)
            {
                MethodInfo[] listMethods = types[i].GetMethods(BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Static);
                for (var j = 0; j < listMethods.Length; j++)
                {
                    object[] attrs = listMethods[j].GetCustomAttributes(typeof(ChatCommandMethodAttribute), true);
                    if (attrs.Length > 0)
                    {
                        ChatCommandMethodAttribute attr = (ChatCommandMethodAttribute) attrs[0];
                        attr.MethodDelegate = new FastMethodInfo(listMethods[j]);
                        ListCommands[attr.Command] = attr;
                    }
                }
            }
            Debug.LogSuccessful($"[ChatCommandHandler]: Loaded {ListCommands.Count} count chat commands!");
        }

        public static bool RunChatCommand(string line, Connection connectionOwner)
        {
            string[] args = line.Split(' ');
            if (args.Length >= 1 && args[0].Length > 0)
            {
                string command = args[0].ToLower();
                string outline = (args.Length > 1) ? line.Substring(command.Length + 1) : string.Empty;
                if (ListCommands.TryGetValue(command, out ChatCommandMethodAttribute methodAttribute))
                {
                    try
                    {
                        methodAttribute.MethodDelegate.Invoke(null, new object[] {line, outline, args, connectionOwner});
                    }
                    catch (Exception ex)
                    {
                        Debug.LogError($"[ChatCommandHandler::RunChatCommand]: Exception in command [{line}]:");
                        Debug.LogException(ex);
                    }

                    return true;
                }
            }

            return false;
        }

        public class ChatCommandMethodAttribute : Attribute
        {
            public string Command { get; private set; }
            public FastMethodInfo MethodDelegate { get; set; }

            public ChatCommandMethodAttribute(string command)
            {
                this.Command = command.ToLower();
            }
        }
    }
}