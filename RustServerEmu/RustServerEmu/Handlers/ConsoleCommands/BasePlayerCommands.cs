using System;
using Facepunch.Math;
using Network;
using RustServerEmu.AppEnvironment;
using RustServerEmu.Data;
using RustServerEmu.Database;
using RustServerEmu.Extension;
using RustServerEmu.GObject.GameObject;
using RustServerEmu.Struct;
using RustServerEmu.Struct.Output;
using Tefor.Engine;

namespace RustServerEmu.Handlers.ConsoleCommands
{
    public class BasePlayerCommands
    {
        [ConsoleCommandHandler.ConsoleCommandMethodAttribute("respawn", ConsoleCommandHandler.EConsoleCommandOwner.Client)]
        static void OnConsoleCommand_BasePlayer_Respawn(string command, string outline, string[] args, ConsoleCommandHandler.EConsoleCommandOwner owner, Connection connectionOwner)
        {
            try
            {
                if (connectionOwner != null)
                {
                    BasePlayer player = connectionOwner.ToPlayer();
                    if (player != null && player.LifeState == EBaseCombatLifeState.Dead)
                    {
                        player.OnRespawn();
                    }
                    else
                        Debug.LogWarning("[BasePlayerCommands::OnConsoleCommand_BasePlayer_Respawn]: Player not found!");
                }
                else
                    Debug.LogWarning("[BasePlayerCommands::OnConsoleCommand_BasePlayer_Respawn]: Connection is null!");
            }
            catch (Exception ex)
            {
                Debug.LogError("[BasePlayerCommands::OnConsoleCommand_BasePlayer_Respawn]: Exception:");
                Debug.LogException(ex);
            }
        }
        
        [ConsoleCommandHandler.ConsoleCommandMethodAttribute("kill", ConsoleCommandHandler.EConsoleCommandOwner.Client)]
        static void OnConsoleCommand_BasePlayer_Kill(string command, string outline, string[] args, ConsoleCommandHandler.EConsoleCommandOwner owner, Connection connectionOwner)
        {
            try
            {
                if (connectionOwner != null)
                {
                    BasePlayer player = connectionOwner.ToPlayer();
                    if (player != null)
                    {
                        player.SetPlayerFlag(EPlayerFlags.Wounded, true, false);
                        player.Hurt(new HitInfo(null, player, EDamageType.Suicide, player.Health));
                    }
                    else
                        Debug.LogWarning("[BasePlayerCommands::OnConsoleCommand_BasePlayer_Respawn]: Player not found!");
                }
                else
                    Debug.LogWarning("[BasePlayerCommands::OnConsoleCommand_BasePlayer_Respawn]: Connection is null!");
            }
            catch (Exception ex)
            {
                Debug.LogError("[BasePlayerCommands::OnConsoleCommand_BasePlayer_Respawn]: Exception:");
                Debug.LogException(ex);
            }
        }
        
        [ConsoleCommandHandler.ConsoleCommandMethodAttribute("chat.say", ConsoleCommandHandler.EConsoleCommandOwner.Client)]
        static void OnConsoleCommand_BasePlayer_ChatSay(string command, string outline, string[] args, ConsoleCommandHandler.EConsoleCommandOwner owner, Connection connectionOwner)
        {
            try
            {
                if (connectionOwner != null)
                {
                    BasePlayer player = connectionOwner.ToPlayer();
                    if (player != null)
                    {
                        if (outline.Length >= 2)
                            outline = outline.Substring(1, outline.Length - 2);
                        if (outline.Length > 1 && outline[0] == '/')
                        {
                            if (ChatCommandHandler.RunChatCommand(outline.Substring(1), connectionOwner))
                                return;
                        }

                        string color = player.HasPlayerFlag(EPlayerFlags.IsAdmin) ? "#af5" : "#5af";
                        string prefix = "";

                        ChatEntry entry = new ChatEntry
                        {
                            Time = Epoch.Current,
                            Message = outline,
                            Color = color,
                            UserId = player.SteamID,
                            Username = player.DisplayName
                        };
                        RconManager.Instance.Broadcast(ELogType.Chat, entry);
                        
                        if (GamePlayDefinition.Instance.Players.TryGetValue(connectionOwner.userid, out GamePlayDefinition.PlayerDefinition playerDefinition))
                        {
                            if (string.IsNullOrEmpty(playerDefinition.ChatPrefix) == false)
                                prefix = $"<color={playerDefinition.ChatPrefixColor}>[{playerDefinition.ChatPrefix}]</color> ";
                        }
                        string ownerName = prefix + "<color=" + color + ">" + player.DisplayName + "</color>";
                        NetworkManager.Instance.ListConnections.SendMessage_ChatMessage(outline,ownerName, player.SteamID);
                    }
                    else
                        Debug.LogWarning("[BasePlayerCommands::OnConsoleCommand_BasePlayer_Respawn]: Player not found!");
                }
                else
                    Debug.LogWarning("[BasePlayerCommands::OnConsoleCommand_BasePlayer_Respawn]: Connection is null!");
            }
            catch (Exception ex)
            {
                Debug.LogError("[BasePlayerCommands::OnConsoleCommand_BasePlayer_Respawn]: Exception:");
                Debug.LogException(ex);
            }
        }
        
        [ConsoleCommandHandler.ConsoleCommandMethodAttribute("pos", ConsoleCommandHandler.EConsoleCommandOwner.Client)]
        static void OnConsoleCommand_BasePlayer_Pos(string command, string outline, string[] args, ConsoleCommandHandler.EConsoleCommandOwner owner, Connection connectionOwner)
        {
            try
            {
                if (connectionOwner != null)
                {
                    BasePlayer player = connectionOwner.ToPlayer();
                    if (player != null)
                    {
                        connectionOwner.SendMessage_ChatMessage($"You position: x: {player.Position.x}; y: {player.Position.y}; z: {player.Position.z}".Replace(",", "."));
                    }
                    else
                        Debug.LogWarning("[BasePlayerCommands::OnConsoleCommand_BasePlayer_Pos]: Player not found!");
                }
                else
                    Debug.LogWarning("[BasePlayerCommands::OnConsoleCommand_BasePlayer_Pos]: Connection is null!");
            }
            catch (Exception ex)
            {
                Debug.LogError("[BasePlayerCommands::OnConsoleCommand_BasePlayer_Pos]: Exception:");
                Debug.LogException(ex);
            }
        }
    }
}