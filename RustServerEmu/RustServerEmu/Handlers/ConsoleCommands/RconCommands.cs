using System;
using System.Collections.Generic;
using System.Linq;
using Facepunch.Math;
using Network;
using RustServerEmu.AppEnvironment;
using RustServerEmu.Data;
using RustServerEmu.Database;
using RustServerEmu.Extension;
using RustServerEmu.GObject.GameObject;
using RustServerEmu.Struct.Output;
using Tefor.Engine;

namespace RustServerEmu.Handlers.ConsoleCommands
{
    public class RconCommands
    {
        [ConsoleCommandHandler.ConsoleCommandMethodAttribute("serverinfo")]
        static void OnChatCommand_Server_ServerInfo(string command, string outline, string[] args, ConsoleCommandHandler.EConsoleCommandOwner owner, Connection connectionOwner)
        {
            Struct.Output.ServerInfoOutput info = new ServerInfoOutput
            {
                Hostname = AppManager.Instance.Settings.Server_Name,
                Map = AppManager.Instance.Settings.Map_Name,
                MaxPlayers = (int)AppManager.Instance.Settings.Server_MaxPlayers,
                Players = BasePlayer.ListActivePlayers.Count,
                Queued = 0,
                Joining = NetworkManager.Instance.ListInConnecting.Count,
                EntityCount = NetworkManager.Instance.ListSpawnedNetworkables.Count,
                GameTime = BaseEnvironment.InstanceEnvironment.CurrentTime.ToString(),
                Uptime = (int)DateTime.Now.Subtract(Framework.MainFramework.StartedTime).TotalSeconds,
                Framerate = AppManager.Instance.CurrentFPS,
                Memory =  (int)(GC.GetTotalMemory(false) / 1048576L),
                Collections = GC.CollectionCount(0),
                Restarting = false,
                NetworkIn = ((int)NetworkManager.Instance.BaseServer.GetStat(null, NetworkPeer.StatTypeLong.BytesReceived_LastSecond)),
                NetworkOut = ((int)NetworkManager.Instance.BaseServer.GetStat(null, NetworkPeer.StatTypeLong.BytesSent_LastSecond)),
                SaveCreatedTime = ""
            };
            RconManager.Instance.Broadcast(ELogType.Generic, info, true);
        }
        
        [ConsoleCommandHandler.ConsoleCommandMethodAttribute("playerlist")]
        static void OnChatCommand_Server_PlayerList(string command, string outline, string[] args, ConsoleCommandHandler.EConsoleCommandOwner owner, Connection connectionOwner)
        {
            List<PlayerInfoOutput> listPlayers = Pool.GetList<PlayerInfoOutput>();
            foreach (var player in BasePlayer.ListActivePlayers)
            {
                listPlayers.Add(new PlayerInfoOutput
                {
                    SteamID = player.Value.SteamID.ToString(),
                    DisplayName = player.Value.DisplayName,
                    Health = player.Value.Health,
                    Address = player.Value.Net.ConnectionOwner?.ipaddress ?? "",
                    Ping = NetworkManager.Instance.BaseServer.GetAveragePing(player.Value.Net.ConnectionOwner),
                    ConnectedSeconds = (int) (player.Value.Net.ConnectionOwner?.GetSecondsConnected() ?? 0),
                    VoiationLevel = 0,
                    OwnerSteamID = player.Value.SteamID.ToString()
                });
            }
            RconManager.Instance.Broadcast(ELogType.Generic, listPlayers, true);
        }
        
        [ConsoleCommandHandler.ConsoleCommandMethodAttribute("kick")]
        static void OnChatCommand_Server_Kick(string command, string outline, string[] args, ConsoleCommandHandler.EConsoleCommandOwner owner, Connection connectionOwner)
        {
            if (args.Length == 2)
            {
                if (ulong.TryParse(args[1], out ulong stetamid))
                {
                    Connection connection = null;
                    if (NetworkManager.Instance.ListConnectedPlayers.TryGetValue(stetamid, out connection))
                        NetworkManager.Instance.BaseServer.Kick(connection, "No Reason Given");
                } else
                    Debug.LogError("[RconCommands::OnChatCommand_Server_Kick]: Not valid format data!");
            }
        }
        
        [ConsoleCommandHandler.ConsoleCommandMethodAttribute("say")]
        static void OnChatCommand_Server_Say(string command, string outline, string[] args, ConsoleCommandHandler.EConsoleCommandOwner owner, Connection connectionOwner)
        {
            if (args.Length >= 2)
            {
                ChatEntry entry = new ChatEntry
                {
                    Time = Epoch.Current,
                    Message = outline,
                    Color = "orange",
                    UserId = 0,
                    Username = "Server"
                };
                RconManager.Instance.Broadcast(ELogType.Chat, entry);
                NetworkManager.Instance.ListConnections.SendMessage_ChatMessage(outline);
            }
        }
        
        [ConsoleCommandHandler.ConsoleCommandMethodAttribute("chat.tail")]
        static void OnChatCommand_Server_ChatTail(string command, string outline, string[] args, ConsoleCommandHandler.EConsoleCommandOwner owner, Connection connectionOwner)
        {
            if (args.Length >= 2 && int.TryParse(args[1], out int @int))
            {
                int startPos = RconResponse.HistroyChat.Count - @int;
                if (startPos < 0)
                    startPos = 0;
                for (var i = startPos; i < RconResponse.HistroyChat.Count; i++)
                    RconManager.Instance.Broadcast(RconResponse.HistroyChat[i].Type, RconResponse.HistroyChat[i]);
            }
        }
        
        [ConsoleCommandHandler.ConsoleCommandMethodAttribute("console.tail")]
        static void OnChatCommand_Server_ConsoleTail(string command, string outline, string[] args, ConsoleCommandHandler.EConsoleCommandOwner owner, Connection connectionOwner)
        {
            if (args.Length >= 2 && int.TryParse(args[1], out int @int))
            {
                int startPos = RconResponse.Histroy.Count - @int;
                if (startPos < 0)
                    startPos = 0;
                for (var i = startPos; i < RconResponse.Histroy.Count; i++)
                    RconManager.Instance.Broadcast(RconResponse.Histroy[i].Type, RconResponse.Histroy[i]);   
            }
        }
    }
}