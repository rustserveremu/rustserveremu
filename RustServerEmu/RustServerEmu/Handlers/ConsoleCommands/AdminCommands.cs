using System;
using Network;
using RustServerEmu.AppEnvironment;
using RustServerEmu.Database;
using RustServerEmu.Extension;
using RustServerEmu.GObject.Component;
using RustServerEmu.GObject.GameObject;
using UnityEngine;

namespace RustServerEmu.Handlers.ConsoleCommands
{
    public class AdminCommands
    {
        
        [ConsoleCommandHandler.ConsoleCommandMethodAttribute("inventory.giveid")]
        static void OnChatCommand_Admin_GiveID(string command, string outline, string[] args, ConsoleCommandHandler.EConsoleCommandOwner owner, Connection connectionOwner)
        {
            if (GamePlayDefinition.Instance.Configuration.PlayerAccessGiveItem || connectionOwner.authLevel >= 1)
            {
                if (args.Length == 3)
                {
                    if (int.TryParse(args[1].Substring(1, args[1].Length - 2), out int itemid))
                    {
                        if (int.TryParse(args[2].Substring(1, args[2].Length - 2), out int count))
                        {
                            Item item = Item.Create(itemid, count);
                            connectionOwner.ToPlayer().Inventory.GiveItem(item);
                            if (GamePlayDefinition.Instance.Configuration.PlayerGiveItemToChatLog)
                                NetworkManager.Instance.ListConnections.SendMessage_ChatMessage($"Player <color=green>[{connectionOwner.username}]</color> gave himself an item <color=blue>[" + item.ItemDefinition.DisplayName + "]</color> x" + count);
                        }
                    }
                }
            }
        }
        
        [ConsoleCommandHandler.ConsoleCommandMethodAttribute("entid")]
        static void OnChatCommand_Admin_EndID(string command, string outline, string[] args, ConsoleCommandHandler.EConsoleCommandOwner owner, Connection connectionOwner)
        {
            if (connectionOwner.authLevel >= 1)
            {
                if (args.Length == 3)
                {
                    string subCommand = args[1].Substring(1, args[1].Length - 2);
                    if (subCommand == "kill")
                    {
                        if (uint.TryParse(args[2].Substring(1, args[2].Length - 2), out uint uid))
                        {
                            if (uid != 0 && NetworkManager.Instance.ListSpawnedNetworkables.TryGetValue(uid, out BaseNetworkable networkable))
                            {
                                if (networkable is BuildingBlock)
                                {
                                    NetworkManager.Instance.Despawn(networkable);
                                }
                                else
                                    connectionOwner.SendMessage_ChatMessage($"Object UID [{uid}] is not the Building Object!");
                            }
                            else
                                connectionOwner.SendMessage_ChatMessage($"Object UID [{uid}] not found!");
                        }
                    }
                }
            }
        }
    }
}