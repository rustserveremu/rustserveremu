using System.Linq;
using Facepunch.Steamworks;
using Network;
using RustServerEmu.AppEnvironment;
using RustServerEmu.Data;
using RustServerEmu.Database;
using RustServerEmu.Extension;
using RustServerEmu.GObject.GameObject;
using Tefor.Engine;

namespace RustServerEmu.Handlers
{
    public class NetworkServerHandler : Network.IServerCallback
    {
        #region [Method] OnNetworkMessage
        public void OnNetworkMessage(Message message)
        {
//            if (message.type != Message.Type.Tick)
//                Debug.LogDebug("OnNetworkMessage: " + message.type);
            switch (message.type)
            {
                case Message.Type.GiveUserInformation:
                    this.OnNetworkMessage_GiveUserInformation(message);
                    break;
                case Message.Type.Ready:
                    NetworkManager.Instance.OnPlayerReady(message);
                    break;
                case Message.Type.ConsoleCommand:
                    this.OnNetworkMessage_ConsoleCommand(message);
                    break;
                case Message.Type.RPCMessage:
                    this.OnNetworkMessage_RPCMessage(message);
                    break;
                case Message.Type.Tick:
                    this.OnNetworkMessage_Tick(message);
                    break;
                case Message.Type.DisconnectReason:
                    
                    break;
                case Message.Type.EAC:
                    // TODO: Last task before release...
                    break;
                case Message.Type.VoiceData:
                    // TODO: Pre last task before release..
                    break;
            }
        }
        #endregion

        #region [Method] OnNetworkMessage_ConsoleCommand

        void OnNetworkMessage_ConsoleCommand(Message message)
        {
            string fullCommand = message.read.String();
            ConsoleCommandHandler.RunConsoleCommand(fullCommand, ConsoleCommandHandler.EConsoleCommandOwner.Client, message.connection);
        }

        #endregion

        #region [Method] OnNetworkMessage_DisconnectedReason
        void OnNetworkMessage_DisconnectedReason(Message message)
        {
            string reason = message.read.String();
            NetworkManager.Instance.OnPlayerDisconnected(message.connection, reason);
        }
        #endregion

        #region [Method] OnNetworkMessage_Tick

        void OnNetworkMessage_Tick(Message message)
        {
            BasePlayer player = message.connection.ToPlayer();
            player?.OnReceivedTick(message);
        }

        #endregion

        #region [Method] OnNetworkMessage_RPCMessage

        void OnNetworkMessage_RPCMessage(Message message)
        {
            uint entityUID = message.read.EntityID();
            uint rpcMethod = message.read.UInt32();
            if (NetworkManager.Instance.ListSpawnedNetworkables.TryGetValue(entityUID, out BaseNetworkable networkable))
            {
                //Debug.LogDebug("RPCMethod: " + (ERPCMessage) rpcMethod + " : " + networkable.GetType().Name + "[" + entityUID + "]");
                networkable.OnRPCMessage(message, (ERPCMessage)rpcMethod);
            } else
                Debug.LogWarning($"Connection [{message.connection}] call rpc method from unknown networkable [{entityUID}]");
        } 
        
        #endregion
        
        #region [Method] OnNetworkMessage_GiveUserInformation

        void OnNetworkMessage_GiveUserInformation(Message message)
        {
            if (message.connection.state != Connection.State.Unconnected)
            {
                NetworkManager.Instance.BaseServer.Kick(message.connection, "Invalid connection state");
                return;
            }
            message.connection.state = Connection.State.Connecting;
            if (message.read.UInt8() != 228)
            {
                NetworkManager.Instance.BaseServer.Kick(message.connection, "Invalid Connection Protocol");
                return;
            }
            message.connection.userid = message.read.UInt64();
            message.connection.protocol = message.read.UInt32();
            message.connection.os = message.read.String();
            message.connection.username = message.read.String();
            if (string.IsNullOrEmpty(message.connection.os))
            {
                NetworkManager.Instance.BaseServer.Kick(message.connection, "Invalid OS");
                return;
            }
            if (string.IsNullOrEmpty(message.connection.username))
            {
                NetworkManager.Instance.BaseServer.Kick(message.connection, "Empty Username");
                return;
            }
            message.connection.username = message.connection.username.Replace('\n', ' ').Replace('\r', ' ').Replace('\t', ' ').Trim();
            if (string.IsNullOrEmpty(message.connection.username))
            {
                NetworkManager.Instance.BaseServer.Kick(message.connection, "Invalid Username");
                return;
            }
            string branch = message.read.String();
            // TODO: Need release check brach
            if (message.connection.protocol > AppManager.Instance.Settings.Server_Protocol)
            {
                NetworkManager.Instance.BaseServer.Kick(message.connection, "Server need update!");
                return;
            }
            if (message.connection.protocol < AppManager.Instance.Settings.Server_Protocol)
            {
                NetworkManager.Instance.BaseServer.Kick(message.connection, "Client need update!");
                return;
            }
            message.connection.token = message.read.BytesWithSize();
            if (message.connection.token == null || message.connection.token.Length < 234 || message.connection.token.Length > 240)
            {
                NetworkManager.Instance.BaseServer.Kick(message.connection, "Steam Auth Invalid");
                return;
            }

            if (message.connection.userid <= 76561197960265728)
            {
                NetworkManager.Instance.BaseServer.Kick(message.connection, "SteamID Invalid");
                return;
            }

            if (NetworkManager.Instance.ListConnectedPlayers.ContainsKey(message.connection.userid))
            {
                NetworkManager.Instance.BaseServer.Kick(message.connection, "You already connected!");
                return;
            }

            if (GamePlayDefinition.Instance.Players.TryGetValue(message.connection.userid, out GamePlayDefinition.PlayerDefinition playerDefinition))
            {
                message.connection.authLevel = playerDefinition.AuthLevel;
                Debug.Log("Detected good player connecting - SteamID: " + playerDefinition.SteamID + " / AuthLevel: " + playerDefinition.AuthLevel);
            }

            bool result = SteamworksManager.Instance.BaseServer?.Auth.StartSession(message.connection.token, message.connection.userid) ?? false;
            if (result == false)
            {
                SteamworksManager.Instance.OnAuthChange(message.connection.userid, message.connection.userid, ServerAuth.Status.NoLicenseOrExpired, message.connection);
            }
            else
            {
                SteamworksManager.Instance.OnAuthChange(message.connection.userid, message.connection.userid, ServerAuth.Status.OK, message.connection);
            }


        }
        #endregion

        #region [Method] OnUnconnectedMessage
        public bool OnUnconnectedMessage(int type, Read read, uint ip, int port)
        {
            if (type != 255 || read.UInt8() != 255 || read.UInt8() != 255 || read.UInt8() != 255)
                return false;
            read.Position = 0L;
            byte[] buffer = new byte[read.Length];
            int size = read.Read(buffer, 0, (int) read.Length);
            
            if (SteamworksManager.Instance.BaseServer != null)
                SteamworksManager.Instance.BaseServer.Query.Handle(buffer, size, ip, (ushort)port);
            return true;
        }
        #endregion

        #region [Method] OnDisconnected
        public void OnDisconnected(string reason, Connection connection)
        {
            SteamworksManager.Instance.BaseServer?.Auth.EndSession(connection.userid);
            NetworkManager.Instance.OnPlayerDisconnected(connection, reason);
        }
        #endregion
    }
}