using System.Collections.Generic;
using Network;
using RustServerEmu.AppEnvironment;
using RustServerEmu.Data;
using RustServerEmu.Database;
using RustServerEmu.GObject.GameObject;
using RustServerEmu.GObject.GameObject.HeldObject;
using UnityEngine;

namespace RustServerEmu.Struct
{
	public class HitInfo
	{

		public bool IsProjectile()
		{
			return this.ProjectileID != 0;
		}

		public BasePlayer InitiatorPlayer
		{
			get
			{
				if (this.Initiator != null && this.Initiator is BasePlayer player)
					return player;
				return null;
			}
		}
		
		public Vector3 attackNormal => (this.PointEnd - this.PointStart).normalized;
		
		public bool HasDamage => this.DamageAmount > 0f;

		public HitInfo()
		{
		}

		public HitInfo(BaseEntity attacker, BaseEntity target, EDamageType type, float damageAmount, Vector3 vhitPosition)
		{
			this.Initiator = attacker;
			this.HitEntity = target;
			this.HitPositionWorld = vhitPosition;
			if (attacker != null)
			{
				this.PointStart = attacker.Position;
			}

			this.DamageAmount = damageAmount;
			this.DamageType = type;
		}

		public HitInfo(BaseEntity attacker, BaseEntity target, EDamageType type, float damageAmount) : this(attacker, target, type, damageAmount, target.Position)
		{
		}

		public void LoadFromAttack(ProtoBuf.Attack attack, bool serverSide)
		{
			this.HitEntity = null;
			this.PointStart = attack.pointStart;
			this.PointEnd = attack.pointEnd;
			if (attack.hitID > 0u)
			{
				this.DidHit = true;
				if (serverSide && NetworkManager.Instance.ListSpawnedNetworkables.TryGetValue(attack.hitID, out BaseNetworkable networkable))
					this.HitEntity = (networkable as BaseEntity);

				if (this.HitEntity != null)
				{
					this.HitBone = attack.hitBone;
					this.HitPart = attack.hitPartID;
				}
			}

			this.DidHit = true;
			this.HitPositionLocal = attack.hitPositionLocal;
			this.HitPositionWorld = attack.hitPositionWorld;
			this.HitNormalLocal = attack.hitNormalLocal.normalized;
			this.HitNormalWorld = attack.hitNormalWorld.normalized;
			this.HitMaterial = attack.hitMaterialID;
		}

		public bool isHeadshot
		{
			get
			{
				if (BoneDefinition.ListBones.TryGetValue(this.HitBone, out BoneDefinition bone))
					return bone.HitArea == EHitArea.Head;
				return false;
			}
		}

		public BaseEntity Initiator;

		public AttackEntity Weapon;

		public bool DoHitEffects = true;

		public bool DoDecals = true;

		public bool IsPredicting;

		public bool UseProtection = true;

		public Connection Predicted;

		public bool DidHit;

		public BaseEntity HitEntity;

		public uint HitBone;

		public uint HitPart;

		public uint HitMaterial;

		public Vector3 HitPositionWorld;

		public Vector3 HitPositionLocal;

		public Vector3 HitNormalWorld;

		public Vector3 HitNormalLocal;

		public Vector3 PointStart;

		public Vector3 PointEnd;

		public int ProjectileID;

		public float ProjectileDistance;

		public Vector3 ProjectileVelocity;

		//public Projectile ProjectilePrefab;
		
		public bool CanGather;

		public bool DidGather;

		public float gatherScale = 1f;

		public float DamageAmount = 0f;

		public EDamageType DamageType = EDamageType.Generic;
	}
}