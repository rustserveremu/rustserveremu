namespace RustServerEmu.Struct.Output
{
    public struct ServerInfoOutput
    {
        public string Hostname;

        public int MaxPlayers;

        public int Players;

        public int Queued;

        public int Joining;

        public int EntityCount;

        public string GameTime;

        public int Uptime;

        public string Map;

        public float Framerate;

        public int Memory;

        public int Collections;

        public int NetworkIn;

        public int NetworkOut;

        public bool Restarting;

        public string SaveCreatedTime;
    }
}