using System.Net;

namespace RustServerEmu.Struct.Output
{
    public struct RconCommand
    {
        // Token: 0x040026B7 RID: 9911
        public IPEndPoint Ip;

        // Token: 0x040026B8 RID: 9912
        public string ConnectionId;

        // Token: 0x040026B9 RID: 9913
        public string Name;

        // Token: 0x040026BA RID: 9914
        public string Message;

        // Token: 0x040026BB RID: 9915
        public int Identifier;
    }
}