namespace RustServerEmu.Struct.Output
{
    public struct ChatEntry
    {
        public string Message { get; set; }
        public ulong UserId { get; set; }
        public string Username { get; set; }
        public string Color { get; set; }
        public int Time { get; set; }
    }
}