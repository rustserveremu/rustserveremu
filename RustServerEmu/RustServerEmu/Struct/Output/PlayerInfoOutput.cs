namespace RustServerEmu.Struct.Output
{
    public struct PlayerInfoOutput
    {
        public string SteamID;

        public string OwnerSteamID;

        public string DisplayName;

        public int Ping;

        public string Address;

        public int ConnectedSeconds;

        public float VoiationLevel;

        public float CurrentLevel;

        public float UnspentXp;

        public float Health;
    }
}