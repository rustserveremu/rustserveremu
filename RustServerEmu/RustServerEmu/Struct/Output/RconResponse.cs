using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using RustServerEmu.Data;

namespace RustServerEmu.Struct.Output
{
    public struct RconResponse
    {
        public static List<RconResponse> Histroy { get; } = new List<RconResponse>();
        public static List<RconResponse> HistroyChat { get; } = new List<RconResponse>();
        
        public string Message;

        public int Identifier;

        [JsonConverter(typeof(StringEnumConverter))]
        public ELogType Type;

        public string Stacktrace;
    }
}