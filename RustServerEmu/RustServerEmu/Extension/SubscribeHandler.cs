using RustServerEmu.AppEnvironment;
using RustServerEmu.Data;
using RustServerEmu.GObject.GameObject;
using Tefor.Engine;

namespace RustServerEmu.Extension
{
    public static class SubscribeHandler
    {
        public static void SubscribeAllToMe(this BaseNetworkable networkable)
        {
            foreach (var player in BasePlayer.ListActivePlayers)
            {
                if (networkable.Net.ListViewToMe.Contains(player.Value.Net.ConnectionOwner) == false)
                {
                    if (player.Value != networkable)
                    {
                        networkable.Net.ListViewToMe.Add(player.Value.Net.ConnectionOwner);
                        networkable.Net.ListFullSubscribers.Add(player.Value.Net.ConnectionOwner);
                        player.Value.Net.ListViewNetworkables.Add(networkable);
                    } else if (networkable.Net.ListFullSubscribers.Contains(player.Value.Net.ConnectionOwner) == false)
                        networkable.Net.ListFullSubscribers.Add(player.Value.Net.ConnectionOwner);
                }
            }
            networkable.SendMeTo(networkable.Net.ListFullSubscribers);
        }

        public static void SubscribeToAll(this BasePlayer player)
        {
            if (player.Net.ConnectionOwner != null && player.Net.ConnectionOwner.connected == true)
            {
                foreach (var networkable in NetworkManager.Instance.ListSpawnedNetworkables)
                {
                    if (networkable.Value.Net.ListViewToMe.Contains(player.Net.ConnectionOwner) == false)
                    {
                        if (networkable.Value != player)
                        {
                            player.Net.ListViewNetworkables.Add(networkable.Value);
                            networkable.Value.Net.ListViewToMe.Add(player.Net.ConnectionOwner);
                            networkable.Value.Net.ListFullSubscribers.Add(player.Net.ConnectionOwner);
                        } else if (networkable.Value.Net.ListFullSubscribers.Contains(player.Net.ConnectionOwner) == false)
                            networkable.Value.Net.ListFullSubscribers.Add(player.Net.ConnectionOwner);
                    }
                    networkable.Value.SendMeTo(player.Net.ConnectionOwner);
                }
            }
            player.SendMeTo(player.Net.ListFullSubscribers);
        }

        public static void UnsubscribeAllToMe(this BaseNetworkable networkable)
        {
            networkable.Net.ListViewToMe.SendMessage_EntityDestroy(networkable, EDestroyMode.None);
            networkable.Net.ListViewToMe.Clear();
            networkable.Net.ListViewNetworkables.Clear();
            networkable.Net.ListFullSubscribers.Clear();
            if (networkable is BasePlayer player && player.Net.ConnectionOwner != null && player.Net.ConnectionOwner.connected == true)
                networkable.Net.ListFullSubscribers.Add(player.Net.ConnectionOwner);
        }

        public static void UnsubscribeToAll(this BasePlayer player)
        {
            if (player.Net.ConnectionOwner != null && player.Net.ConnectionOwner.connected == true)
            {
                for (var i = 0; i < player.Net.ListViewNetworkables.Count; i++)
                {
                    player.Net.ConnectionOwner.SendMessage_EntityDestroy(player.Net.ListViewNetworkables[i]);
                    if (player.Net.ListViewNetworkables[i].Net.ListViewToMe.Contains(player.Net.ConnectionOwner))
                    {
                        player.Net.ListViewNetworkables[i].Net.ListViewToMe.Remove(player.Net.ConnectionOwner);
                        player.Net.ListViewNetworkables[i].Net.ListFullSubscribers.Remove(player.Net.ConnectionOwner);
                    }
                }
            }
            else
            {
                for (var i = 0; i < player.Net.ListViewNetworkables.Count; i++)
                {
                    if (player.Net.ListViewNetworkables[i].Net.ListViewToMe.Contains(player.Net.ConnectionOwner))
                    {
                        player.Net.ListViewNetworkables[i].Net.ListViewToMe.Remove(player.Net.ConnectionOwner);
                        player.Net.ListViewNetworkables[i].Net.ListFullSubscribers.Remove(player.Net.ConnectionOwner);
                    }
                }
            }
            player.Net.ListViewNetworkables.Clear();
        }
    }
}