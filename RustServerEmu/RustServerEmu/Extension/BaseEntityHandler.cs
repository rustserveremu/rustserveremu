using RustServerEmu.Data;
using RustServerEmu.GObject.GameObject;

namespace RustServerEmu.Extension
{
    public static class BaseEntityHandler
    {
        public static bool HasFlag(this BaseEntity entity, EEntityFlags f) => ((entity.Flags & f) == f);

        public static void SetFlag(this BaseEntity entity, EEntityFlags f, bool b, bool recursive = false, bool networkUpdate = true)
        {
            if (b)
            {
                if (entity.HasFlag(f) == false)
                    entity.Flags |= f;
            }
            else 
            {
                if (entity.HasFlag(f))
                    entity.Flags &= ~f;
            }
            if (recursive && entity.ListChild != null && entity.ListChild.Count != 0)
            {
                for (int i = 0; i < entity.ListChild.Count; i++)
                {
                    if (entity.ListChild[i] is BaseEntity entity_item)
                        entity_item.SetFlag(f, b, true, true);
                }
            }
            
            if (networkUpdate)
                entity.SendMeTo(entity.Net.ListFullSubscribers);
        }
    }
}