using System;
using Network;
using RustServerEmu.AppEnvironment;
using RustServerEmu.Data;
using RustServerEmu.GObject.GameObject;

namespace RustServerEmu.Extension
{
    public static class PlayerHandler
    {
        public static BasePlayer ToPlayer(this Connection connection)
        {
            if (BasePlayer.ListActivePlayers.TryGetValue(connection, out BasePlayer player))
                return player;
            return null;
        }
        
        public static BasePlayer ToPlayer(this ulong steamid)
        {
            if (NetworkManager.Instance.ListConnectedPlayers.TryGetValue(steamid, out Connection connection))
            {
                if (BasePlayer.ListActivePlayers.TryGetValue(connection, out BasePlayer player))
                    return player;
            }
            return null;
        }

        public static BasePlayer ToPlayer(this BaseNetworkable networkable)
        {
            if (networkable is BasePlayer player)
                return player;
            return null;
        }
        
        public static bool HasModelStateFlag(this BasePlayer player, EPlayerModalState f) => ((player.ModelStateFlags & f) == f);

        public static void SetModelStateFlag(this BasePlayer player, EPlayerModalState f, bool b, bool networkUpdate = true)
        {
            if (b)
            {
                if (player.HasModelStateFlag(f) == false)
                    player.ModelStateFlags |= f;
            }
            else 
            {
                if (player.HasModelStateFlag(f))
                    player.ModelStateFlags &= ~f;
            }
            
            if (networkUpdate)
                player.SendMeTo(player.Net.ListFullSubscribers);
        }
        
        
        public static bool HasPlayerFlag(this BasePlayer player, EPlayerFlags f) => ((player.PlayerFlags & f) == f);

        public static void SetPlayerFlag(this BasePlayer player, EPlayerFlags f, bool b, bool networkUpdate = true)
        {
            if (b)
            {
                if (player.HasPlayerFlag(f) == false)
                    player.PlayerFlags |= f;
            }
            else 
            {
                if (player.HasPlayerFlag(f))
                    player.PlayerFlags &= ~f;
            }
            
            if (networkUpdate)
                player.SendMeTo(player.Net.ListFullSubscribers);
        }
        
        public static void StartSleeping(this BasePlayer player, bool networkUpdate = true)
        {
            player.ModelStateFlags = EPlayerModalState.Sleeping; 
            player.SetPlayerFlag(EPlayerFlags.Sleeping, true, false);
            if (networkUpdate)
                player.SendMeTo(player.Net.ListFullSubscribers);
        }

        public static void EndSleeping(this BasePlayer player, bool networkUpdate = true)
        {
            player.ModelStateFlags = EPlayerModalState.OnGround; 
            player.SetPlayerFlag(EPlayerFlags.Sleeping, false, false);
            if (networkUpdate)
                player.SendMeTo(player.Net.ListFullSubscribers);
        }
        
        
        
        public static void StartReceivingSnapshot(this BasePlayer player, bool networkUpdate = true)
        {
            if (player.Net.ConnectionOwner != null)
                player.SendMessage_RPCMessage(new SendInfo(player.Net.ConnectionOwner), null, ERPCMessage.BasePlayer_StartLoading);
            player.SetPlayerFlag(EPlayerFlags.ReceivingSnapshot, true, false);
            if (networkUpdate)
                player.SendMeTo(player.Net.ListFullSubscribers);
        }

        public static void EndReceivingSnapshot(this BasePlayer player, bool networkUpdate = true)
        {
            if (player.Net.ConnectionOwner != null)
                player.SendMessage_RPCMessage(new SendInfo(player.Net.ConnectionOwner), null, ERPCMessage.BasePlayer_FinishLoading);
            player.SetPlayerFlag(EPlayerFlags.ReceivingSnapshot, false, false);
            if (networkUpdate)
                player.SendMeTo(player.Net.ListFullSubscribers);
        }

        public static void StartWounded(this BasePlayer player, float time = 40f)
        {
            player.LastWoundedTime = DateTime.Now;
            player.LostTimeWounded = time;
            player.SetPlayerFlag(EPlayerFlags.Wounded, true, false);
            player.SetActiveHeldEntity(null);
            player.SendMeTo(player.Net.ListFullSubscribers);
        }
        public static void StopWounded(this BasePlayer player, bool networkUpdate = true)
        {
            player.SetPlayerFlag(EPlayerFlags.Wounded, false, false);
            if (networkUpdate)
                player.SendMeTo(player.Net.ListFullSubscribers);
        }
    }
}