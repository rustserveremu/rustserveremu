using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Facepunch.Math;
using Network;
using ProtoBuf;
using RustServerEmu.AppEnvironment;
using RustServerEmu.Data;
using RustServerEmu.Extension.ProtoBuf;
using RustServerEmu.Struct.Output;
using BaseEntity = RustServerEmu.GObject.GameObject.BaseEntity;
using BaseNetworkable = RustServerEmu.GObject.GameObject.BaseNetworkable;
using BasePlayer = RustServerEmu.GObject.GameObject.BasePlayer;

namespace RustServerEmu.Extension
{
	public static class ConnectionPacket
	{
		#region [Method] SendMessage_Approval

		public static void SendMessage_Approval(this Connection connection, uint encryptionLevel)
		{
			if (connection == null)
				return;
			using (Approval approval = Facepunch.Pool.Get<Approval>())
			{
				approval.level = AppManager.Instance.Settings.Map_Name;
				approval.levelUrl = "";
				approval.levelSeed = (uint) AppManager.Instance.Settings.Map_Seed;
				approval.levelSize = (uint) AppManager.Instance.Settings.Map_Size;
				approval.checksum = "";
				approval.hostname = AppManager.Instance.Settings.Server_Name;
				approval.official = false;
				approval.encryption = encryptionLevel;

				if (NetworkManager.Instance.BaseServer.write.Start())
				{
					NetworkManager.Instance.BaseServer.write.PacketID(Message.Type.Approved);
					approval.WriteToStream(NetworkManager.Instance.BaseServer.write);
					NetworkManager.Instance.BaseServer.write.Send(new SendInfo(connection));
				}
			}
		}

		#endregion

		#region [Method] IsValidTarget

		public static bool IsValidTarget(this object obj)
		{
			if (obj != null)
			{
				if (obj is Connection connection && connection.connected)
					return true;
				if (obj is List<Connection> listConnections && listConnections.Count != 0)
					return true;
				if (obj is Connection[] arrayConnections && arrayConnections.Length != 0)
					return true;
			}
			return false;
		}
		#endregion
		
		public static void SendMessage_ConsoleCommand(this Connection connection, string command)
		{
			if (connection == null)
				return;
			if (NetworkManager.Instance.BaseServer.write.Start())
			{
				NetworkManager.Instance.BaseServer.write.PacketID(Message.Type.ConsoleCommand);
				NetworkManager.Instance.BaseServer.write.String(command);
				NetworkManager.Instance.BaseServer.write.Send(new SendInfo(connection));
			}
		}

		public static void SendMessage_ConsoleCommand(this Connection[] connections, string command)
		{
			if (connections.Length == 0)
				return;
			if (NetworkManager.Instance.BaseServer.write.Start())
			{
				NetworkManager.Instance.BaseServer.write.PacketID(Message.Type.ConsoleCommand);
				NetworkManager.Instance.BaseServer.write.String(command);
				NetworkManager.Instance.BaseServer.write.Send(new SendInfo(connections));
			}
		}

		public static void SendMessage_ConsoleCommand(this List<Connection> connections, string command)
		{
			if (connections.Count == 0)
				return;
			if (NetworkManager.Instance.BaseServer.write.Start())
			{
				NetworkManager.Instance.BaseServer.write.PacketID(Message.Type.ConsoleCommand);
				NetworkManager.Instance.BaseServer.write.String(command);
				NetworkManager.Instance.BaseServer.write.Send(new SendInfo(connections));
			}
		}

		#region [Methods] SendMessage_ChatMessage

		public static void SendMessage_ChatMessage(this Connection connection, string message, string ownerName = "<color=orange>Server</color>", ulong ownerID = 0)
		{
			if (connection == null)
				return;
			if (NetworkManager.Instance.BaseServer.write.Start())
            {
            	NetworkManager.Instance.BaseServer.write.PacketID(Message.Type.ConsoleCommand);
            	NetworkManager.Instance.BaseServer.write.String($"chat.add2 \"{ownerID}\" \"{message}\" \"{ownerName}\"");
            	NetworkManager.Instance.BaseServer.write.Send(new SendInfo(connection));
            }
		}

		public static void SendMessage_ChatMessage(this Connection[] connections, string message, string ownerName = "<color=orange>Server</color>", ulong ownerID = 0)
		{
			if (connections.Length == 0)
				return;
			if (NetworkManager.Instance.BaseServer.write.Start())
			{
				NetworkManager.Instance.BaseServer.write.PacketID(Message.Type.ConsoleCommand);
				NetworkManager.Instance.BaseServer.write.String($"chat.add2 \"{ownerID}\" \"{message}\" \"{ownerName}\"");
				NetworkManager.Instance.BaseServer.write.Send(new SendInfo(connections));
			}
		}

		public static void SendMessage_ChatMessage(this List<Connection> connections, string message, string ownerName = "<color=orange>Server</color>", ulong ownerID = 0)
		{
			if (connections.Count == 0)
				return;
			if (NetworkManager.Instance.BaseServer.write.Start())
			{
				NetworkManager.Instance.BaseServer.write.PacketID(Message.Type.ConsoleCommand);
				NetworkManager.Instance.BaseServer.write.String($"chat.add2 \"{ownerID}\" \"{message}\" \"{ownerName}\"");
				NetworkManager.Instance.BaseServer.write.Send(new SendInfo(connections));
			}
		}

		#endregion

		#region [Methods] SendMessage_EntityFlags

		public static void SendMessage_EntityFlags(this Connection connection, uint uid, EEntityFlags flags)
		{
			if (connection == null)
				return;
			if (NetworkManager.Instance.BaseServer.write.Start())
			{
				NetworkManager.Instance.BaseServer.write.PacketID(Message.Type.EntityFlags);
				NetworkManager.Instance.BaseServer.write.UInt32(uid);
				NetworkManager.Instance.BaseServer.write.Int32((int)flags);
				NetworkManager.Instance.BaseServer.write.Send(new SendInfo(connection));
			}
		}
		
		public static void SendMessage_EntityFlags(this List<Connection> connections, uint uid, EEntityFlags flags)
		{
			if (connections.Count == 0)
				return;
			if (NetworkManager.Instance.BaseServer.write.Start())
			{
				NetworkManager.Instance.BaseServer.write.PacketID(Message.Type.EntityFlags);
				NetworkManager.Instance.BaseServer.write.UInt32(uid);
				NetworkManager.Instance.BaseServer.write.Int32((int)flags);
				NetworkManager.Instance.BaseServer.write.Send(new SendInfo(connections));
			}
		}
		
		public static void SendMessage_EntityFlags(this Connection[] connections, uint uid, EEntityFlags flags)
		{
			if (connections.Length == 0)
				return;
			if (NetworkManager.Instance.BaseServer.write.Start())
			{
				NetworkManager.Instance.BaseServer.write.PacketID(Message.Type.EntityFlags);
				NetworkManager.Instance.BaseServer.write.UInt32(uid);
				NetworkManager.Instance.BaseServer.write.Int32((int)flags);
				NetworkManager.Instance.BaseServer.write.Send(new SendInfo(connections));
			}
		}
		
		public static void SendMessage_EntityFlags(this object target, uint uid, EEntityFlags flags)
		{
			if (target is List<Connection> listConnections)
				listConnections.SendMessage_EntityFlags(uid, flags);
			if (target is Connection[] arrayConnections)
				arrayConnections.SendMessage_EntityFlags(uid, flags);
			else if (target is Connection connection)
				connection.SendMessage_EntityFlags(uid, flags);
		}
		#endregion

		#region [Methods] SendMessage_Effect

		public static void SendMessage_Effect(this Connection connection, Effect effect)
		{
			if (connection == null)
				return;
			if (NetworkManager.Instance.BaseServer.write.Start())
			{
				NetworkManager.Instance.BaseServer.write.PacketID(Message.Type.Effect);
				effect.WriteToStream(NetworkManager.Instance.BaseServer.write);
				NetworkManager.Instance.BaseServer.write.Send(new SendInfo(connection));
			}
		}

		public static void SendMessage_Effect(this Connection[] connections, Effect effect)
		{
			if (connections.Length == 0)
				return;
			if (NetworkManager.Instance.BaseServer.write.Start())
			{
				NetworkManager.Instance.BaseServer.write.PacketID(Message.Type.Effect);
				effect.WriteToStream(NetworkManager.Instance.BaseServer.write);
				NetworkManager.Instance.BaseServer.write.Send(new SendInfo(connections));
			}
		}

		public static void SendMessage_Effect(this List<Connection> connections, Effect effect)
		{
			if (connections.Count == 0)
				return;
			if (NetworkManager.Instance.BaseServer.write.Start())
			{
				NetworkManager.Instance.BaseServer.write.PacketID(Message.Type.Effect);
				effect.WriteToStream(NetworkManager.Instance.BaseServer.write);
				NetworkManager.Instance.BaseServer.write.Send(new SendInfo(connections));
			}
		}
		
		#endregion

		#region [Methods] SendMessage_EntityDestroy

		public static void SendMessage_EntityDestroy(this Connection connection, BaseNetworkable networkable, EDestroyMode destroyMode = EDestroyMode.None)
		{
			if (connection == null || (networkable is BasePlayer player && player.Net.ConnectionOwner == connection))
				return;
			if (NetworkManager.Instance.BaseServer.write.Start())
			{
				NetworkManager.Instance.BaseServer.write.PacketID(Message.Type.EntityDestroy);
				NetworkManager.Instance.BaseServer.write.EntityID(networkable.Net.UID);
				NetworkManager.Instance.BaseServer.write.UInt8((byte) destroyMode);
				NetworkManager.Instance.BaseServer.write.Send(new SendInfo(connection));
			}
		}

		public static void SendMessage_EntityDestroy(this Connection[] connections, BaseNetworkable networkable, EDestroyMode destroyMode = EDestroyMode.None)
		{
			if (connections.Length == 0)
				return;
			if (networkable is BasePlayer player && player.Net.ConnectionOwner != null)
			{
				if (connections.Contains(player.Net.ConnectionOwner))
				{
					connections = connections.Where(c => c != player.Net.ConnectionOwner).ToArray();
				}
			}

			if (NetworkManager.Instance.BaseServer.write.Start())
			{
				NetworkManager.Instance.BaseServer.write.PacketID(Message.Type.EntityDestroy);
				NetworkManager.Instance.BaseServer.write.EntityID(networkable.Net.UID);
				NetworkManager.Instance.BaseServer.write.UInt8((byte) destroyMode);
				NetworkManager.Instance.BaseServer.write.Send(new SendInfo(connections));
			}
		}

		public static void SendMessage_EntityDestroy(this List<Connection> connections, BaseNetworkable networkable, EDestroyMode destroyMode = EDestroyMode.None)
		{
			if (connections.Count == 0)
				return;
			if (networkable is BasePlayer player && player.Net.ConnectionOwner != null)
			{
				if (connections.Contains(player.Net.ConnectionOwner))
				{
					connections = connections.Where(c => c != player.Net.ConnectionOwner).ToList();
				}
			}
			if (NetworkManager.Instance.BaseServer.write.Start())
			{
				NetworkManager.Instance.BaseServer.write.PacketID(Message.Type.EntityDestroy);
				NetworkManager.Instance.BaseServer.write.EntityID(networkable.Net.UID);
				NetworkManager.Instance.BaseServer.write.UInt8((byte) destroyMode);
				NetworkManager.Instance.BaseServer.write.Send(new SendInfo(connections));
			}
		}

		#endregion

		#region [Methods] SendMessage_Entities

		public static void SendMessage_Entities(this Connection connection, Entity entity)
		{
			if (connection == null)
				return;
			if (NetworkManager.Instance.BaseServer.write.Start())
			{
				NetworkManager.Instance.BaseServer.write.PacketID(Message.Type.Entities);
				NetworkManager.Instance.BaseServer.write.UInt32(++connection.validate.entityUpdates);
				entity.WriteToStream(NetworkManager.Instance.BaseServer.write);
				NetworkManager.Instance.BaseServer.write.Send(new SendInfo(connection));
			}
		}

		public static void SendMessage_Entities(this Connection[] connections, Entity entity)
		{
			if (connections.Length == 0)
				return;
			for (var i = 0; i < connections.Length; i++)
				connections[i].SendMessage_Entities(entity);
		}

		public static void SendMessage_Entities(this List<Connection> connections, Entity entity)
		{
			if (connections.Count == 0)
				return;
			for (var i = 0; i < connections.Count; i++)
				connections[i].SendMessage_Entities(entity);
		}

		public static void SendMessage_Entities(this object target, Entity entity)
		{
			if (target is List<Connection> listConnections)
				listConnections.SendMessage_Entities(entity);
			if (target is Connection[] arrayConnections)
				arrayConnections.SendMessage_Entities(entity);
			else if (target is Connection connection)
				connection.SendMessage_Entities(entity);
		}

		#endregion

		#region [Methods] SendMessage_Position

		public static void SendMessage_Position(this Connection connection, BaseEntity entity)
		{
			if (connection == null)
				return;
			if (NetworkManager.Instance.BaseServer.write.Start())
			{
				NetworkManager.Instance.BaseServer.write.PacketID(Message.Type.EntityPosition);
				NetworkManager.Instance.BaseServer.write.EntityID(entity.Net.UID);
				NetworkManager.Instance.BaseServer.write.Vector3(entity.Position);
				NetworkManager.Instance.BaseServer.write.Vector3(entity.Rotation);
				NetworkManager.Instance.BaseServer.write.Float(0);
				NetworkManager.Instance.BaseServer.write.Send(new SendInfo(connection));
			}
		}

		public static void SendMessage_Position(this Connection[] connections, BaseEntity entity)
		{
			if (connections.Length == 0)
				return;
			if (NetworkManager.Instance.BaseServer.write.Start())
			{
				NetworkManager.Instance.BaseServer.write.PacketID(Message.Type.EntityPosition);
				NetworkManager.Instance.BaseServer.write.EntityID(entity.Net.UID);
				NetworkManager.Instance.BaseServer.write.Vector3(entity.Position);
				NetworkManager.Instance.BaseServer.write.Vector3(entity.Rotation);
				NetworkManager.Instance.BaseServer.write.Float(0);
				NetworkManager.Instance.BaseServer.write.Send(new SendInfo(connections));
			}
		}

		public static void SendMessage_Position(this List<Connection> connections, BaseEntity entity)
		{
			if (connections.Count == 0)
				return;
			if (NetworkManager.Instance.BaseServer.write.Start())
			{
				NetworkManager.Instance.BaseServer.write.PacketID(Message.Type.EntityPosition);
				NetworkManager.Instance.BaseServer.write.EntityID(entity.Net.UID);
				NetworkManager.Instance.BaseServer.write.Vector3(entity.Position);
				NetworkManager.Instance.BaseServer.write.Vector3(entity.Rotation);
				NetworkManager.Instance.BaseServer.write.Float(0);
				NetworkManager.Instance.BaseServer.write.Send(new SendInfo(connections));
			}
		}

		public static void SendMessage_Position(this object target, BaseEntity entity)
		{
			if (target is List<Connection> listConnections)
				listConnections.SendMessage_Position(entity);
			if (target is Connection[] arrayConnections)
				arrayConnections.SendMessage_Position(entity);
			else if (target is Connection connection)
				connection.SendMessage_Position(entity);
		}

		#endregion

		#region [Methods] SendMessage_RPCMessage

		public static void SendMessage_RPCMessage<T1, T2, T3, T4, T5>(this BaseNetworkable networkable, SendInfo sendInfo, Connection sourceConnection, ERPCMessage rpcMethod, T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5)
		{
			if (ClientRPCStart(networkable, sourceConnection, rpcMethod))
			{
				ClientRPCWrite<T1>(arg1);
				ClientRPCWrite<T2>(arg2);
				ClientRPCWrite<T3>(arg3);
				ClientRPCWrite<T4>(arg4);
				ClientRPCWrite<T5>(arg5);
				ClientRPCSend(sendInfo);
			}
		}
		
		public static void SendMessage_RPCMessage<T1, T2, T3, T4>(this BaseNetworkable networkable, SendInfo sendInfo, Connection sourceConnection, ERPCMessage rpcMethod, T1 arg1, T2 arg2, T3 arg3, T4 arg4)
		{
			if (ClientRPCStart(networkable, sourceConnection, rpcMethod))
			{
				ClientRPCWrite<T1>(arg1);
				ClientRPCWrite<T2>(arg2);
				ClientRPCWrite<T3>(arg3);
				ClientRPCWrite<T4>(arg4);
				ClientRPCSend(sendInfo);
			}
		}
		
		public static void SendMessage_RPCMessage<T1, T2, T3>(this BaseNetworkable networkable, SendInfo sendInfo, Connection sourceConnection, ERPCMessage rpcMethod, T1 arg1, T2 arg2, T3 arg3)
		{
			if (ClientRPCStart(networkable, sourceConnection, rpcMethod))
			{
				ClientRPCWrite<T1>(arg1);
				ClientRPCWrite<T2>(arg2);
				ClientRPCWrite<T3>(arg3);
				ClientRPCSend(sendInfo);
			}
		}
		
		public static void SendMessage_RPCMessage<T1, T2>(this BaseNetworkable networkable, SendInfo sendInfo, Connection sourceConnection, ERPCMessage rpcMethod, T1 arg1, T2 arg2)
		{
			if (ClientRPCStart(networkable, sourceConnection, rpcMethod))
			{
				ClientRPCWrite<T1>(arg1);
				ClientRPCWrite<T2>(arg2);
				ClientRPCSend(sendInfo);
			}
		}
		
		public static void SendMessage_RPCMessage<T1>(this BaseNetworkable networkable, SendInfo sendInfo, Connection sourceConnection, ERPCMessage rpcMethod, T1 arg1)
		{
			if (ClientRPCStart(networkable, sourceConnection, rpcMethod))
			{
				ClientRPCWrite<T1>(arg1);
				ClientRPCSend(sendInfo);
			}
		}
		
		public static void SendMessage_RPCMessage(this BaseNetworkable networkable, SendInfo sendInfo, Connection sourceConnection, ERPCMessage rpcMethod)
		{
			if (ClientRPCStart(networkable, sourceConnection, rpcMethod))
			{
				ClientRPCSend(sendInfo);
			}
		}

		private static bool ClientRPCStart(BaseNetworkable networkable, Connection sourceConnection, ERPCMessage rpcMethod)
		{
			if (NetworkManager.Instance.BaseServer.write.Start())
			{
				NetworkManager.Instance.BaseServer.write.PacketID(Message.Type.RPCMessage);
				NetworkManager.Instance.BaseServer.write.UInt32(networkable.Net.UID);
				NetworkManager.Instance.BaseServer.write.UInt32((uint)rpcMethod);
				NetworkManager.Instance.BaseServer.write.UInt64((sourceConnection == null) ? 0UL : sourceConnection.userid);
				return true;
			}

			return false;
		}

		private static void ClientRPCWrite<T>(T arg)
		{
			NetworkManager.Instance.BaseServer.write.WriteObject(arg);
		}

		private static void ClientRPCSend(SendInfo sendInfo)
		{
			NetworkManager.Instance.BaseServer.write.Send(sendInfo);
		}
		#endregion
	}
}