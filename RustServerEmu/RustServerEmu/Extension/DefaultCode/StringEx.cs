using System;
using System.Collections.Generic;

namespace RustServerEmu.Extension.DefaultCode
{
    public static class StringEx
    {
        public static IEnumerable<string> SplitToChunks(this string str, int chunkLength)
        {
            if (string.IsNullOrEmpty(str))
            {
                throw new ArgumentException("string cannot be null");
            }
            if (chunkLength < 1)
            {
                throw new ArgumentException("chunk length needs to be more than 0");
            }
            for (int i = 0; i < str.Length; i += chunkLength)
            {
                if (chunkLength + i >= str.Length)
                {
                    chunkLength = str.Length - i;
                }
                yield return str.Substring(i, chunkLength);
            }
            yield break;
        }
    }
}