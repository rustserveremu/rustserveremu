using Network;
using RustServerEmu.Data;
using RustServerEmu.GObject.GameObject;
using UnityEngine;

namespace RustServerEmu.Extension.ProtoBuf
{
	public class Effect : EffectData
	{
		public static Effect Instance { get;  } = new Effect();

		public void Init(EPrefabID prefab, EEffectType type, Vector3 position, Vector3 normal, ulong source)
		{
			this.Clear();
			this.pooledstringid = (uint) prefab;
			this.type = (uint) type;
			this.origin = position;
			this.normal = normal;
			this.source = source;
		}
		
		
		public void InitFromPrefabID(uint prefabID, EEffectType type, Vector3 position, Vector3 normal, ulong source)
		{
			this.Clear();
			this.pooledstringid = (uint) prefabID;
			this.type = (uint) type;
			this.origin = position;
			this.normal = normal;
			this.source = source;
		}
		

		public void Clear()
		{
			this.pooledstringid = 0;
			this.origin = Vector3.zero;
			this.normal = Vector3.zero;
			this.scale = 0;
			this.entity = 0;
			this.bone = 0;
			this.source = 0;
			this.type = 0;
			this.number = 0;
		}
	}

}