using RustServerEmu.Data;
using RustServerEmu.GObject.Component;

namespace RustServerEmu.Extension
{
    public static class ItemContainerHandler
    {
        public static bool HasItemContainerFlag(this ItemContainer container, EItemContainerFlags f) => ((container.ItemContainerFlags & f) == f);

        public static void SetItemContainerFlag(this ItemContainer container, EItemContainerFlags f, bool b)
        {
            if (b)
            {
                if (container.HasItemContainerFlag(f) == false)
                    container.ItemContainerFlags |= f;
            }
            else 
            {
                if (container.HasItemContainerFlag(f))
                    container.ItemContainerFlags &= ~f;
            }
        }
    }
}