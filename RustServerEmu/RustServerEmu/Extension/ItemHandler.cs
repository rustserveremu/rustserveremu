using RustServerEmu.Data;
using RustServerEmu.GObject.Component;

namespace RustServerEmu.Extension
{
    public static class ItemHandler
    {
        public static bool HasItemFlag(this Item item, EItemFlags f) => ((item.ItemFlags & f) == f);

        public static void SetItemFlag(this Item item, EItemFlags f, bool b)
        {
            if (b)
            {
                if (item.HasItemFlag(f) == false)
                    item.ItemFlags |= f;
            }
            else 
            {
                if (item.HasItemFlag(f))
                    item.ItemFlags &= ~f;
            }
        }
    }
}