using RustServerEmu.AppEnvironment;
using RustServerEmu.GObject.GameObject;

namespace RustServerEmu.Extension
{
    public static class NetworkableHandler
    {
        public static T GetNetworkable<T>(this uint uid) where T : BaseNetworkable
        {
            if (NetworkManager.Instance.ListSpawnedNetworkables.TryGetValue(uid, out BaseNetworkable networkable) && networkable is T)
                return (T)networkable;
            return null;
        }

        public static BaseNetworkable GetNetworkable(this uint uid)
        {
            if (NetworkManager.Instance.ListSpawnedNetworkables.TryGetValue(uid, out BaseNetworkable networkable))
                return networkable;
            return null;
        }
    }
}