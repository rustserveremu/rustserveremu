using System;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;
using RustServerEmu.AppEnvironment;
using RustServerEmu.Data;
using Tefor.Engine;

namespace RustServerEmu.Database
{
    public class BoneDefinition
    {
        public static Dictionary<uint, BoneDefinition> ListBones { get; private set; } = new Dictionary<uint, BoneDefinition>();

        static void Init()
        {
            try
            {
                string filePath = Path.GetFullPath(AppDomain.CurrentDomain.BaseDirectory + AppManager.Instance.Settings.Path_DatabaseDir + "/Bone.json");
                if (File.Exists(filePath) == false)
                {
                    BoneDefinition defaultItem = new BoneDefinition();
                    ListBones.Add(defaultItem.BoneID, defaultItem);
                    File.WriteAllText(filePath, JsonConvert.SerializeObject(ListBones, Formatting.Indented));
                }
                else
                    ListBones = JsonConvert.DeserializeObject<Dictionary<uint, BoneDefinition>>(File.ReadAllText(filePath));

                Debug.LogSuccessful($"[BoneDefinition::Init] Loaded {ListBones.Count} count bullet!");
            }
            catch (Exception ex)
            {
                Debug.LogSuccessful($"[BoneDefinition::Init] Exception: ");
                Debug.LogException(ex);
            }
        }

        [JsonProperty("boneID")] public uint BoneID { get; set; }
        [JsonProperty("boneName")] public string BoneName { get; set; } = "";
        [JsonProperty("hitArea")] public EHitArea HitArea { get; set; } = EHitArea.Arm;
        [JsonProperty("damageScale")] public float damageScale { get; set; } = 1f;
    }
}