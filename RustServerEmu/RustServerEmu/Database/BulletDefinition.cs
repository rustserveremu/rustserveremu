using System;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;
using RustServerEmu.AppEnvironment;
using RustServerEmu.Data;
using Tefor.Engine;

namespace RustServerEmu.Database
{
    public class BulletDefinition
    {
        public static Dictionary<int, BulletDefinition> ListBullets { get; private set; } = new Dictionary<int, BulletDefinition>();

        static void Init()
        {
            try
            {
                string filePath = Path.GetFullPath(AppDomain.CurrentDomain.BaseDirectory + AppManager.Instance.Settings.Path_DatabaseDir + "/Bullet.json");
                if (File.Exists(filePath) == false)
                {
                    BulletDefinition defaultItem = new BulletDefinition();
                    ListBullets.Add(defaultItem.ItemID, defaultItem);
                    File.WriteAllText(filePath, JsonConvert.SerializeObject(ListBullets, Formatting.Indented));
                }
                else
                    ListBullets = JsonConvert.DeserializeObject<Dictionary<int, BulletDefinition>>(File.ReadAllText(filePath));

                Debug.LogSuccessful($"[BulletDefinition::Init] Loaded {ListBullets.Count} count bullet!");
            }
            catch (Exception ex)
            {
                Debug.LogSuccessful($"[BulletDefinition::Init] Exception: ");
                Debug.LogException(ex);
            }
        }

        [JsonProperty("itemID")] public int ItemID { get; set; }
        [JsonProperty("ammoType")] public EAmmoTypes AmmoType { get; set; }
        [JsonProperty("baseDamage")] public float BaseDamage { get; set; }
    }
}