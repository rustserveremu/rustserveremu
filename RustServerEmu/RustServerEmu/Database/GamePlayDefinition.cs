using System;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;
using RustServerEmu.GObject.GameObject;
using UnityEngine;
using Debug = Tefor.Engine.Debug;
using Random = System.Random;

namespace RustServerEmu.Database
{
    public class GamePlayDefinition
    {
        public static GamePlayDefinition Instance { get; private set; } = new GamePlayDefinition();

        private static Random RandomInstance { get; } = new Random();

        public static SpawnDefinition GetRandomSpawn(BasePlayer playerOwner = null)
        {
            try
            {
                int randomValue = RandomInstance.Next(0, Instance.SpawnList.Count);
                return Instance.SpawnList[randomValue];
            }
            catch (Exception ex)
            {
                Debug.LogError("[GamePlayDefinition::GetRandomSpawn] Exception: ");
                Debug.LogException(ex);
            }

            Debug.LogWarning("[GamePlayDefinition::GetRandomSpawn] Not found Any spawn!");
            return new SpawnDefinition();
        }

        public static StartInventoryDefinition GetRandomStartInventory()
        {
            try
            {
                int randomValue = RandomInstance.Next(0, Instance.StartInventory.Count);
                return Instance.StartInventory[randomValue];
            }
            catch (Exception ex)
            {
                Debug.LogError("[GamePlayDefinition::GetRandomStartInventory] Exception: ");
                Debug.LogException(ex);
            }

            Debug.LogWarning("[GamePlayDefinition::GetRandomStartInventory] Not found Any StartInventory!");
            return new StartInventoryDefinition();
        }
        
        static void Init()
        {
            Instance = new GamePlayDefinition();
            
            if (Directory.Exists(AppDomain.CurrentDomain.BaseDirectory + "/Server/"))
            {
                try
                {
                    if (File.Exists(AppDomain.CurrentDomain.BaseDirectory + "/Server/GamePlay.json"))
                    {
                        string content = File.ReadAllText(AppDomain.CurrentDomain.BaseDirectory + "/Server/GamePlay.json");
                        Instance = JsonConvert.DeserializeObject<GamePlayDefinition>(content);
                        Debug.LogSuccessful($"[GamePlayDefinition::Init] Loading GamePlay has been success!");
                        return;
                    }
                }
                catch (Exception ex)
                {
                    Debug.LogSuccessful($"[GamePlayDefinition::Init] Exception: ");
                    Debug.LogException(ex);
                }
            }
            else
                Directory.CreateDirectory(AppDomain.CurrentDomain.BaseDirectory + "/Server/");
            
            Instance.Players.Add(76561198204356551, new PlayerDefinition());
            Instance.SpawnList.Add(new SpawnDefinition());
            Instance.StartInventory.Add(new StartInventoryDefinition());
            Instance.StartInventory[0].ContainerBelt.Add(new StartInventoryItemDefinition());
            File.WriteAllText(AppDomain.CurrentDomain.BaseDirectory + "/Server/GamePlay.json", JsonConvert.SerializeObject(Instance, Formatting.Indented));
            Debug.LogWarning("[GamePlayDefinition::Init] Not loaded you save, create default GamePlay in /Server/GamePlay.json");
        }

        public static void Save()
        {
            try
            {
                File.WriteAllText(AppDomain.CurrentDomain.BaseDirectory + "/Server/GamePlay.json", JsonConvert.SerializeObject(Instance, Formatting.Indented));
                Debug.LogSuccessful("[GamePlayDefinition::Save] GamePlay base has been success!");
            }
            catch (Exception ex)
            {
                Debug.LogSuccessful($"[GamePlayDefinition::Save] Exception: ");
                Debug.LogException(ex);
            }
        }

        [JsonProperty("spawnList")] public List<SpawnDefinition> SpawnList { get; set; } = new List<SpawnDefinition>();
        [JsonProperty("startInventory")] public List<StartInventoryDefinition> StartInventory { get; set; } = new List<StartInventoryDefinition>();
        [JsonProperty("startMessage")] public string StartMessage { get; set; } = "Добро пожаловать к нам на сервер!";
        [JsonProperty("gamePlayConfiguration")]  public GamePlayConfiguration Configuration { get; set; } = new GamePlayConfiguration();
        [JsonProperty("players")] public Dictionary<ulong, PlayerDefinition> Players { get; set; } = new Dictionary<ulong, PlayerDefinition>();

        
        public class GamePlayConfiguration
        {
            [JsonProperty("player.woundedEnable")] public bool PlayerWoundedEnable { get; set; } = true;
            [JsonProperty("player.woundedTime")] public int PlayerWoundedTime { get; set; } = 40;
            [JsonProperty("player.woundedDelay")] public int PlayerWoundedDelay { get; set; } = 60;
            [JsonProperty("player.woundedIfHeadshot")] public bool PlayerWoundedIfHeadshot { get; set; } = false;

            [JsonProperty("player.accessGiveItem")] public bool PlayerAccessGiveItem { get; set; } = false;
            [JsonProperty("player.giveItemToChatLog")] public bool PlayerGiveItemToChatLog { get; set; } = true;

            [JsonProperty("datetime.scale")] public float DatetimeScale { get; } = 12;
            [JsonProperty("datetime.startHour")] public int DatetimeStartHour = 10;

            [JsonProperty("info.killInChat")] public bool InfoKillInChat { get; set; } = true;
            [JsonProperty("info.connectionsInChat")] public bool InfoConnectionsInChat { get; set; } = true;

            [JsonProperty("player.approvalPirate")] public bool PlayerApprovalPirate { get; set; } = true;
        }

        public class PlayerDefinition
        {
            [JsonProperty("steamID")] public ulong SteamID { get; set; } = 76561198204356551;
            [JsonProperty("authLevel")] public uint AuthLevel { get; set; } = 1;
            [JsonProperty("chatPrefix")] public string ChatPrefix { get; set; } = "Developer";
            [JsonProperty("chatPrefixColor")] public string ChatPrefixColor { get; set; } = "#fff";
        }
        
        public class SpawnDefinition
        {
            private static int TakeSpawnID = 0;
            [JsonProperty("spawnID")] public int SpawnID { get; set; } = ++TakeSpawnID;
            [JsonProperty("spawnName")] public string SpawnName { get; set; } = "DefaultSpawn";
            [JsonProperty("position")] public Vector3 Position { get; set; } = new Vector3(0, 215, 0);
        }

        public class StartInventoryDefinition
        {
            [JsonProperty("containerBelt")] public List<StartInventoryItemDefinition> ContainerBelt { get; set; } = new List<StartInventoryItemDefinition>();
            [JsonProperty("containerMain")] public List<StartInventoryItemDefinition> ContainerMain { get; set; } = new List<StartInventoryItemDefinition>();
        }

        public class StartInventoryItemDefinition
        {
            [JsonProperty("itemID")] public int ItemID { get; set; } = 1545779598;
            [JsonProperty("amount")] public int Amount { get; set; } = 1;
            [JsonProperty("skitID")] public ulong SkinID { get; set; } = 0;
            [JsonProperty("amountBullet")] public int AmountBullet { get; set; } = 0;
        }
    }
}