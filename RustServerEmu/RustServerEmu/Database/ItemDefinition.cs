using System;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;
using RustServerEmu.AppEnvironment;
using RustServerEmu.Data;
using Tefor.Engine;

namespace RustServerEmu.Database
{
    public class ItemDefinition
    {
        public static Dictionary<int, ItemDefinition> ListItems { get; private set; } = new Dictionary<int, ItemDefinition>();

        static void Init()
        {
            try
            {
                string filePath = Path.GetFullPath(AppDomain.CurrentDomain.BaseDirectory + AppManager.Instance.Settings.Path_DatabaseDir + "/Items.json");
                if (File.Exists(filePath) == false)
                {
                    ItemDefinition defaultItem = new ItemDefinition();
                    ListItems.Add(defaultItem.ItemID, defaultItem);
                    File.WriteAllText(filePath, JsonConvert.SerializeObject(ListItems, Formatting.Indented));
                }
                else
                    ListItems = JsonConvert.DeserializeObject<Dictionary<int, ItemDefinition>>(File.ReadAllText(filePath));

                Debug.LogSuccessful($"[ItemDefinition::Init] Loaded {ListItems.Count} count items!");
            }catch (Exception ex)
            {
                Debug.LogSuccessful($"[ItemDefinition::Init] Exception: ");
                Debug.LogException(ex);
            }
        }
        
        [JsonProperty("itemid")] public int ItemID { get; set; }

        [JsonProperty("typeItem")] public string TypeItem { get;  set; }

        [JsonProperty("shortname")] public string ShortName { get; set; }

        [JsonProperty("amountType")] public EAmountType AmountType { get; set; }

        [JsonProperty("category")] public EItemCategory Category { get; set; }

        [JsonProperty("contentsType")] public EContantsType ContentsType { get; set; }

        [JsonProperty("traitsFlag")] public ETraitFlag TraitsFlag { get; set; }

        [JsonProperty("displayName")] public string DisplayName { get; set; }

        [JsonProperty("parentID")] public int ParentItemID { get; set; }

        [JsonProperty("stackable")] public int Stackable { get; set; }

        [JsonProperty("worldEntityPrefabID")] public uint WorldEntityPrefabID { get; set; }

        [JsonProperty("condition")] public int Condition { get; set; }

        [JsonProperty("hidden")] public bool Hidden { get; set; }

        [JsonProperty("heldDefinition")] public HeldEntityInfo HeldDefinition { get; set; }


        [JsonProperty("itemComponents")] public ItemComponents ItemComponentDefinitions { get; set; }

        public class ItemComponents
        {
            [JsonProperty("alterCondition")] public ItemComponentAlterCondition AlterCondition { get; set; }
            [JsonProperty("blueprintCraft")] public ItemComponentBlueprintCraft BlueprintCraft { get; set; }
            [JsonProperty("burnable")] public ItemComponentBurnable Burnable { get; set; }
            [JsonProperty("conditionContainerFlag")] public ItemComponentConditionContainerFlag ConditionContainerFlag { get; set; }
            [JsonProperty("conditionHasContents")] public ItemComponentConditionHasContents ConditionHasContents { get; set; }
            [JsonProperty("conditionHasFlag")] public ItemComponentConditionHasFlag ConditionHasFlag { get; set; }
            [JsonProperty("conditionInWater")] public ItemComponentConditionInWater ConditionInWater { get; set; }
            [JsonProperty("consume")] public ItemComponentConsume Consume { get; set; }
            [JsonProperty("consumable")] public ItemComponentConsumable Consumable { get; set; }
            [JsonProperty("consumeContents")] public ItemComponentConsumeContents ConsumeContents { get; set; }
            [JsonProperty("container")] public ItemComponentContainer Container { get; set; }
            [JsonProperty("cookable")] public ItemComponentCookable Cookable { get; set; }
            [JsonProperty("deployable")] public ItemComponentDeployable Deployable { get; set; }
            [JsonProperty("entity")] public ItemComponentEntity Entity { get; set; }
            [JsonProperty("giveOxygen")] public ItemComponentGiveOxygen GiveOxygen { get; set; }
            [JsonProperty("keycard")] public ItemComponentKeycard Keycard { get; set; }
            [JsonProperty("projectile")] public ItemComponentProjectile Projectile { get; set; }
            [JsonProperty("recycleInto")] public ItemComponentRecycleInto RecycleInto { get; set; }
            [JsonProperty("repair")] public ItemComponentRepair Repair { get; set; }
            [JsonProperty("reveal")] public ItemComponentReveal Reveal { get; set; }
            [JsonProperty("rFListener")] public ItemComponentRFListener RFListener { get; set; }
            [JsonProperty("sound")] public ItemComponentSound Sound { get; set; }
            [JsonProperty("studyBlueprint")] public ItemComponentStudyBlueprint StudyBlueprint { get; set; }
            [JsonProperty("swap")] public ItemComponentSwap Swap { get; set; }
            [JsonProperty("unwrap")] public ItemComponentUnwrap Unwrap { get; set; }
            [JsonProperty("upgrade")] public ItemComponentUpgrade Upgrade { get; set; }
            [JsonProperty("useContent")] public ItemComponentUseContent UseContent { get; set; }
            [JsonProperty("wearable")] public ItemComponentWearable Wearable { get; set; }
        }

        public class ItemComponentAlterCondition
        {
            [JsonProperty("conditionChange")] public float ConditionChange { get; set; }
        }
        public class ItemComponentBlueprintCraft
        {
            [JsonProperty("successEffectPrefabID")] public uint SuccessEffectPrefabID { get; set; }
        }
        public class ItemComponentBurnable
        {
            [JsonProperty("fuelAmount")] public float FuelAmount { get; set; } = 10f;

            [JsonProperty("byproductItemID")] public int ByproductItemID { get; set; }
            
            [JsonProperty("byproductAmount")] public int ByproductAmount { get; set; } = 1;
            
            [JsonProperty("byproductChance")] public float ByproductChance { get; set; } = 0.5f;
        }
        public class ItemComponentConditionContainerFlag
        {
            [JsonProperty("flag")] public EItemContainerFlags Flag { get; set; }

            [JsonProperty("requiredState")] public bool RequiredState { get; set; }
        }
        public class ItemComponentConditionHasContents
        {
            [JsonProperty("itemID")] public int ItemID { get; set; }

            [JsonProperty("requiredState")] public bool RequiredState { get; set; }
        }
        public class ItemComponentConditionHasFlag
        {
            [JsonProperty("flag")] public EItemFlags Flag { get; set; }
            
            [JsonProperty("requiredState")] public bool RequiredState { get; set; }
        }
        public class ItemComponentConditionInWater
        {
            [JsonProperty("requiredState")] public bool RequiredState { get; set; }
        }
        public class ItemComponentConsumable
        {
            [JsonProperty("amountToConsume")] public int AmountToConsume { get; set; } = 1;
            
            [JsonProperty("conditionFractionToLose")] public float ConditionFractionToLose { get; set; }
            
            [JsonProperty("effects")] public List<ItemComponentConsumable.ConsumableEffect> Effects { get; set; } = new List<ItemComponentConsumable.ConsumableEffect>();
            
            public class ConsumableEffect
            {
                [JsonProperty("type")] public EMetabolismType Type { get; set; }
                
                [JsonProperty("amount")] public float Amount { get; set; }
                
                [JsonProperty("time")] public float Time { get; set; }
                
                [JsonProperty("onlyIfHealthLessThan")] public float OnlyIfHealthLessThan { get; set; } = 1f;
            }
        }
        public class ItemComponentConsume
        {
            [JsonProperty("consumeEffectPrefabID")] public uint ConsumeEffectPrefabID { get; set; }

            [JsonProperty("eatGesture")] public string EatGesture { get; set; } = "eat_2hand";
        }
        public class ItemComponentConsumeContents
        {
            [JsonProperty("consumeEffectPrefabID")] public uint ConsumeEffectPrefabID { get; set; }
        }
        public class ItemComponentContainer
        {
            [JsonProperty("capacity")] public int Capacity { get; set; } = 6;

            [JsonProperty("maxStackSize")] public int MaxStackSize { get; set; }

            [JsonProperty("containerFlags")] public EItemContainerFlags ContainerFlags { get; set; }

            [JsonProperty("onlyAllowedContents")] public EContantsType OnlyAllowedContents { get; set; } = EContantsType.Generic;

            [JsonProperty("onlyAllowedItemID")] public int OnlyAllowedItemID { get; set; }

            [JsonProperty("availableSlots")] public List<EItemSlot> AvailableSlots { get; set; } = new List<EItemSlot>();

            [JsonProperty("openInDeployed")] public bool OpenInDeployed { get; set; } = true;

            [JsonProperty("openInInventory")] public bool OpenInInventory { get; set; } = true;
        }
        public class ItemComponentCookable
        {
            [JsonProperty("becomeOnCookedItemID")] public int BecomeOnCookedItemID { get; set; }

            [JsonProperty("cookTime")] public float CookTime { get; set; } = 30f;

            [JsonProperty("amountOfBecome")] public int AmountOfBecome { get; set; } = 1;

            [JsonProperty("lowTemp")] public int LowTemp { get; set; }

            [JsonProperty("highTemp")] public int HighTemp { get; set; }

            [JsonProperty("setCookingFlag")] public bool SetCookingFlag { get; set; }
        }
        public class ItemComponentDeployable
        {
            [JsonProperty("entityPrefabID")] public uint EntityPrefabID { get; set; }

            [JsonProperty("showCrosshair")] public bool ShowCrosshair { get; set; }

            [JsonProperty("unlockAchievement")] public string UnlockAchievement { get; set; }
        }
        public class ItemComponentEntity
        {
            [JsonProperty("entityPrefabID")] public uint EntityPrefabID { get; set; }

            [JsonProperty("defaultBone")] public uint DefaultBone { get; set; }
        }
        public class ItemComponentGiveOxygen
        {
            [JsonProperty("amountToConsume")] public int AmountToConsume { get; set; } = 1;

            [JsonProperty("inhaleEffectPrefabID")] public uint InhaleEffectPrefabID { get; set; }

            [JsonProperty("exhaleEffectPrefabID")] public uint ExhaleEffectPrefabID { get; set; }

            [JsonProperty("bubblesEffectPrefabID")] public uint BubblesEffectPrefabID { get; set; }
        }
        public class ItemComponentKeycard
        {
            [JsonProperty("accessLevel")] public int AccessLevel { get; set; }
        }
        public class ItemComponentProjectile
        {
            [JsonProperty("projectileObjectPrefabID")] public uint ProjectileObjectPrefabID { get; set; }

            [JsonProperty("ammoType")] public EAmmoTypes AmmoType { get; set; }

            [JsonProperty("numProjectiles")] public int NumProjectiles { get; set; } = 1;

            [JsonProperty("projectileSpread")] public float ProjectileSpread { get; set; }

            [JsonProperty("projectileVelocity")] public float ProjectileVelocity { get; set; } = 100f;

            [JsonProperty("projectileVelocitySpread")] public float ProjectileVelocitySpread { get; set; }

            [JsonProperty("useCurve")] public bool UseCurve { get; set; }

            [JsonProperty("attackEffectOverridePrefabID")] public uint AttackEffectOverridePrefabID { get; set; }

            [JsonProperty("barrelConditionLoss")] public float BarrelConditionLoss { get; set; }

            [JsonProperty("category")] public string Category { get; set; } = "bullet";
            [JsonProperty("damageTypes")] public List<DamageTypeInfo> DamageTypes { get; set; } = new List<DamageTypeInfo>();
        }
        public class ItemComponentRecycleInto
        {
            [JsonProperty("recycleIntoItemID")] public int RecycleIntoItemID { get; set; }

            [JsonProperty("numRecycledItemMin")] public int NumRecycledItemMin { get; set; } = 1;

            [JsonProperty("numRecycledItemMax")] public int NumRecycledItemMax { get; set; } = 1;

            [JsonProperty("successEffectPrefabID")] public uint SuccessEffectPrefabID { get; set; }
        }
        public class ItemComponentRepair
        {
            [JsonProperty("conditionLost")] public float ConditionLost { get; set; } = 0.05f;

            [JsonProperty("successEffectPrefabID")] public uint SuccessEffectPrefabID { get; set; }

            [JsonProperty("workbenchLvlRequired")] public int WorkbenchLvlRequired { get; set; }
        }
        public class ItemComponentReveal
        {
            [JsonProperty("numForReveal")] public int NumForReveal { get; set; } = 10;

            [JsonProperty("revealedItemOverrideItemID")] public int RevealedItemOverrideItemID { get; set; }

            [JsonProperty("revealedItemAmount")] public int RevealedItemAmount { get; set; } = 1;

            [JsonProperty("successEffectPrefabID")] public uint SuccessEffectPrefabID { get; set; }
        }
        public class ItemComponentRFListener
        {
            [JsonProperty("frequencyPanelPrefabID")] public uint FrequencyPanelPrefabID { get; set; }

            [JsonProperty("entityPrefabID")] public uint EntityPrefabID { get; set; }
        }
        public class ItemComponentSound
        {
            [JsonProperty("effectPrefabID")] public uint EffectPrefabID { get; set; }
        }
        public class ItemComponentStudyBlueprint
        {
            [JsonProperty("studyEffectPrefabID")] public uint StudyEffectPrefabID { get; set; }
        }
        public class ItemComponentSwap
        {
            [JsonProperty("actionEffectPrefabID")] public uint ActionEffectPrefabID { get; set; }

            [JsonProperty("sendPlayerPickupNotification")] public bool SendPlayerPickupNotification { get; set; }

            [JsonProperty("sendPlayerDropNotification")] public bool SendPlayerDropNotification { get; set; }

            [JsonProperty("xpScale")] public float XpScale { get; set; } = 1f;
        }
        public class ItemComponentUnwrap
        {
            [JsonProperty("successEffectPrefabID")] public uint SuccessEffectPrefabID { get; set; }

            [JsonProperty("minTries")] public int MinTries { get; set; } = 1;

            [JsonProperty("maxTries")] public int MaxTries { get; set; } = 1;
        }
        public class ItemComponentUpgrade
        {
            [JsonProperty("numForUpgrade")] public int NumForUpgrade { get; set; } = 10;

            [JsonProperty("upgradeSuccessChance")] public float UpgradeSuccessChance { get; set; } = 1f;

            [JsonProperty("numToLoseOnFail")] public int NumToLoseOnFail { get; set; } = 2;

            [JsonProperty("upgradedItemID")] public int UpgradedItemID { get; set; }

            [JsonProperty("numUpgradedItem")] public int NumUpgradedItem { get; set; } = 1;

            [JsonProperty("successEffectPrefabID")] public uint SuccessEffectPrefabID { get; set; }

            [JsonProperty("failEffectPrefabID")] public uint FailEffectPrefabID { get; set; }
        }
        public class ItemComponentUseContent
        {
            [JsonProperty("amountToConsume")] public int AmountToConsume { get; set; } = 1;
        }
        public class ItemComponentWearable
        {
            [JsonProperty("entityPrefabID")] public uint EntityPrefabID { get; set; }

            [JsonProperty("protections")] public float[] Protections { get; set; }
            
            [JsonProperty("protectionDensity")] public float ProtectionDensity { get; set; }

            [JsonProperty("armorArea")] public EHitArea ArmorArea { get; set; }

            [JsonProperty("blocksAiming")] public bool BlocksAiming { get; set; }

            [JsonProperty("emissive")] public bool Emissive { get; set; }

            [JsonProperty("accuracyBonus")] public float AccuracyBonus { get; set; }

            [JsonProperty("blocksEquipping")] public bool BlocksEquipping { get; set; }

            [JsonProperty("eggVision")] public float EggVision { get; set; }

            [JsonProperty("viewmodelAdditionPrefabID")] public uint ViewmodelAdditionPrefabID { get; set; }
        }

        public class DamageTypeInfo
        {
            [JsonProperty("damage")] public float Damage { get; set; }
            [JsonProperty("type")] public EDamageType Type { get; set; }
        }

        public class HeldEntityInfo
        {
            [JsonProperty("typeHeldEntity")] public string TypeHeldEntity { get; set; }
            [JsonProperty("parentBone")] public uint ParentBone { get; set; }
            [JsonProperty("prefabID")] public uint PrefabID { get; set; }
            [JsonProperty("deployDelay")] public float DeployDelay{ get; set; }    
            [JsonProperty("meleeInfo")] public BaseMeleeInfo MeleeDefinition { get; set; }
            [JsonProperty("rangeInfo")] public BaseRangeInfo RangeDefinition { get; set; }
            [JsonProperty("thrownInfo")] public ThrownWeaponInfo ThrownDefinition { get; set; }
            [JsonProperty("medicalInfo")] public MedicalToolInfo MedicalDefinition { get; set; }
        }

        public class BaseMeleeInfo
        {
            [JsonProperty("distance")] public float Distance { get; set; }
            [JsonProperty("canProjectile")] public bool CanProjectile { get; set; }
            [JsonProperty("onlyProjectile")] public bool OnlyProjectile { get; set; }
            [JsonProperty("damageTypes")] public System.Collections.Generic.List<DamageTypeInfo> DamageTypes { get; set; }
        }

        public class BaseRangeInfo
        {
            [JsonProperty("canAutomatic")] public bool CanAutomatic { get; set; }
            [JsonProperty("damageScale")] public float DamageScale { get; set; }
            [JsonProperty("distanceScale")] public float DistanceScale { get; set; }
            [JsonProperty("magazineDefinition")] public MagazineInfo MagazineDefinition { get; set; }
            [JsonProperty("reloadTime")] public float ReloadTime { get; set; }
        }

        public class MagazineInfo
        {
            [JsonProperty("ammoType")] public EAmmoTypes AmmoType { get; set; }
            [JsonProperty("builtInSize")] public int BuiltInSize{ get; set; }
            [JsonProperty("capacity")] public int Capacity { get; set; }
            [JsonProperty("defaultItem")] public int ItemID { get; set; }
            [JsonProperty("contents")] public int Contents { get; set; }
        }

        public class ThrownWeaponInfo
        {
            [JsonProperty("angle_x")] public float AngleX { get; set; }
            [JsonProperty("angle_y")] public float AngleY { get; set; }
            [JsonProperty("angle_z")] public float AngleZ { get; set; }
            [JsonProperty("prefabID")] public uint PrefabID { get; set; }
        }

        public class MedicalToolInfo
        {
            [JsonProperty("canRevive")] public bool CanRevive { get; set; }
        }
    }
}