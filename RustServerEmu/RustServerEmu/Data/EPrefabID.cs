namespace RustServerEmu.Data
{
    public enum EPrefabID : uint
    {
        BasePlayer = 4108440852,
        Effect_BasePlayer_FallDamage = 2602853624,
        BaseEnvironment = 3134327264,
        DefaultWorldItemEntity_GenericWorld = 3255145925,
        Effect_ItemBreak = 2184296839,
        Effect_TakeDamageHit = 2504360139,
        Effect_Headshot = 2911953017,
        BasePlayer_Corpse = 2604534927
    }
}