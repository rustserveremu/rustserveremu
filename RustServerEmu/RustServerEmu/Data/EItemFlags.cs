namespace RustServerEmu.Data
{
    public enum EItemFlags
    {
        None = 0,
        Placeholder = 1,
        IsOn = 2,
        OnFire = 4,
        IsLocked = 8,
        Cooking = 16
    }
}