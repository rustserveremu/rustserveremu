namespace RustServerEmu.Data
{
    public enum ELogType
    {
        Generic,
        Error,
        Warning,
        Chat
    }
}