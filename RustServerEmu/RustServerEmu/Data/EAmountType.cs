namespace RustServerEmu.Data
{
    public enum EAmountType
    {
        Count,
        Millilitre,
        Feet,
        Genetics,
        OxygenSeconds,
        Frequency
    }
}