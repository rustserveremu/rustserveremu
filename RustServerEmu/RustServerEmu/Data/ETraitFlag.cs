using System;

namespace RustServerEmu.Data
{
    [Flags]
    public enum ETraitFlag
    {
        None = 0,
        Alive = 1,
        Animal = 2,
        Human = 4,
        Interesting = 8,
        Food = 16,
        Meat = 32,
        Water = 32
    }
}