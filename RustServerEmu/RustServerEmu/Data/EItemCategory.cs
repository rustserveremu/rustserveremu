namespace RustServerEmu.Data
{
    public enum EItemCategory
    {
        Weapon,
        Construction,
        Items,
        Resources,
        Attire,
        Tool,
        Medical,
        Food,
        Ammunition,
        Traps,
        Misc,
        All,
        Common,
        Component,
        Search
    }
}