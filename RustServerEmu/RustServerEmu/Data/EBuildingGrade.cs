namespace RustServerEmu.Data
{
    public enum EBuildingGrade
    {
        None = -1,
        Twigs,
        Wood,
        Stone,
        Metal,
        TopTier,
        Count
    }
}