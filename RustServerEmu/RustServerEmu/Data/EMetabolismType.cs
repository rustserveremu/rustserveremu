namespace RustServerEmu.Data
{
    public enum EMetabolismType
    {
        Calories,
        Hydration,
        Heartrate,
        Poison,
        Radiation,
        Bleeding,
        Health,
        HealthOverTime
    }
}