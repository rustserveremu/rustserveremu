using System;

namespace RustServerEmu.Data
{
    [Flags]
    public enum EPlayerModalState
    {
        Ducked = 1,
        Jumped,
        OnGround = 4,
        Sleeping = 8,
        Sprinting = 16,
        OnLadder = 32,
        Flying = 64,
        Aiming = 128,
        Prone = 256,
        Mounted = 512,
        Relaxed = 1024
    }
}