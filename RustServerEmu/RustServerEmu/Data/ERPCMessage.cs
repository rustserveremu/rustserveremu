using System;

namespace RustServerEmu.Data
{
    public enum ERPCMessage : UInt32
    {
        BasePlayer_StartLoading = 2808526587,
        BasePlayer_FinishLoading = 2811987493,
        BasePlayer_OnPlayerLanded = 1998170713,
        BasePlayer_UpdateSteamInventory = 643458331,
        BasePlayer_ClientLoadingComplete = 3782818894,
        BasePlayer_DirectionalDamage = 397787795,
        BasePlayer_MoveItem = 3041092525,
        BasePlayer_ItemCMD = 3482449460,
        BaseEntity_BroadcastSignalFromClient = 1552640099,
        BaseEntity_SignalFromServerEx = 2406343387,
        HeldEntity_PlayerAttack = 4088326849, // End Melle Attack
        HeldEntity_CLProject = 3168282921, // Start Range Attack
        BasePlayer_OnProjectileAttack = 363681694, // End Range Attack
        BasePlayer_OnModelState = 1779218792,
        BaseCombatEntity_HitNotify = 3063539021,
        Planner_DoPlace = 1872774636,
        BasePlayer_UpdatedItemContainer = 1571447769,
        BaseProjectile_StartReload = 555589155,
        BaseProjectile_Reload = 1720368164,
        BasePlayer_OnProjectileRicochet = 1500391289,
        BasePlayer_OnProjectileUpdate = 2324190493,
        BaseProjectile_SwitchAmmoTo = 1918419884,
        BasePlayer_KeepAlive = 3263238541,
        BasePlayer_Assist = 970468557,
        CorpseLootable_LootCorpse = 2278459738,
        BasePlayer_OnDied = 1710591064,
        BowWeapon_BowReload = 4228048190,
        CompoundBowWeapon_StringHoldStatus = 618693016,
        MedicalTool_UseSelf = 2918424470,
        MedicalTool_UseOther = 789049461,
        MedicalTool_Reset = 3916310150,
        BasePlayer_ForcePositionTo = 3278437942,
        BaseProjectile_ServerFractionalReloadInsert = 240404208
    }
}
