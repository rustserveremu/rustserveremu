namespace RustServerEmu.Data
{
    public enum EBaseCombatLifeState
    {
        Alive,
        Dead
    }
}