namespace RustServerEmu.Data
{
    public enum EItemSlot
    {
        None = 1,
        Barrel = 2,
        Silencer = 4,
        Scope = 8,
        UnderBarrel = 16
    }
}