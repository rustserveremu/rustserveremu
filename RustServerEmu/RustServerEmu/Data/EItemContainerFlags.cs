namespace RustServerEmu.Data
{
    public enum EItemContainerFlags
    {
        IsPlayer = 1,
        Clothing = 2,
        Belt = 4,
        SingleType = 8,
        IsLocked = 16,
        ShowSlotsOnIcon = 32,
        NoBrokenItems = 64,
        NoItemInput = 128
    }
}