using Newtonsoft.Json;

namespace RustServerEmu
{
    public class ServerSettings
    {
        [JsonProperty("server.name")] public string Server_Name { get; set; } = "[RustServerEmu] No Name Host";
        [JsonProperty("server.maxplayers")] public uint Server_MaxPlayers { get; set; } = 10;
        [JsonProperty("server.protocol")] public uint Server_Protocol { get; set; } = 2164;
        [JsonProperty("server.port")] public uint Server_Port { get; set; } = 28015;
        [JsonProperty("server.ip")] public string Server_IP { get; set; } = "0.0.0.0";
        [JsonProperty("server.encryption")] public uint Server_Encryption { get; set; } = 1;
        
        [JsonProperty("rcon.enable")] public bool Rcon_Enable { get; set; } = true;
        [JsonProperty("rcon.password")] public string Rcon_Password { get; set; } = "12345678";
        
        [JsonProperty("map.name")] public string Map_Name { get; set; } = "HapisIsland";
        [JsonProperty("map.size")] public uint Map_Size { get; set; } = 4000;
        [JsonProperty("map.seed")] public uint Map_Seed { get; set; } = 0;

        [JsonProperty("server.description")] public string Server_Description { get; set; } = "";
        [JsonProperty("server.imageurl")] public string Server_ImageURL { get; set; } = "";
        [JsonProperty("server.url")] public string Server_URL { get; set; } = "";

        [JsonProperty("path.logsdir")] public string Path_LogDir { get; set; } = "/Logs/";
        [JsonProperty("path.databasedir")] public string Path_DatabaseDir { get; set; } = "/Data/Database/";
        
        [JsonProperty("gamewer.enable")] public bool GameWer_Enable { get; set; } = false;
        [JsonProperty("gamewer.licenskey")] public string GameWer_LicensKey { get; set; } = "licensKeyToEmu";
        [JsonProperty("gamewer.anymod")] public bool GameWer_AnyMod { get; set; } = false;
        [JsonProperty("gamewer.downloadmessage")] public string GameWer_DownloadMessage { get; set; } = "You not runned anti-cheat GameWer. Download: https://vk.com/allowerd";
    }
}