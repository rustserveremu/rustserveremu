using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Threading;
using Facepunch;
using Newtonsoft.Json;
using RustServerEmu.GObject.GameObject;
using RustServerEmu.Handlers;
using Tefor.Engine;

namespace RustServerEmu.AppEnvironment
{
    public class AppManager : BaseType
    {
        public static AppManager Instance { get; private set; }
        static void Init() => Framework.Init<AppManager>();

        public ServerSettings Settings { get; set; }
        public int CurrentFPS { get; private set; } = 100;

        private List<float> ListFps { get; } = new List<float>();
        private float TimePerSecond { get; set; } = 0f;

        void OnAwake()
        {
            Instance = this;
            this.InitializationSettings();
            this.InitializationLogs();
            this.InitializationSystems();
        }

        void InitializationLogs()
        {
            try
            {
                if (Directory.Exists(AppDomain.CurrentDomain.BaseDirectory + this.Settings.Path_LogDir) == false)
                    Directory.CreateDirectory(AppDomain.CurrentDomain.BaseDirectory + this.Settings.Path_LogDir);
                Debug.OutputPath = Path.GetFullPath(AppDomain.CurrentDomain.BaseDirectory + this.Settings.Path_LogDir + $"/output_{DateTime.Now:dd_mm_yyyy__HH_mm}.log");
            }
            catch (Exception ex)
            {
                Debug.LogError("[AppManager::InitializationLogs] Exception:");
                Debug.LogException(ex);
            }
        }

        void InitializationSystems()
        {
            ChatCommandHandler.Init();
            ConsoleCommandHandler.Init();
            this.AddType<DatabaseManager>();
            this.AddType<SteamworksManager>();
            this.AddType<NetworkManager>();
            this.AddType<RconManager>();
            this.AddType<GameWerManager>();
        }

        void DownloadSettings()
        {
            try
            {
                string settingsPath = AppDomain.CurrentDomain.BaseDirectory + "/Settings.json";
                if (CommandLine.HasSwitch("settings.url") && Uri.TryCreate(CommandLine.GetSwitch("settings.url", ""), UriKind.RelativeOrAbsolute, out Uri uri))
                {
                    Debug.LogSuccessful("[AppManager::DownloadSettings] Detected [settings.url], downloading settings...");
                    using (WebClient wc = new WebClient())
                    {
                        wc.Encoding = Encoding.UTF8;
                        string content = wc.DownloadString(uri);
                        File.WriteAllText(settingsPath, content);
                    }
                }
            }
            catch (Exception ex)
            {
                Debug.LogError("[AppManager::DownloadSettings] Exception:");
                Debug.LogException(ex);
            }
        }
        
        void InitializationSettings()
        {
            this.DownloadSettings();
            string settingsPath = AppDomain.CurrentDomain.BaseDirectory + "/Settings.json";
            try
            {
                if (File.Exists(settingsPath))
                {
                    this.Settings = JsonConvert.DeserializeObject<ServerSettings>(File.ReadAllText(settingsPath));
                }
                else
                {
                    this.Settings = new ServerSettings();
                    File.WriteAllText(settingsPath, JsonConvert.SerializeObject(this.Settings, Formatting.Indented));
                    
                }
            }
            catch (Exception ex)
            {
                Debug.LogError("[AppManager::InitializationSettings] Exception:");
                Debug.LogException(ex);
            }

            if (this.Settings == null)
            {
                Debug.LogError("[AppManager::InitializationSettings] Settings not initialized, start has failed...");
                Thread.Sleep(3000);
                Framework.Shutdown();
                Environment.Exit(0);
            }

             PropertyInfo[] props = this.Settings.GetType().GetProperties();
             for (var i = 0; i < props.Length; i++)
             {
                 object[] attrs = props[i].GetCustomAttributes(typeof(JsonPropertyAttribute), true);
                 if (attrs.Length >= 1)
                 {
                     JsonPropertyAttribute propAttr = (JsonPropertyAttribute) attrs[0];
                     if (CommandLine.HasSwitch("-" + propAttr.PropertyName))
                     {
                         switch (props[i].PropertyType.Name)
                         {
                             case "Int32":
                                 props[i].SetValue(this.Settings, (int)CommandLine.GetSwitchInt("-" + propAttr.PropertyName, 0));
                                 break;
                             case "UInt32":
                                 props[i].SetValue(this.Settings, (uint)CommandLine.GetSwitchInt("-" + propAttr.PropertyName, 0));
                                 break;
                             case "String":
                                 props[i].SetValue(this.Settings, CommandLine.GetSwitch("-" + propAttr.PropertyName, ""));
                                 break;
                             case "Boolean":
                                 props[i].SetValue(this.Settings, CommandLine.GetSwitchInt("-" + propAttr.PropertyName, 0) == 1);
                                 break;
                         }
                     }
                 }
             }
        }

        void OnUpdate()
        {
            this.ListFps.Add((1f / FrameworkManager.DeltaTime));
        }

        void OnFixedUpdate(float fixedDeltaTime)
        {
            this.TimePerSecond += fixedDeltaTime;
            if (this.TimePerSecond >= 1f)
            {
                this.TimePerSecond = 0f;
                this.CurrentFPS = (int) this.ListFps.Average();
                this.ListFps.Clear();
                
                Console.Title = $"[{BaseEnvironment.InstanceEnvironment.CurrentTime:HH:mm}] " +
                                $"FPS: {this.CurrentFPS}; " +
                                $"Entities: {NetworkManager.Instance.ListSpawnedNetworkables.Count}; " +
                                $"Online: {BasePlayer.ListActivePlayers.Count} / {this.Settings.Server_MaxPlayers}; [{this.Settings.Server_Name}]";
            }
        }
    }
}