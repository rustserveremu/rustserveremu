using System;
using System.Collections.Generic;
using System.IO;
using Network;
using ProtoBuf;
using RustServerEmu.BasicEnvironment;
using RustServerEmu.Extension;
using RustServerEmu.GObject.GameObject;
using RustServerEmu.Handlers;
using Tefor.Engine;
using BaseNetworkable = RustServerEmu.GObject.GameObject.BaseNetworkable;
using BasePlayer = RustServerEmu.GObject.GameObject.BasePlayer;
using Server = Facepunch.Network.Raknet.Server;

namespace RustServerEmu.AppEnvironment
{
    public class NetworkManager : BaseType
    {
        public static NetworkManager Instance { get; private set; }
        public Server BaseServer { get; private set; }

        public List<Connection> ListInConnecting { get; } = new List<Connection>();
        public List<Connection> ListConnections { get; } = new List<Connection>();
        public Dictionary<ulong, Connection> ListConnectedPlayers { get; } = new Dictionary<ulong, Connection>();
        public Dictionary<uint, BaseNetworkable> ListSpawnedNetworkables { get; } = new Dictionary<uint, BaseNetworkable>();
        private HashSet<ulong> ListOnlineSteamID { get; } = new HashSet<ulong>();

        void OnAwake()
        {
            Instance = this;
            this.StartServer();
            this.Create<BaseEnvironment>().Spawn();
        }

        void StartServer()
        {
            try
            {
                this.BaseServer = new Server();
                this.BaseServer.ip = AppManager.Instance.Settings.Server_IP;
                this.BaseServer.port = (int) AppManager.Instance.Settings.Server_Port;
                this.BaseServer.cryptography = new CraptographyHandler();
                this.BaseServer.callbackHandler = new NetworkServerHandler();
                this.BaseServer.Start();
                Debug.LogSuccessful("[NetworkManager]: Server has been started!");
            }
            catch (Exception ex)
            {
                Debug.LogError("[NetworkManager]: Exception in StartServer: ");
                Debug.LogException(ex);
            }
        }

        public void OnPlayerConnected(Connection connection)
        {
            if (this.ListOnlineSteamID.Contains(connection.userid) == false)
            {
                this.ListInConnecting.Add(connection);
                this.ListOnlineSteamID.Add(connection.userid);
                if (this.ListConnections.Contains(connection) == false)
                {
                    connection.state = Connection.State.Welcoming;

                    Debug.Log($"[NetworkManaged]: Player [{connection.userid} / {connection.username}] has been connected");

                    connection.SendMessage_Approval(AppManager.Instance.Settings.Server_Encryption);
                    connection.encryptionLevel = AppManager.Instance.Settings.Server_Encryption;
                    connection.encryptOutgoing = true;
                }
            }
            else
                this.BaseServer.Kick(connection, "This connection is already!");
        }

        public void OnPlayerReady(Message message)
        {
            if (this.ListInConnecting.Contains(message.connection))
                this.ListInConnecting.Remove(message.connection);
            
            message.connection.decryptIncoming = true;
            message.connection.connected = true;
            message.connection.state = Connection.State.Connected;
            
            using (ClientReady clientReady = ProtoBuf.ClientReady.Deserialize(message.read))
            {
                foreach (ClientReady.ClientInfo clientInfo in clientReady.clientInfo)
                    message.connection.info.Set(clientInfo.name, clientInfo.value);
            }
            
            this.ListConnections.Add(message.connection);
            this.ListConnectedPlayers[message.connection.userid] = message.connection;

            BasePlayer player = null;
            if (BasePlayer.ListAnyBasePlayers.TryGetValue(message.connection.userid, out player) == false)
            {
                player = this.Create<BasePlayer>();
                BasePlayer.ListAnyBasePlayers[message.connection.userid] = player;
                player.Spawn();
            }

            player.SetConnectionOwner(message.connection);
            GameWerManager.Instance.AuthPlayer(message.connection);
        }

        public void OnPlayerDisconnected(Connection connection, string reason)
        {
            GameWerManager.Instance.LogoutPlayer(connection);
            connection.connected = false;
            connection.state = Connection.State.Disconnected;
            
            if (this.ListInConnecting.Contains(connection))
                this.ListInConnecting.Remove(connection);
            
            if (this.ListOnlineSteamID.Contains(connection.userid))
                this.ListOnlineSteamID.Remove(connection.userid);
            
            if (this.ListConnections.Contains(connection))
            {
                BasePlayer player = connection.userid.ToPlayer();
                player?.SetConnectionOwner(null);
                
                this.ListConnections.Remove(connection);
                this.ListConnectedPlayers.Remove(connection.userid);            
            }
            Debug.Log($"[NetworkManaged]: Player [{connection.userid} / {connection.username}] has been disconnected: " + reason);
        }

        public T Create<T>() where T : BaseNetworkable
        {
            T gObject = this.AddType<T>();
            return gObject;
        }

        public void Despawn(BaseNetworkable networkable)
        {
            if (this.ListSpawnedNetworkables.ContainsKey(networkable.Net.UID))
            {
                Destroy(networkable);
                this.ListSpawnedNetworkables.Remove(networkable.Net.UID);
            }
        }

        void OnUpdate()
        {
            this.BaseServer?.Cycle();
        }
    }
}