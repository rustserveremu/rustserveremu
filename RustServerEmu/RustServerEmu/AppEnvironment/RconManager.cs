using System;
using System.Collections.Generic;
using System.Net;
using Facepunch;
using Facepunch.Math;
using Facepunch.Rcon;
using Network;
using Newtonsoft.Json;
using RustServerEmu.Data;
using RustServerEmu.Handlers;
using RustServerEmu.Struct;
using RustServerEmu.Struct.Output;
using Tefor.Engine;

namespace RustServerEmu.AppEnvironment
{
    public class RconManager : BaseType
    {
        public static RconManager Instance { get; private set; }
        public Listener BaseListener { get; private set; } = null;

        private Queue<RconCommand> CommandsQueue { get; set; } = new Queue<RconCommand>();
        private int CurrentTargetIdentifier { get; set; } = -1;
        private string CurrentTargetConnection { get; set; } = "";

        void OnAwake()
        {
            Instance = this;
            if (AppManager.Instance.Settings.Rcon_Enable == true)
            {
                Debug.OnOutputLog += this.OnOutputHandle;

                this.BaseListener = new Listener();
                if (!string.IsNullOrEmpty(AppManager.Instance.Settings.Server_IP))
                    this.BaseListener.Address = AppManager.Instance.Settings.Server_IP;

                this.BaseListener.Password = AppManager.Instance.Settings.Rcon_Password;
                this.BaseListener.Port = (int) AppManager.Instance.Settings.Server_Port;
                this.BaseListener.SslCertificate = CommandLine.GetSwitch("-rcon.ssl", null);
                this.BaseListener.SslCertificatePassword = CommandLine.GetSwitch("-rcon.sslpwd", null);
                this.BaseListener.OnMessage = delegate(IPEndPoint ip, string id, string msg)
                {
                    lock (this.CommandsQueue)
                    {
                        RconCommand item = JsonConvert.DeserializeObject<RconCommand>(msg);
                        item.Ip = ip;
                        item.ConnectionId = id;
                        this.CommandsQueue.Enqueue(item);
                    }
                };
                this.BaseListener.Start();
                Debug.LogSuccessful("WebSocket RCon Started on " + AppManager.Instance.Settings.Server_Port);
            }
        }

        void OnUpdate()
        {
            if (this.BaseListener != null)
            {
                lock (this.CommandsQueue)
                {
                    while (this.CommandsQueue.Count > 0)
                    {
                        RconCommand command = this.CommandsQueue.Dequeue();
                        try
                        {
                            this.CurrentTargetConnection = command.ConnectionId;
                            this.CurrentTargetIdentifier = command.Identifier;

                            ConsoleCommandHandler.RunConsoleCommand(command.Message, ConsoleCommandHandler.EConsoleCommandOwner.Server, null);
                        }
                        finally
                        {
                            this.CurrentTargetConnection = string.Empty;
                            this.CurrentTargetIdentifier = -1;
                        }
                    }
                }
            }
        }
        
        public void Broadcast(ELogType type, object obj, bool classicMessage = false)
        {
            if (this.BaseListener != null)
            {
                RconResponse response = default(RconResponse);
                if (obj is RconResponse == false)
                {
                    response.Message = JsonConvert.SerializeObject(obj, Formatting.Indented);
                    response.Stacktrace = "";
                    response.Type = type;
                    if (this.CurrentTargetIdentifier == -1 || type == ELogType.Chat)
                    {
                        if (type == ELogType.Chat)
                            RconResponse.HistroyChat.Add(response);
                        else
                            RconResponse.Histroy.Add(response);
                    }
                }
                else
                    response = (RconResponse) obj;

                response.Identifier = (classicMessage ? this.CurrentTargetIdentifier : -1);

                if (string.IsNullOrEmpty(this.CurrentTargetConnection))
                {
                    this.BaseListener.BroadcastMessage(JsonConvert.SerializeObject(response, Formatting.Indented));
                    return;
                }

                this.BaseListener.SendMessage(this.CurrentTargetConnection, JsonConvert.SerializeObject(response, Formatting.Indented));
            }
        }


        void OnOutputHandle(DebugLine line)
        {
            if (this.BaseListener != null)
            {
                RconResponse response = default(RconResponse);
                response.Identifier = this.CurrentTargetIdentifier;
                response.Message = "[" + line.Date.ToString("HH:mm:ss.ffff") + "] [" + line.Prefix + "] " + line.Message;
                response.Stacktrace = "";
                response.Type = ELogType.Generic;

                if (line.Prefix == "ERROR" || line.Prefix == "EXCEPTION")
                    response.Type = ELogType.Error;
                else if (line.Prefix == "WARNING")
                    response.Type = ELogType.Warning;

                if (this.CurrentTargetIdentifier == -1)
                    RconResponse.Histroy.Add(response);

                if (string.IsNullOrEmpty(this.CurrentTargetConnection))
                {
                    this.BaseListener.BroadcastMessage(JsonConvert.SerializeObject(response, Formatting.Indented));
                    return;
                }

                this.BaseListener.SendMessage(this.CurrentTargetConnection, JsonConvert.SerializeObject(response, Formatting.Indented));
            }
        }

        void OnDestroy()
        {
            if (this.BaseListener != null)
                this.BaseListener.Shutdown();
        }
    }
}