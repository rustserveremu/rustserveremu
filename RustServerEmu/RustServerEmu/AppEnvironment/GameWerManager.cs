using System.Collections.Generic;
using Network;
using RustServerEmu.Extension;
using Tefor.Engine;

namespace RustServerEmu.AppEnvironment
{
    public class GameWerManager : BaseType
    {
        public static GameWerManager Instance { get; private set; }

        private HashSet<Connection> ListAuthSuccessConnections = new HashSet<Connection>();
        private bool CurrentNetworkStatus = false;
        private bool IsInitialized = false;
        
        void OnAwake()
        {
            Instance = this;
            if (AppManager.Instance.Settings.GameWer_Enable == true)
            {
                GameWer.SDK.Interface.NeedKickPlayer = OnGameWerNeedKickPlayer;
                GameWer.SDK.Interface.NetworkStatusChange = NetworkStatusChange;
                GameWer.SDK.Interface.Initialization(AppManager.Instance.Settings.GameWer_LicensKey, AppManager.Instance.Settings.Server_Name);
                this.IsInitialized = true;
                Debug.LogSuccessful("[GameWerManager] Has been initialized!");
            }
        }

        public void AuthPlayer(Connection connection)
        {
            if (CurrentNetworkStatus == true)
            {
                int appid = GameWer.SDK.Interface.GetAppID(connection.token);
                if (appid == 480 || AppManager.Instance.Settings.GameWer_AnyMod == true)
                {
                    if (this.ListAuthSuccessConnections.Contains(connection) == false)
                    {
                        this.ListAuthSuccessConnections.Add(connection);
                        GameWer.SDK.Interface.AuthPlayer(connection.userid, connection.ipaddress.Split(':')[0]);
                        Timer.Once(30f, () =>
                        {
                            if (CurrentNetworkStatus == true && connection.connected == true)
                            {
                                GameWer.SDK.Interface.GetScreenshot(connection.userid);
                                Timer.Once(60f, () =>
                                {
                                    if (CurrentNetworkStatus == true && connection.connected == true)
                                    {
                                        GameWer.SDK.Interface.GetScreenshot(connection.userid);
                                        Timer.Once(120f, () => { this.GetScreenshotRepeet(connection); }, Framework.MainFramework);
                                    }
                                }, Framework.MainFramework);
                            }
                        }, Framework.MainFramework);
                    }
                }
            }
        }

        private void GetScreenshotRepeet(Connection connection)
        {
            if (CurrentNetworkStatus == true && connection.connected == true)
            {
                if (this.ListAuthSuccessConnections.Contains(connection))
                {
                    GameWer.SDK.Interface.GetScreenshot(connection.userid);
                    Timer.Once(1000f, () => { this.GetScreenshotRepeet(connection); }, Framework.MainFramework);
                }
            }
        }

        public void LogoutPlayer(Connection connection)
        {
            if (CurrentNetworkStatus == true && this.ListAuthSuccessConnections.Contains(connection))
            {
                this.ListAuthSuccessConnections.Remove(connection);
                GameWer.SDK.Interface.Logout(connection.userid);
            }
        }

        private void NetworkStatusChange(bool status)
        {
            Debug.LogSuccessful("[GameWerManager::NetworkStatusChange] GameWer Network Status: " + status);
            this.CurrentNetworkStatus = status;
            if (status != false)
            {
                for (var i = 0; i < NetworkManager.Instance.ListConnections.Count; i++)
                    this.AuthPlayer(NetworkManager.Instance.ListConnections[i]);
            } else
                this.ListAuthSuccessConnections.Clear();
        }

        private void OnGameWerNeedKickPlayer(ulong steamid, string reason)
        {
            if (NetworkManager.Instance.ListConnectedPlayers.TryGetValue(steamid, out Connection connection))
            {
                if (reason == "anticheat-offline")
                    reason = AppManager.Instance.Settings.GameWer_DownloadMessage;
                NetworkManager.Instance.BaseServer.Kick(connection, reason);
                if (reason.Contains("banned"))
                    NetworkManager.Instance.ListConnections.SendMessage_ChatMessage("Kick reason: " + reason, "<color=red>GameWer</color>");
            }
        }

        void OnFixedUpdate()
        {
            if (this.IsInitialized)
                GameWer.SDK.Interface.Cycle();
        }

        void OnDestroy()
        {
            if (this.IsInitialized)
                GameWer.SDK.Interface.Shutdown();
        }
    }
}