using System;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using Newtonsoft.Json;
using Tefor.Engine;

namespace RustServerEmu.AppEnvironment
{
    public class DatabaseManager : BaseType
    {
        void OnAwake()
        {
            this.InitializationDatabaseDir();
            
            Type[] types = typeof(Bootstrap).Assembly.GetTypes();
            types = types.Where(t => t.Namespace == typeof(Bootstrap).Namespace + ".Database").ToArray();
            for (var i = 0; i < types.Length; i++)
            {
                MethodInfo mi = types[i].GetMethod("Init", BindingFlags.Static | BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public);
                mi?.Invoke(null, new object[0]);
            }
        }

        void InitializationDatabaseDir()
        {
            try
            {
                if (Directory.Exists(AppDomain.CurrentDomain.BaseDirectory + AppManager.Instance.Settings.Path_DatabaseDir) == false)
                    Directory.CreateDirectory(AppDomain.CurrentDomain.BaseDirectory + AppManager.Instance.Settings.Path_DatabaseDir);

                DirectoryInfo dirDatabase = new DirectoryInfo(AppDomain.CurrentDomain.BaseDirectory + "/Data/Database/");
                FileInfo[] files = dirDatabase.GetFiles();
                for (var i = 0; i < files.Length; i++)
                {
                    string pathFile = Path.GetFullPath(AppDomain.CurrentDomain.BaseDirectory + AppManager.Instance.Settings.Path_DatabaseDir + "/" + files[i].Name);
                    if (File.Exists(pathFile) == false)
                        files[i].CopyTo(pathFile);
                }
            }
            catch (Exception ex)
            {
                Debug.LogError("[DatabaseManager::InitializationDatabaseDir] Exception: ");
                Debug.LogException(ex);
            }
        }
    }
}