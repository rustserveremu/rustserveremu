using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using Facepunch.Math;
using Facepunch.Steamworks;
using Network;
using ProtoBuf;
using RustServerEmu.BasicEnvironment;
using RustServerEmu.Database;
using RustServerEmu.Extension.DefaultCode;
using Tefor.Engine;
using Server = Facepunch.Steamworks.Server;

namespace RustServerEmu.AppEnvironment
{
    public class SteamworksManager : BaseType
    {
        public static SteamworksManager Instance { get; private set; }
        public HashSet<ulong> ListLicenceSteamIDs { get; } = new HashSet<ulong>();
        public Facepunch.Steamworks.Server BaseServer { get; set; }

        void OnAwake()
        {
            Instance = this;
            ServerInit serverInit = new ServerInit("rust", "Rust");
            serverInit.IpAddress = IPAddress.Parse(AppManager.Instance.Settings.Server_IP);
            serverInit.GamePort = (ushort)AppManager.Instance.Settings.Server_Port;
            serverInit.Secure = true;
            serverInit.VersionString = AppManager.Instance.Settings.Server_Protocol.ToString();
            serverInit.QueryShareGamePort();
            this.BaseServer = new Server(252490, serverInit);
            if (this.BaseServer.IsValid)
            {
                this.BaseServer.Auth.OnAuthChange = (steamid, ownerID, status) =>
                {
                    Connection connection = NetworkManager.Instance.ListConnections.FirstOrDefault(c => c.userid == steamid);
                    Timer.Once(1, () => { this.OnAuthChange(steamid, ownerID, status, connection); }, Framework.MainFramework);
                };
                this.BaseServer.LogOnAnonymous();
                Timer.Once(3, this.UpdateServerInformation, Framework.MainFramework);
                Debug.LogSuccessful("[SteamworksManager::OnAwake] Steamworks has been initialized!");
            }
            else
            {
                if (this.BaseServer != null)
                {
                    this.BaseServer.Dispose();
                    this.BaseServer = null;
                }
                Debug.LogError("[SteamworksManager::OnAwake] Steamworks is not initialized!");
            }
        }

        public void OnAuthChange(ulong steamID, ulong ownerID, ServerAuth.Status status, Connection connection)
        {
            if (connection != null)
            {
                if (status == ServerAuth.Status.OK)
                {
                    if (this.ListLicenceSteamIDs.Contains(steamID) == false)
                        this.ListLicenceSteamIDs.Add(steamID);
                    if (this.ListLicenceSteamIDs.Contains(ownerID) == false)
                        this.ListLicenceSteamIDs.Add(ownerID);
                    if (NetworkManager.Instance.ListInConnecting.Contains(connection) == false && NetworkManager.Instance.ListConnections.Contains(connection) == false)
                    {
                        Debug.Log($"[SteamworksManager::OnAuthChange] Player {steamID} use licence rust, good boy!");
                        NetworkManager.Instance.OnPlayerConnected(connection);
                    }
                }
                else
                {
                    if (this.ListLicenceSteamIDs.Contains(steamID))
                        this.ListLicenceSteamIDs.Remove(steamID);
                    if (this.ListLicenceSteamIDs.Contains(ownerID))
                        this.ListLicenceSteamIDs.Remove(ownerID);
                    if (GamePlayDefinition.Instance.Configuration.PlayerApprovalPirate)
                    {
                        if (NetworkManager.Instance.ListInConnecting.Contains(connection) == false && NetworkManager.Instance.ListConnections.Contains(connection) == false)
                        {
                            Debug.Log($"[SteamworksManager::OnAuthChange] Player {steamID} not auth in steam, result: " + status);
                            NetworkManager.Instance.OnPlayerConnected(connection);
                        }
                    }
                    else if (connection != null)
                        NetworkManager.Instance.BaseServer.Kick(connection, "Steam Auth Failed!");
                }
            }
        }

        void UpdateServerInformation()
        {
            if (this.BaseServer != null)
            {
                int fullOnline = NetworkManager.Instance.ListConnections.Count;
                int validOnline = NetworkManager.Instance.ListConnections.Count(con => this.ListLicenceSteamIDs.Contains(con.userid));
                int failedOnline = fullOnline - validOnline;
                
                this.BaseServer.ServerName = AppManager.Instance.Settings.Server_Name;
                this.BaseServer.MaxPlayers = (int)AppManager.Instance.Settings.Server_MaxPlayers;
                this.BaseServer.Passworded = false;
                this.BaseServer.MapName = AppManager.Instance.Settings.Map_Name;
                string gameTags = string.Format("mp{0},cp{1},qp{5},v{2}{3},h{4},{6},{7},ai{8},modded,serveremu", new object[]
                {
                    AppManager.Instance.Settings.Server_MaxPlayers,
                    validOnline,
                    AppManager.Instance.Settings.Server_Protocol,
                    string.Empty,
                    ApplicationVersion.Version,
                    0,
                    "stok",
                    "born10",
                    failedOnline
                });
                this.BaseServer.GameTags = gameTags;
                
                if (AppManager.Instance.Settings.Server_Description != null && AppManager.Instance.Settings.Server_Description.Length > 100)
                {
                    string[] array = AppManager.Instance.Settings.Server_Description.SplitToChunks(100).ToArray<string>();
                    for (int i = 0; i < 16; i++)
                        this.BaseServer.SetKey(string.Format("description_{0:00}", i), ((i < array.Length) ? array[i] : string.Empty));
                }
                else
                {
                    this.BaseServer.SetKey("description_0", AppManager.Instance.Settings.Server_Description);
                    for (int j = 1; j < 16; j++)
                        this.BaseServer.SetKey(string.Format("description_{0:00}", j), string.Empty);
                }
                this.BaseServer.SetKey("hash", ApplicationVersion.Version);
                this.BaseServer.SetKey("world.seed", AppManager.Instance.Settings.Map_Seed.ToString());
                this.BaseServer.SetKey("world.size", AppManager.Instance.Settings.Map_Size.ToString());
                this.BaseServer.SetKey("pve", "False");
                this.BaseServer.SetKey("headerimage", AppManager.Instance.Settings.Server_ImageURL);
                this.BaseServer.SetKey("url", AppManager.Instance.Settings.Server_URL);
                this.BaseServer.SetKey("uptime", ((int)DateTime.Now.Subtract(Framework.MainFramework.StartedTime).TotalSeconds).ToString());
                this.BaseServer.SetKey("gc_mb", "0");
                this.BaseServer.SetKey("gc_cl", "0");
                this.BaseServer.SetKey("fps", AppManager.Instance.CurrentFPS.ToString());
                this.BaseServer.SetKey("fps_avg", AppManager.Instance.CurrentFPS.ToString());
                this.BaseServer.SetKey("ent_cnt", NetworkManager.Instance.ListSpawnedNetworkables.Count.ToString());
                this.BaseServer.SetKey("build", ApplicationVersion.Branch + ":" + ApplicationVersion.Version);
            }   
            Timer.Once(10, this.UpdateServerInformation, Framework.MainFramework);
        }

        void OnUpdate()
        {
            if (this.BaseServer != null)
            {
                this.BaseServer.RunUpdateCallbacks();
                this.BaseServer.Update();
            }
        }

        void OnFixedUpdate()
        {
            ServerQuery.Packet packet;
            while (this.BaseServer.Query.GetOutgoingPacket(out packet))
                NetworkManager.Instance.BaseServer.SendUnconnected(packet.Address, packet.Port, packet.Data, packet.Size);
        }
    }
}