using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using CSharpPatcher.SDK;
using Newtonsoft.Json;
using UnityEngine;

namespace DumpDatabaseFromServer
{
    public class DumperType
    {
        [HookMethod("Assembly-CSharp.dll", "", "BaseEntity", "ServerInit", "", HookMethod.ETypeInject.RewriteMethod)]
        public static void ClearServerInit()
        {
            
        }

        public static uint GetPrefabID(GameObjectRef refRes)
        {
            if (refRes == null || refRes.isValid == false)
                return 0;
            try
            {
                return refRes.resourceID;
            }
            catch
            {
                try
                {
                    return refRes.Get().GetComponent<BaseNetworkable>()?.prefabID ?? 0;
                }
                catch
                {
                    
                }
            }
            return 0;
        }

        public static int GetItemID(global::ItemDefinition itemDefinition)
        {
            if (itemDefinition == null)
                return 0;
            return itemDefinition.itemid;
        }

        public static void GetComponents(ItemDefenition dumpingItem, global::ItemDefinition itemDefinition)
        {
            try
            {
                ItemModAlterCondition componentAlterCondition = itemDefinition.GetComponent<ItemModAlterCondition>();
                if (componentAlterCondition != null)
                {
                    dumpingItem.ItemComponentDefinitions.AlterCondition = new ItemDefenition.ItemComponentAlterCondition();
                    dumpingItem.ItemComponentDefinitions.AlterCondition.ConditionChange = componentAlterCondition.conditionChange;
                }
            }
            catch
            {
                Debug.LogError("[=> " + itemDefinition.itemid + "] Exception in GetComponents : ItemModAlterCondition");
            }

            try
            {
                ItemModBlueprintCraft componentBlueprintCraft = itemDefinition.GetComponent<ItemModBlueprintCraft>();
                if (componentBlueprintCraft != null)
                {
                    dumpingItem.ItemComponentDefinitions.BlueprintCraft = new ItemDefenition.ItemComponentBlueprintCraft();
                    dumpingItem.ItemComponentDefinitions.BlueprintCraft.SuccessEffectPrefabID = GetPrefabID(componentBlueprintCraft.successEffect);
                }
            }
            catch
            {
                Debug.LogError("[=> " + itemDefinition.itemid + "] Exception in GetComponents : ItemModBlueprintCraft");
            }

            try
            {
                ItemModBurnable componentBurnable = itemDefinition.GetComponent<ItemModBurnable>();
                if (componentBurnable != null)
                {
                    dumpingItem.ItemComponentDefinitions.Burnable = new ItemDefenition.ItemComponentBurnable();
                    dumpingItem.ItemComponentDefinitions.Burnable.FuelAmount = componentBurnable.fuelAmount;
                    dumpingItem.ItemComponentDefinitions.Burnable.ByproductItemID = GetItemID(componentBurnable.byproductItem);
                    dumpingItem.ItemComponentDefinitions.Burnable.ByproductAmount = componentBurnable.byproductAmount;
                    dumpingItem.ItemComponentDefinitions.Burnable.ByproductChance = componentBurnable.byproductChance;
                }
            }
            catch
            {
                Debug.LogError("[=> " + itemDefinition.itemid + "] Exception in GetComponents : ItemModBurnable");
            }

            try
            {
                ItemModConditionContainerFlag componentConditionContainerFlag = itemDefinition.GetComponent<ItemModConditionContainerFlag>();
                if (componentConditionContainerFlag != null)
                {
                    dumpingItem.ItemComponentDefinitions.ConditionContainerFlag = new ItemDefenition.ItemComponentConditionContainerFlag();
                    dumpingItem.ItemComponentDefinitions.ConditionContainerFlag.Flag = componentConditionContainerFlag.flag;
                    dumpingItem.ItemComponentDefinitions.ConditionContainerFlag.RequiredState = componentConditionContainerFlag.requiredState;
                }
            }
            catch
            {
                Debug.LogError("[=> " + itemDefinition.itemid + "] Exception in GetComponents : ItemModConditionContainerFlag");
            }

            try
            {
                ItemModConditionHasContents componentConditionHasContents = itemDefinition.GetComponent<ItemModConditionHasContents>();
                if (componentConditionHasContents != null)
                {
                    dumpingItem.ItemComponentDefinitions.ConditionHasContents = new ItemDefenition.ItemComponentConditionHasContents();
                    dumpingItem.ItemComponentDefinitions.ConditionHasContents.ItemID = GetItemID(componentConditionHasContents.itemDef);
                    dumpingItem.ItemComponentDefinitions.ConditionHasContents.RequiredState = componentConditionHasContents.requiredState;
                }
            }
            catch
            {
                Debug.LogError("[=> " + itemDefinition.itemid + "] Exception in GetComponents : ItemModConditionHasContents");
            }

            try
            {
                ItemModConditionHasFlag componentConditionHasFlag = itemDefinition.GetComponent<ItemModConditionHasFlag>();
                if (componentConditionHasFlag != null)
                {
                    dumpingItem.ItemComponentDefinitions.ConditionHasFlag = new ItemDefenition.ItemComponentConditionHasFlag();
                    dumpingItem.ItemComponentDefinitions.ConditionHasFlag.Flag = componentConditionHasFlag.flag;
                    dumpingItem.ItemComponentDefinitions.ConditionHasFlag.RequiredState = componentConditionHasFlag.requiredState;
                }
            }
            catch
            {
                Debug.LogError("[=> " + itemDefinition.itemid + "] Exception in GetComponents : ItemModConditionHasFlag");
            }

            try
            {
                ItemModConditionInWater componentConditionInWater = itemDefinition.GetComponent<ItemModConditionInWater>();
                if (componentConditionInWater != null)
                {
                    dumpingItem.ItemComponentDefinitions.ConditionInWater = new ItemDefenition.ItemComponentConditionInWater();
                    dumpingItem.ItemComponentDefinitions.ConditionInWater.RequiredState = componentConditionInWater.requiredState;
                }
            }
            catch
            {
                Debug.LogError("[=> " + itemDefinition.itemid + "] Exception in GetComponents : ItemModConditionInWater");
            }

            try
            {
                ItemModConsume componentConsume = itemDefinition.GetComponent<ItemModConsume>();
                if (componentConsume != null)
                {
                    dumpingItem.ItemComponentDefinitions.Consume = new ItemDefenition.ItemComponentConsume();
                    dumpingItem.ItemComponentDefinitions.Consume.ConsumeEffectPrefabID = GetPrefabID(componentConsume.consumeEffect);
                    dumpingItem.ItemComponentDefinitions.Consume.EatGesture = componentConsume.eatGesture;
                }
            }
            catch
            {
                Debug.LogError("[=> " + itemDefinition.itemid + "] Exception in GetComponents : ItemModConsume");
            }

            try
            {
                ItemModConsumable componentConsumable = itemDefinition.GetComponent<ItemModConsumable>();
                if (componentConsumable != null)
                {
                    dumpingItem.ItemComponentDefinitions.Consumable = new ItemDefenition.ItemComponentConsumable();
                    dumpingItem.ItemComponentDefinitions.Consumable.AmountToConsume = componentConsumable.amountToConsume;
                    dumpingItem.ItemComponentDefinitions.Consumable.ConditionFractionToLose = componentConsumable.conditionFractionToLose;
                    if (componentConsumable.effects != null)
                    {
                        dumpingItem.ItemComponentDefinitions.Consumable.Effects = new List<ItemDefenition.ItemComponentConsumable.ConsumableEffect>();
                        for (var i = 0; i < componentConsumable.effects.Count; i++)
                        {
                            dumpingItem.ItemComponentDefinitions.Consumable.Effects.Add(new ItemDefenition.ItemComponentConsumable.ConsumableEffect
                            {
                                Amount = componentConsumable.effects[i].amount,
                                Time = componentConsumable.effects[i].time,
                                OnlyIfHealthLessThan = componentConsumable.effects[i].onlyIfHealthLessThan,
                                Type = componentConsumable.effects[i].type,
                            });
                        }
                    }
                }
            }
            catch
            {
                Debug.LogError("[=> " + itemDefinition.itemid + "] Exception in GetComponents : ItemModConsumable");
            }

            try
            {
                ItemModConsumeContents componentConsumeContents = itemDefinition.GetComponent<ItemModConsumeContents>();
                if (componentConsumeContents != null)
                {
                    dumpingItem.ItemComponentDefinitions.ConsumeContents = new ItemDefenition.ItemComponentConsumeContents();
                    dumpingItem.ItemComponentDefinitions.ConsumeContents.ConsumeEffectPrefabID = GetPrefabID(componentConsumeContents.consumeEffect);
                }
            }
            catch
            {
                Debug.LogError("[=> " + itemDefinition.itemid + "] Exception in GetComponents : ItemModConsumeContents");
            }

            try
            {
                ItemModContainer componentContainer = itemDefinition.GetComponent<ItemModContainer>();
                if (componentContainer != null)
                {
                    dumpingItem.ItemComponentDefinitions.Container = new ItemDefenition.ItemComponentContainer();
                    dumpingItem.ItemComponentDefinitions.Container.Capacity = componentContainer.capacity;
                    dumpingItem.ItemComponentDefinitions.Container.MaxStackSize = componentContainer.maxStackSize;
                    dumpingItem.ItemComponentDefinitions.Container.ContainerFlags = componentContainer.containerFlags;
                    dumpingItem.ItemComponentDefinitions.Container.OnlyAllowedContents = componentContainer.onlyAllowedContents;
                    dumpingItem.ItemComponentDefinitions.Container.OnlyAllowedItemID = GetItemID(componentContainer.onlyAllowedItemType);
                    dumpingItem.ItemComponentDefinitions.Container.AvailableSlots = componentContainer.availableSlots;
                    dumpingItem.ItemComponentDefinitions.Container.OpenInDeployed = componentContainer.openInDeployed;
                    dumpingItem.ItemComponentDefinitions.Container.OpenInInventory = componentContainer.openInInventory;
                }
            }
            catch
            {
                Debug.LogError("[=> " + itemDefinition.itemid + "] Exception in GetComponents : ItemModContainer");
            }

            try
            {
                ItemModCookable componentCookable = itemDefinition.GetComponent<ItemModCookable>();
                if (componentCookable != null)
                {
                    dumpingItem.ItemComponentDefinitions.Cookable = new ItemDefenition.ItemComponentCookable();
                    dumpingItem.ItemComponentDefinitions.Cookable.BecomeOnCookedItemID = GetItemID(componentCookable.becomeOnCooked);
                    dumpingItem.ItemComponentDefinitions.Cookable.CookTime = componentCookable.cookTime;
                    dumpingItem.ItemComponentDefinitions.Cookable.AmountOfBecome = componentCookable.amountOfBecome;
                    dumpingItem.ItemComponentDefinitions.Cookable.LowTemp = componentCookable.lowTemp;
                    dumpingItem.ItemComponentDefinitions.Cookable.HighTemp = componentCookable.highTemp;
                    dumpingItem.ItemComponentDefinitions.Cookable.SetCookingFlag = componentCookable.setCookingFlag;
                }
            }
            catch
            {
                Debug.LogError("[=> " + itemDefinition.itemid + "] Exception in GetComponents : ItemModCookable");
            }

            try
            {
                ItemModDeployable componentDeployable = itemDefinition.GetComponent<ItemModDeployable>();
                if (componentDeployable != null)
                {
                    dumpingItem.ItemComponentDefinitions.Deployable = new ItemDefenition.ItemComponentDeployable();
                    dumpingItem.ItemComponentDefinitions.Deployable.EntityPrefabID = GetPrefabID(componentDeployable?.entityPrefab);
                    dumpingItem.ItemComponentDefinitions.Deployable.ShowCrosshair = componentDeployable.showCrosshair;
                    dumpingItem.ItemComponentDefinitions.Deployable.UnlockAchievement = componentDeployable.UnlockAchievement;
                }
            }
            catch
            {
                Debug.LogError("[=> " + itemDefinition.itemid + "] Exception in GetComponents : ItemModDeployable");
            }

            try
            {
                ItemModEntity componentEntity = itemDefinition.GetComponent<ItemModEntity>();
                if (componentEntity != null)
                {
                    dumpingItem.ItemComponentDefinitions.Entity = new ItemDefenition.ItemComponentEntity();
                    dumpingItem.ItemComponentDefinitions.Entity.EntityPrefabID = GetPrefabID(componentEntity.entityPrefab);
                    dumpingItem.ItemComponentDefinitions.Entity.DefaultBone = StringPool.Get(componentEntity?.defaultBone ?? "");
                }
            }
            catch
            {
                Debug.LogError("[=> " + itemDefinition.itemid + "] Exception in GetComponents : ItemModEntity");
            }

            try
            {
                ItemModGiveOxygen componentGiveOxygen = itemDefinition.GetComponent<ItemModGiveOxygen>();
                if (componentGiveOxygen != null)
                {
                    dumpingItem.ItemComponentDefinitions.GiveOxygen = new ItemDefenition.ItemComponentGiveOxygen();
                    dumpingItem.ItemComponentDefinitions.GiveOxygen.AmountToConsume = componentGiveOxygen.amountToConsume;
                    dumpingItem.ItemComponentDefinitions.GiveOxygen.InhaleEffectPrefabID = GetPrefabID(componentGiveOxygen.inhaleEffect);
                    dumpingItem.ItemComponentDefinitions.GiveOxygen.ExhaleEffectPrefabID = GetPrefabID(componentGiveOxygen.exhaleEffect);
                    dumpingItem.ItemComponentDefinitions.GiveOxygen.BubblesEffectPrefabID = GetPrefabID(componentGiveOxygen.bubblesEffect);
                }
            }
            catch
            {
                Debug.LogError("[=> " + itemDefinition.itemid + "] Exception in GetComponents : ItemModGiveOxygen");
            }

            try
            {
                ItemModKeycard componentKeycard = itemDefinition.GetComponent<ItemModKeycard>();
                if (componentKeycard != null)
                {
                    dumpingItem.ItemComponentDefinitions.Keycard = new ItemDefenition.ItemComponentKeycard();
                    dumpingItem.ItemComponentDefinitions.Keycard.AccessLevel = componentKeycard.accessLevel;
                }
            }
            catch
            {
                Debug.LogError("[=> " + itemDefinition.itemid + "] Exception in GetComponents : ItemModKeycard");
            }

            try
            {
                ItemModProjectile componentProjectile = itemDefinition.GetComponent<ItemModProjectile>();
                if (componentProjectile != null)
                {
                    dumpingItem.ItemComponentDefinitions.Projectile = new ItemDefenition.ItemComponentProjectile();
                    dumpingItem.ItemComponentDefinitions.Projectile.ProjectileObjectPrefabID = GetPrefabID(componentProjectile.projectileObject);
                    dumpingItem.ItemComponentDefinitions.Projectile.AmmoType = componentProjectile.ammoType;
                    dumpingItem.ItemComponentDefinitions.Projectile.NumProjectiles = componentProjectile.numProjectiles;
                    dumpingItem.ItemComponentDefinitions.Projectile.ProjectileSpread = componentProjectile.projectileSpread;
                    dumpingItem.ItemComponentDefinitions.Projectile.ProjectileVelocity = componentProjectile.projectileVelocity;
                    dumpingItem.ItemComponentDefinitions.Projectile.ProjectileVelocitySpread = componentProjectile.projectileVelocitySpread;
                    dumpingItem.ItemComponentDefinitions.Projectile.UseCurve = componentProjectile.useCurve;
                    dumpingItem.ItemComponentDefinitions.Projectile.AttackEffectOverridePrefabID = GetPrefabID(componentProjectile.attackEffectOverride);
                    dumpingItem.ItemComponentDefinitions.Projectile.BarrelConditionLoss = componentProjectile.barrelConditionLoss;
                    dumpingItem.ItemComponentDefinitions.Projectile.Category = componentProjectile.category;

                    Projectile projectileObject = componentProjectile.projectileObject.Get()?.GetComponent<Projectile>();
                    if (projectileObject != null)
                    {
                        for (var i = 0; projectileObject.damageTypes != null && i < projectileObject.damageTypes.Count; i++)
                        {
                            dumpingItem.ItemComponentDefinitions.Projectile.DamageTypes.Add(new ItemDefenition.DamageTypeInfo
                            {
                                Damage = projectileObject.damageTypes[i].amount,
                                Type = projectileObject.damageTypes[i].type
                            });
                        }
                    }
                }
            }
            catch
            {
                Debug.LogError("[=> " + itemDefinition.itemid + "] Exception in GetComponents : ItemModProjectile");
            }

            try
            {
                ItemModRecycleInto componentRecycleInto = itemDefinition.GetComponent<ItemModRecycleInto>();
                if (componentRecycleInto != null)
                {
                    dumpingItem.ItemComponentDefinitions.RecycleInto = new ItemDefenition.ItemComponentRecycleInto();
                    dumpingItem.ItemComponentDefinitions.RecycleInto.RecycleIntoItemID = GetItemID(componentRecycleInto.recycleIntoItem);
                    dumpingItem.ItemComponentDefinitions.RecycleInto.NumRecycledItemMin = componentRecycleInto.numRecycledItemMin;
                    dumpingItem.ItemComponentDefinitions.RecycleInto.NumRecycledItemMax = componentRecycleInto.numRecycledItemMax;
                    dumpingItem.ItemComponentDefinitions.RecycleInto.SuccessEffectPrefabID = GetPrefabID(componentRecycleInto.successEffect);
                }
            }
            catch
            {
                Debug.LogError("[=> " + itemDefinition.itemid + "] Exception in GetComponents : ItemModProjectile");
            }

            try
            {
                ItemModRepair componentRepair = itemDefinition.GetComponent<ItemModRepair>();
                if (componentRepair != null)
                {
                    dumpingItem.ItemComponentDefinitions.Repair = new ItemDefenition.ItemComponentRepair();
                    dumpingItem.ItemComponentDefinitions.Repair.ConditionLost = componentRepair.conditionLost;
                    dumpingItem.ItemComponentDefinitions.Repair.SuccessEffectPrefabID = GetPrefabID(componentRepair.successEffect);
                    dumpingItem.ItemComponentDefinitions.Repair.WorkbenchLvlRequired = componentRepair.workbenchLvlRequired;
                }
            }
            catch
            {
                Debug.LogError("[=> " + itemDefinition.itemid + "] Exception in GetComponents : ItemModRepair");
            }

            try
            {
                ItemModReveal componentReveal = itemDefinition.GetComponent<ItemModReveal>();
                if (componentReveal != null)
                {
                    dumpingItem.ItemComponentDefinitions.Reveal = new ItemDefenition.ItemComponentReveal();
                    dumpingItem.ItemComponentDefinitions.Reveal.NumForReveal = componentReveal.numForReveal;
                    dumpingItem.ItemComponentDefinitions.Reveal.RevealedItemOverrideItemID = GetItemID(componentReveal.revealedItemOverride);
                    dumpingItem.ItemComponentDefinitions.Reveal.RevealedItemAmount = componentReveal.revealedItemAmount;
                    dumpingItem.ItemComponentDefinitions.Reveal.SuccessEffectPrefabID = GetPrefabID(componentReveal.successEffect);
                }
            }
            catch
            {
                Debug.LogError("[=> " + itemDefinition.itemid + "] Exception in GetComponents : ItemModReveal");
            }

            try
            {
                ItemModRFListener componentRFListener = itemDefinition.GetComponent<ItemModRFListener>();
                if (componentRFListener != null)
                {
                    dumpingItem.ItemComponentDefinitions.RFListener = new ItemDefenition.ItemComponentRFListener();
                    dumpingItem.ItemComponentDefinitions.RFListener.FrequencyPanelPrefabID = GetPrefabID(componentRFListener.frequencyPanelPrefab);
                    dumpingItem.ItemComponentDefinitions.RFListener.EntityPrefabID = GetPrefabID(componentRFListener.entityPrefab);
                }
            }
            catch
            {
                Debug.LogError("[=> " + itemDefinition.itemid + "] Exception in GetComponents : ItemModRFListener");
            }

            try
            {
                ItemModSound componentSound = itemDefinition.GetComponent<ItemModSound>();
                if (componentSound != null)
                {
                    dumpingItem.ItemComponentDefinitions.Sound = new ItemDefenition.ItemComponentSound();
                    dumpingItem.ItemComponentDefinitions.Sound.EffectPrefabID = GetPrefabID(componentSound.effect);
                }
            }
            catch
            {
                Debug.LogError("[=> " + itemDefinition.itemid + "] Exception in GetComponents : ItemModSound");
            }

            try
            {
                ItemModStudyBlueprint componentStudyBlueprint = itemDefinition.GetComponent<ItemModStudyBlueprint>();
                if (componentStudyBlueprint != null)
                {
                    dumpingItem.ItemComponentDefinitions.StudyBlueprint = new ItemDefenition.ItemComponentStudyBlueprint();
                    dumpingItem.ItemComponentDefinitions.StudyBlueprint.StudyEffectPrefabID = GetPrefabID(componentStudyBlueprint.studyEffect);
                }
            }
            catch
            {
                Debug.LogError("[=> " + itemDefinition.itemid + "] Exception in GetComponents : ItemModStudyBlueprint");
            }

            try
            {
                ItemModSwap componentSwap = itemDefinition.GetComponent<ItemModSwap>();
                if (componentSwap != null)
                {
                    dumpingItem.ItemComponentDefinitions.Swap = new ItemDefenition.ItemComponentSwap();
                    dumpingItem.ItemComponentDefinitions.Swap.ActionEffectPrefabID = GetPrefabID(componentSwap.actionEffect);
                    dumpingItem.ItemComponentDefinitions.Swap.SendPlayerPickupNotification = componentSwap.sendPlayerPickupNotification;
                    dumpingItem.ItemComponentDefinitions.Swap.SendPlayerDropNotification = componentSwap.sendPlayerDropNotification;
                    dumpingItem.ItemComponentDefinitions.Swap.XpScale = componentSwap.xpScale;
                }
            }
            catch
            {
                Debug.LogError("[=> " + itemDefinition.itemid + "] Exception in GetComponents : ItemModSwap");
            }

            try
            {
                ItemModUnwrap componentUnwrap = itemDefinition.GetComponent<ItemModUnwrap>();
                if (componentUnwrap != null)
                {
                    dumpingItem.ItemComponentDefinitions.Unwrap = new ItemDefenition.ItemComponentUnwrap();
                    dumpingItem.ItemComponentDefinitions.Unwrap.SuccessEffectPrefabID = GetPrefabID(componentUnwrap.successEffect);
                    dumpingItem.ItemComponentDefinitions.Unwrap.MinTries = componentUnwrap.minTries;
                    dumpingItem.ItemComponentDefinitions.Unwrap.MaxTries = componentUnwrap.maxTries;
                }
            }
            catch
            {
                Debug.LogError("[=> " + itemDefinition.itemid + "] Exception in GetComponents : ItemModUnwrap");
            }

            try
            {
                ItemModUpgrade componentUpgrade = itemDefinition.GetComponent<ItemModUpgrade>();
                if (componentUpgrade != null)
                {
                    dumpingItem.ItemComponentDefinitions.Upgrade = new ItemDefenition.ItemComponentUpgrade();
                    dumpingItem.ItemComponentDefinitions.Upgrade.NumForUpgrade = componentUpgrade.numForUpgrade;
                    dumpingItem.ItemComponentDefinitions.Upgrade.UpgradeSuccessChance = componentUpgrade.upgradeSuccessChance;
                    dumpingItem.ItemComponentDefinitions.Upgrade.NumToLoseOnFail = componentUpgrade.numToLoseOnFail;
                    dumpingItem.ItemComponentDefinitions.Upgrade.UpgradedItemID = GetItemID(componentUpgrade.upgradedItem);
                    dumpingItem.ItemComponentDefinitions.Upgrade.NumUpgradedItem = componentUpgrade.numUpgradedItem;
                    dumpingItem.ItemComponentDefinitions.Upgrade.SuccessEffectPrefabID = GetPrefabID(componentUpgrade.successEffect);
                    dumpingItem.ItemComponentDefinitions.Upgrade.FailEffectPrefabID = GetPrefabID(componentUpgrade.failEffect);
                }
            }
            catch
            {
                Debug.LogError("[=> " + itemDefinition.itemid + "] Exception in GetComponents : ItemModUpgrade");
            }

            try
            {
                ItemModUseContent componentUseContent = itemDefinition.GetComponent<ItemModUseContent>();
                if (componentUseContent != null)
                {
                    dumpingItem.ItemComponentDefinitions.UseContent = new ItemDefenition.ItemComponentUseContent();
                    dumpingItem.ItemComponentDefinitions.UseContent.AmountToConsume = componentUseContent.amountToConsume;
                }
            }
            catch
            {
                Debug.LogError("[=> " + itemDefinition.itemid + "] Exception in GetComponents : ItemModUseContent");
            }

            try
            {
                ItemModWearable componentWearable = itemDefinition.GetComponent<ItemModWearable>();
                if (componentWearable != null)
                {
                    dumpingItem.ItemComponentDefinitions.Wearable = new ItemDefenition.ItemComponentWearable();
                    dumpingItem.ItemComponentDefinitions.Wearable.EntityPrefabID = GetPrefabID(componentWearable.entityPrefab);
                    dumpingItem.ItemComponentDefinitions.Wearable.Protections = componentWearable?.protectionProperties?.amounts ?? new float[0];
                    dumpingItem.ItemComponentDefinitions.Wearable.ProtectionDensity = componentWearable?.protectionProperties?.density ?? 0;
                    dumpingItem.ItemComponentDefinitions.Wearable.ArmorArea = componentWearable?.armorProperties?.area ?? HitArea.Arm;
                    dumpingItem.ItemComponentDefinitions.Wearable.BlocksAiming = componentWearable.blocksAiming;
                    dumpingItem.ItemComponentDefinitions.Wearable.Emissive = componentWearable.emissive;
                    dumpingItem.ItemComponentDefinitions.Wearable.AccuracyBonus = componentWearable.accuracyBonus;
                    dumpingItem.ItemComponentDefinitions.Wearable.BlocksEquipping = componentWearable.blocksEquipping;
                    dumpingItem.ItemComponentDefinitions.Wearable.EggVision = componentWearable.eggVision;
                    dumpingItem.ItemComponentDefinitions.Wearable.ViewmodelAdditionPrefabID = GetPrefabID(componentWearable.viewmodelAddition);
                }
            }
            catch
            {
                Debug.LogError("[=> " + itemDefinition.itemid + "] Exception in GetComponents : ItemModWearable");
            }
        }

        [HookMethod("Assembly-CSharp.dll", "", "ItemManager", "Initialize", "", HookMethod.ETypeInject.InjectMethod, 232)]
        public static void DumpItems()
        {
            Dictionary<uint, string> vals = (Dictionary<uint, string>)typeof(StringPool).GetField("toString", BindingFlags.Static | BindingFlags.NonPublic | BindingFlags.Instance).GetValue(null);
            File.WriteAllText("./StringPool.txt", "");
            foreach (var pair in vals)
                File.AppendAllText("./StringPool.txt", "\n" + pair.Key + "=" + pair.Value);
            
            Dictionary<int, ItemDefenition> listDumpingItems = new Dictionary<int, ItemDefenition>();
            foreach (var def in ItemManager.itemDictionary)
            {
                ItemDefenition dumpingItem = new ItemDefenition();
                dumpingItem.ItemID = def.Value.itemid;
                dumpingItem.Condition = (int) def.Value.condition.max;
                dumpingItem.Hidden = def.Value.hidden;
                dumpingItem.AmountType = def.Value.amountType;
                dumpingItem.Category = def.Value.category;
                dumpingItem.DisplayName = def.Value.displayName?.translated ?? "";
                dumpingItem.ContentsType = def.Value.itemType;
                dumpingItem.ParentItemID = def.Value.Parent?.itemid ?? 0;
                dumpingItem.ShortName = def.Value.shortname;
                dumpingItem.Stackable = def.Value.stackable;
                dumpingItem.TraitsFlag = def.Value.Traits;

                try
                {
                    Item item = ItemManager.Create(def.Value, 1, 0UL);
                    if (item != null)
                    {
                        dumpingItem.TypeItem = item.GetType().Name;
                        BaseEntity entity = item.GetHeldEntity();
                        if (entity != null && entity is HeldEntity held)
                        {
                            dumpingItem.HeldDefinition = new ItemDefenition.HeldEntityInfo();
                            dumpingItem.HeldDefinition.TypeHeldEntity = held.GetType().Name;
                            dumpingItem.HeldDefinition.ParentBone = StringPool.Get(held.handBone);
                            dumpingItem.HeldDefinition.PrefabID = held.prefabID;
                            if (held is BaseMelee melee)
                            {
                                dumpingItem.HeldDefinition.DeployDelay = melee.deployDelay;
                                dumpingItem.HeldDefinition.MeleeDefinition = new ItemDefenition.BaseMeleeInfo();
                                dumpingItem.HeldDefinition.MeleeDefinition.DamageTypes = new List<ItemDefenition.DamageTypeInfo>();
                                for (var i = 0; melee.damageTypes != null && i < melee.damageTypes.Count; i++)
                                {
                                    dumpingItem.HeldDefinition.MeleeDefinition.DamageTypes.Add(new ItemDefenition.DamageTypeInfo()
                                    {
                                        Damage = melee.damageTypes[i].amount,
                                        Type = melee.damageTypes[i].type
                                    });
                                }

                                dumpingItem.HeldDefinition.MeleeDefinition.Distance = melee.maxDistance;
                                dumpingItem.HeldDefinition.MeleeDefinition.CanProjectile = melee.canThrowAsProjectile;
                                dumpingItem.HeldDefinition.MeleeDefinition.OnlyProjectile = melee.onlyThrowAsProjectile;
                            }

                            if (held is BaseProjectile projectile)
                            {
                                dumpingItem.HeldDefinition.DeployDelay = projectile.deployDelay;
                                dumpingItem.HeldDefinition.RangeDefinition = new ItemDefenition.BaseRangeInfo();
                                dumpingItem.HeldDefinition.RangeDefinition.CanAutomatic = projectile.automatic;
                                dumpingItem.HeldDefinition.RangeDefinition.DamageScale = projectile.damageScale;
                                dumpingItem.HeldDefinition.RangeDefinition.DistanceScale = projectile.distanceScale;
                                dumpingItem.HeldDefinition.RangeDefinition.ReloadTime = projectile.reloadTime;
                                if (projectile.primaryMagazine != null)
                                {
                                    dumpingItem.HeldDefinition.RangeDefinition.MagazineDefinition = new ItemDefenition.MagazineInfo();
                                    dumpingItem.HeldDefinition.RangeDefinition.MagazineDefinition.Capacity = projectile.primaryMagazine.capacity;
                                    dumpingItem.HeldDefinition.RangeDefinition.MagazineDefinition.Contents = projectile.primaryMagazine.contents;
                                    dumpingItem.HeldDefinition.RangeDefinition.MagazineDefinition.ItemID = GetItemID(projectile.primaryMagazine.ammoType);
                                    dumpingItem.HeldDefinition.RangeDefinition.MagazineDefinition.AmmoType = projectile.primaryMagazine.definition.ammoTypes;
                                    dumpingItem.HeldDefinition.RangeDefinition.MagazineDefinition.BuiltInSize = projectile.primaryMagazine.definition.builtInSize;
                                }
                            }

                            if (held is ThrownWeapon thrownWeapon)
                            {
                                dumpingItem.HeldDefinition.ThrownDefinition = new ItemDefenition.ThrownWeaponInfo();
                                dumpingItem.HeldDefinition.ThrownDefinition.PrefabID = GetPrefabID(thrownWeapon.prefabToThrow);
                                if (thrownWeapon.overrideAngle != null)
                                {
                                    dumpingItem.HeldDefinition.ThrownDefinition.AngleX = thrownWeapon.overrideAngle.x;
                                    dumpingItem.HeldDefinition.ThrownDefinition.AngleY = thrownWeapon.overrideAngle.y;
                                    dumpingItem.HeldDefinition.ThrownDefinition.AngleZ = thrownWeapon.overrideAngle.z;
                                }
                            }

                            if (held is MedicalTool medicalTool)
                            {
                                dumpingItem.HeldDefinition.DeployDelay = medicalTool.deployDelay;
                                dumpingItem.HeldDefinition.MedicalDefinition = new ItemDefenition.MedicalToolInfo();
                                dumpingItem.HeldDefinition.MedicalDefinition.CanRevive = medicalTool.canRevive;
                            }

                        }
                    }
                }
                catch
                {
                    
                }
                dumpingItem.ItemComponentDefinitions = new ItemDefenition.ItemComponents();
                DumperType.GetComponents(dumpingItem, def.Value);
                listDumpingItems.Add(def.Value.itemid, dumpingItem);
            }
            string json = JsonConvert.SerializeObject(listDumpingItems, Formatting.Indented);
            File.WriteAllText("./Items.json", json);
        }
        
    }
}