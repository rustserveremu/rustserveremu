AmmoTypes:
	- PISTOL_9MM = 1
	- RIFLE_556MM = 2
	- SHOTGUN_12GUAGE = 4
	- BOW_ARROW = 8
	- HANDMADE_SHELL = 16
	- ROCKET = 32
	- NAILS = 64
	- AMMO_40MM = 128
________________________________
HitArea:
	- Head = 1
        - Chest = 2
        - Stomach = 4
        - Arm = 8
        - Hand = 16
        - Leg = 32
        - Foot = 64

________________________________
ItemContainerFlags:
	- IsPlayer = 1
        - Clothing = 2
        - Belt = 4
        - SingleType = 8
        - IsLocked = 16
        - ShowSlotsOnIcon = 32
        - NoBrokenItems = 64
        - NoItemInput = 128

ItemFlags:
	- None = 0,
        - Placeholder = 1
        - IsOn = 2
        - OnFire = 4
        - IsLocked = 8
        - Cooking = 16

MetabolismTypes:
	- Calories = 0
        - Hydration = 1
        - Heartrate = 2
        - Poison = 3
        - Radiation = 4
        - Bleeding = 5
        - Health = 6
        - HealthOverTime = 7

ContentsType:
	- Generic = 1
        - Liquid = 2

ItemSlot:
	- None = 1
        - Barrel = 2
        - Silencer = 4
        - Scope = 8
        - UnderBarrel = 16

DamageTypes:
	- Generic = 0
        - Hunger = 1
        - Thirst = 2
        - Cold = 3
        - Drowned = 4
        - Heat = 5
        - Bleeding = 6
        - Poison = 7
        - Suicide = 8
        - Bullet = 9
        - Slash = 10
        - Blunt = 11
        - Fall = 12
        - Radiation = 13
        - Bite = 14
        - Stab = 15
        - Explosion = 16
        - RadiationExposure = 17
        - ColdExposure = 18
        - Decay = 19
        - ElectricShock = 20
        - Arrow = 21
        - LAST = 22