using System;
using System.Net;
using System.Text;
using System.Threading;

namespace Tefor.Engine
{
    public class Web
    {
        public static void GetBytes(string url, Action<byte[]> callback, Framework frameworkManager, Action<Exception> exception_callback = null)
        {
            ThreadPool.QueueUserWorkItem(_ =>
            {
                byte[] finish = new byte[0];
                try
                {
                    using (WebClient wc = new WebClient())
                    {
                        wc.Encoding = Encoding.UTF8;
                        finish = wc.DownloadData(url);
                    }
                }
                catch (Exception ex)
                {
                    if (exception_callback != null)
                        frameworkManager.CallInThread(() => exception_callback(ex));
                }
                frameworkManager.CallInThread(() => callback(finish));
            });
        }
        
        public static void GetString(string url, Action<string> callback, Framework frameworkManager, Action<Exception> exception_callback = null, Encoding encoding = null)
        {
            ThreadPool.QueueUserWorkItem(_ =>
            {
                string finish = string.Empty;
                try
                {
                    using (WebClient wc = new WebClient())
                    {
                        wc.Encoding = ((encoding == null) ? Encoding.UTF8 : encoding);
                        finish = wc.DownloadString(url);
                    }
                }
                catch (Exception ex)
                {
                    if (exception_callback != null)
                        frameworkManager.CallInThread(() => exception_callback(ex));
                }
                frameworkManager.CallInThread(() => callback(finish));
            });
        }
    }
}