using System;

namespace Tefor.Engine
{
    public class DebugLine
    {
        public String Message { get; set; }
        public String Prefix { get; set; }
        public DateTime Date { get; set; }
        public ConsoleColor Color { get; set; }
    }
}