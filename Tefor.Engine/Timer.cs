using System;
using System.Threading;

namespace Tefor.Engine
{
    public class Timer
    {
        public static Timer Once(float interval, Action callback, Framework frameworkManager)
        {
            Timer timer = Pool.Get<Timer>();
            timer.Interval = TimeSpan.FromSeconds(interval);
            timer.IsLoop = false;
            timer.IsAlive = true;
            timer.Callbacka = callback;
            timer.FrameworkManager = frameworkManager;
            ThreadPool.QueueUserWorkItem(timer.StartWorker);
            return timer;
        }

        public static Timer Loop(float interval, int countLoop, Action callback, Framework frameworkManager)
        {
            Timer timer = Pool.Get<Timer>();
            timer.Interval = TimeSpan.FromSeconds(interval);
            timer.IsLoop = true;
            timer.CoutnLoop = countLoop;
            timer.IsAlive = true;
            timer.Callbacka = callback;
            timer.FrameworkManager = frameworkManager;
            ThreadPool.QueueUserWorkItem(timer.StartWorker);
            return timer;
        }

        public static void Close(Timer timer)
        {
            if (timer.m_IsAlive)
            {
                timer.m_IsAlive = false;
                lock (timer.FrameworkManager)
                    timer.FrameworkManager = null;
                timer.TimerThrad.Abort();
                Pool.Free(ref timer);
            }
        }
        
        public Action Callbacka { get; private set; }
        public TimeSpan Interval { get; private set; }

        public bool IsAlive
        {
            get => this.m_IsAlive;
            private set => this.m_IsAlive = value;
        }
        public bool IsLoop { get; private set; }
        public int CoutnLoop { get; private set; }
        public Framework FrameworkManager { get; private set; }
        public Thread TimerThrad { get; private set; }

        private volatile bool m_IsAlive = false;
        
        private void StartWorker(object sender)
        {
            this.TimerThrad = Thread.CurrentThread;
            int countLooped = 0;
            while (true)
            {
                Thread.Sleep(this.Interval);
                countLooped++;
                if (this.m_IsAlive)
                {
                    lock (this.FrameworkManager)
                        this.FrameworkManager.CallInThread(this.Callbacka);
                }
                else
                    break;
                if (this.IsLoop == false || countLooped == this.CoutnLoop)
                    break;
                
            }
            if (this.m_IsAlive)
                Close(this);
        }
    }
}