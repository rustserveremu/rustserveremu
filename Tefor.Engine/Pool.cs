using System;
using System.Collections.Generic;
using System.IO;

namespace Tefor.Engine
{
    public static class Pool
	{
		public static void FreeList<T>(ref List<T> obj)
		{
			if (obj == null)
			{
				throw new ArgumentNullException();
			}
			obj.Clear();
			Pool.Free<List<T>>(ref obj);
			if (obj != null)
			{
				throw new SystemException("Pool.Free is not setting object to NULL");
			}
		}

		public static void FreeMemoryStream(ref MemoryStream obj)
		{
			if (obj == null)
			{
				throw new ArgumentNullException();
			}
			obj.Position = 0L;
			obj.SetLength(0L);
			Pool.Free<MemoryStream>(ref obj);
			if (obj != null)
			{
				throw new SystemException("Pool.Free is not setting object to NULL");
			}
		}

		public static void Free<T>(ref T obj) where T : class
		{
			if (obj == null)
			{
				throw new ArgumentNullException();
			}
			Pool.PoolCollection<T> poolCollection = Pool.FindCollection<T>();
			lock (poolCollection)
			{


				if (poolCollection.ItemsInStack >= (long) poolCollection.buffer.Length)
				{
					poolCollection.ItemsSpilled += 1L;
					poolCollection.ItemsInUse -= 1L;
					obj = (T) ((object) null);
					return;
				}

				poolCollection.buffer[(int) (checked((IntPtr) poolCollection.ItemsInStack))] = obj;
				poolCollection.ItemsInStack += 1L;
				poolCollection.ItemsInUse -= 1L;
			}

			Pool.IPooled pooled = obj as Pool.IPooled;
			if (pooled != null)
			{
				pooled.EnterPool();
			}
			obj = (T)((object)null);
		}

		public static T Get<T>() where T : class, new()
		{
			Pool.PoolCollection<T> poolCollection = Pool.FindCollection<T>();
			lock (poolCollection)
			{
				if (poolCollection.ItemsInStack > 0L)
				{
					poolCollection.ItemsInStack -= 1L;
					poolCollection.ItemsInUse += 1L;
					T t;
					checked
					{
						t = poolCollection.buffer[(int) ((IntPtr) poolCollection.ItemsInStack)];
						poolCollection.buffer[(int) ((IntPtr) poolCollection.ItemsInStack)] = (T) ((object) null);
						Pool.IPooled pooled = t as Pool.IPooled;
						if (pooled != null)
						{
							pooled.LeavePool();
						}
					}

					poolCollection.ItemsTaken += 1L;
					return t;
				}

				poolCollection.ItemsCreated += 1L;
				poolCollection.ItemsInUse += 1L;
			}
			return Activator.CreateInstance<T>();
		}

		public static List<T> GetList<T>()
		{
			return Pool.Get<List<T>>();
		}

		public static void ResizeBuffer<T>(int size) where T : class
		{
			Pool.PoolCollection<T> poolCollection = Pool.FindCollection<T>();
			Array.Resize<T>(ref poolCollection.buffer, size);
		}

		public static void FillBuffer<T>(int count = 2147483647) where T : class, new()
		{
			Pool.PoolCollection<T> poolCollection = Pool.FindCollection<T>();
			for (int i = 0; i < count; i++)
			{
				if (poolCollection.ItemsInStack >= (long)poolCollection.buffer.Length)
				{
					break;
				}
				poolCollection.buffer[(int)(checked((IntPtr)poolCollection.ItemsInStack))] = Activator.CreateInstance<T>();
				poolCollection.ItemsInStack += 1L;
			}
		}

		public static Pool.PoolCollection<T> FindCollection<T>() where T : class
		{
			
			Pool.PoolCollection<T> poolCollection = Pool<T>.Collection;
			if (poolCollection == null)
			{
				lock (Pool.directory)
				{
					if (Pool<T>.Collection == null)
					{
						poolCollection = new Pool.PoolCollection<T>();
						Pool<T>.Collection = poolCollection;
						Pool.directory.Add(typeof(T), Pool<T>.Collection);
					}
					else
						poolCollection = Pool<T>.Collection;
				}
			}
			return poolCollection;
		}

		public static void Clear()
		{
			foreach (KeyValuePair<Type, Pool.ICollection> keyValuePair in Pool.directory)
			{
				keyValuePair.Value.Reset();
			}
		}

		public static Dictionary<Type, Pool.ICollection> directory = new Dictionary<Type, Pool.ICollection>();

		public interface IPooled
		{
			void EnterPool();

			void LeavePool();
		}

		public interface ICollection
		{
			long ItemsInStack { get; }

			long ItemsInUse { get; }

			long ItemsCreated { get; }

			long ItemsTaken { get; }

			long ItemsSpilled { get; }

			void Reset();
		}

		public class PoolCollection<T> : Pool.ICollection
		{
			public PoolCollection()
			{
				this.Reset();
			}

			public long ItemsInStack { get; set; }

			public long ItemsInUse { get; set; }

			public long ItemsCreated { get; set; }

			public long ItemsTaken { get; set; }

			public long ItemsSpilled { get; set; }

			public void Reset()
			{
				this.buffer = new T[512];
				this.ItemsInStack = 0L;
				this.ItemsInUse = 0L;
				this.ItemsCreated = 0L;
				this.ItemsTaken = 0L;
				this.ItemsSpilled = 0L;
			}

			public T[] buffer;
		}
	}
	
	internal static class Pool<T> where T : class
	{
		public static Pool.PoolCollection<T> Collection;
	}
}