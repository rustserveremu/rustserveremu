using System;
using System.Collections.Generic;
using System.Security;
using System.Threading;

namespace Tefor.Engine
{
    internal class FrameworkWorker
    {
        public static Dictionary<int, FrameworkWorker> ListWorkerThreads { get; } = new Dictionary<int, FrameworkWorker>();

        public int ThreadID { get; private set; }
        public Framework FrameworkOwner { get; private set; }
        
        internal Queue<Action> ListCallInThread = new Queue<Action>();
        internal List<BaseType> ListTypeCreated = new List<BaseType>();
        internal List<BaseType> ListTypeNeedUpdate = new List<BaseType>();
        internal List<BaseType> ListTypeNeedFixedUpdate = new List<BaseType>();
        internal List<BaseType> ListTypeNeedDrawUpdate = new List<BaseType>();
        
        internal List<BaseType> ListTypeNeedStart = new List<BaseType>();
        internal List<BaseType> ListTypeNeedDestroy = new List<BaseType>();

        internal FrameworkWorker(Framework framework)
        {
            this.FrameworkOwner = framework;
            framework.WorkerManager = this;
            this.ThreadID = Thread.CurrentThread.ManagedThreadId;
            Thread.CurrentThread.Name = "FrameworkWorker ThradID " + this.ThreadID;
            lock (ListWorkerThreads)
                ListWorkerThreads.Add(this.ThreadID, this);
        }


        public void StartWork()
        {
            List<BaseType> workerList;
            DateTime lastTick = DateTime.Now;
            DateTime lastFixedTick = DateTime.Now;
            while (Framework.IsWork)
            {
                this.FrameworkOwner.DeltaTime = (Single)DateTime.Now.Subtract(lastTick).TotalSeconds;
                lastTick = DateTime.Now;

                try
                {
                    if (this.ListCallInThread.Count != 0)
                    {
                        while (this.ListCallInThread.Count != 0)
                        {
                            Action action = null;
                            lock (this.ListCallInThread)
                                action = this.ListCallInThread.Dequeue();
                            try
                            {
                                action();
                            }
                            catch (Exception ex)
                            {
                                Debug.LogError("Exception in CallInThread:");
                                Debug.LogException(ex);
                            }

                            if (this.ListCallInThread.Count == 0)
                                lock (this.ListCallInThread)
                                    this.ListCallInThread.Clear();
                        }
                    }
                }
                catch (Exception ex)
                {
                    Debug.LogError("Exception in while CallInThreads:");
                    Debug.LogException(ex);
                }

                if (ListTypeNeedStart.Count != 0)
                {
                    workerList = new List<BaseType>(ListTypeNeedStart);
                    this.ListTypeNeedStart.Clear();
                    for (var i = 0; i < workerList.Count; ++i)
                    {
                        if (workerList[i].Information.HasOnStartMethod)
                        {
                            try
                            {
                                workerList[i].Information.OnStart.Invoke(workerList[i]);
                            } catch (Exception ex)
                            {
                                Debug.LogError("Exception in OnStart:");
                                Debug.LogException(ex);
                            }
                        }

                        if (workerList[i].Information.HasOnUpdateMethod)
                            ListTypeNeedUpdate.Add(workerList[i]);
                        if (workerList[i].Information.HasOnFixedUpdateMethod)
                            ListTypeNeedFixedUpdate.Add(workerList[i]);
                        if (workerList[i].Information.HasOnDrawMethod)
                            ListTypeNeedDrawUpdate.Add(workerList[i]);
                        lock (ListTypeCreated)
                            ListTypeCreated.Add(workerList[i]);
                    }
                    workerList.Clear();
                }

                for (var i = 0; i < ListTypeNeedUpdate.Count; ++i)
                {
                    if (ListTypeNeedUpdate[i].m_destroyed == false)
                    {
                        ListTypeNeedUpdate[i].m_internalUpdate += this.FrameworkOwner.DeltaTime;
                        if (ListTypeNeedUpdate[i].m_internalUpdate >= this.FrameworkOwner.IntervalUpdate)
                        {
                            try
                            {
                                ListTypeNeedUpdate[i].Information.OnUpdate.Invoke(ListTypeNeedUpdate[i], ListTypeNeedUpdate[i].m_internalUpdate);
                            }
                            catch(Exception ex)
                            {
                                Debug.LogError("Exception in OnUpdate:");
                                Debug.LogException(ex);
                            }
                            ListTypeNeedUpdate[i].m_internalUpdate = 0;
                        }
                    }
                }

                for (var i = 0; i < ListTypeNeedFixedUpdate.Count; ++i)
                {
                    if (ListTypeNeedFixedUpdate[i].m_destroyed == false)
                    {
                        ListTypeNeedFixedUpdate[i].m_internalFixedUpdate += this.FrameworkOwner.DeltaTime;
                        if (ListTypeNeedFixedUpdate[i].m_internalFixedUpdate >= this.FrameworkOwner.IntervalFixedUpdate)
                        {
                            try
                            {
                                ListTypeNeedFixedUpdate[i].Information.OnFixedUpdate.Invoke(ListTypeNeedFixedUpdate[i], ListTypeNeedFixedUpdate[i].m_internalFixedUpdate);
                            }catch(Exception ex)
                            {
                                Debug.LogError("Exception in OnFixedUpdate:");
                                Debug.LogException(ex);
                            }

                            ListTypeNeedFixedUpdate[i].m_internalFixedUpdate = 0;
                        }
                    }
                }

                for (var i = 0; i < ListTypeNeedDrawUpdate.Count; ++i)
                {
                    if (ListTypeNeedDrawUpdate[i].m_destroyed == false)
                    {
                        ListTypeNeedDrawUpdate[i].m_internalDraw += this.FrameworkOwner.DeltaTime;
                        if (ListTypeNeedDrawUpdate[i].m_internalDraw >= this.FrameworkOwner.IntervalDraw)
                        {
                            try
                            {
                                ListTypeNeedDrawUpdate[i].Information.OnDraw.Invoke(ListTypeNeedDrawUpdate[i], ListTypeNeedDrawUpdate[i].m_internalDraw);
                            }catch(Exception ex)
                            {
                                Debug.LogError("Exception in OnDraw:");
                                Debug.LogException(ex);
                            }

                            ListTypeNeedDrawUpdate[i].m_internalDraw = 0;
                        }
                    }
                }
                
                if (ListTypeNeedDestroy.Count != 0)
                {
                    workerList = new List<BaseType>(ListTypeNeedDestroy);
                    this.ListTypeNeedDestroy.Clear();
                    for (var i = 0; i < workerList.Count; ++i)
                    {
                        if (workerList[i].Information.HasOnUpdateMethod)
                            ListTypeNeedDrawUpdate.Remove(workerList[i]);
                        if (workerList[i].Information.HasOnFixedUpdateMethod)
                            ListTypeNeedFixedUpdate.Remove(workerList[i]);
                        if (workerList[i].Information.HasOnDrawMethod)
                            ListTypeNeedDrawUpdate.Remove(workerList[i]);
                        lock(ListTypeCreated)
                            ListTypeCreated.Remove(workerList[i]);
                        workerList[i].Dispose();
                    }
                    workerList.Clear();
                }

                if (this.FrameworkOwner.IntervalTotal)
                {
                    float delay = (Single)DateTime.Now.Subtract(lastTick).TotalSeconds;
                    Thread.Sleep(TimeSpan.FromSeconds(this.FrameworkOwner.IntervalUpdate - delay));
                } else
                    Thread.Sleep(TimeSpan.FromSeconds(this.FrameworkOwner.IntervalUpdate));
            }
        }
    }
}