using System;
using System.Security;
using System.Threading;

namespace Tefor.Engine
{
    public class Framework
    {
        public static bool IsWork { get; private set; } = true;
        public static Framework MainFramework { get; private set; } = null;

        public static T Init<T>(bool _newThread = false) where T : BaseType
        {
            if (IsWork)
            {
                Framework framework = null;
                T result = null;
                if (_newThread == false)
                {
                    if (MainFramework != null)
                        throw new Exception("Main Framework is created!");
                    framework = new Framework();
                    MainFramework = framework;
                    FrameworkWorker worker = new FrameworkWorker(framework);
                    result = BaseType.Instantiate<T>(framework, null);
                    worker.StartWork();
                }
                else
                {
                    ThreadPool.QueueUserWorkItem(_ =>
                    {
                        framework = new Framework();
                        FrameworkWorker worker = new FrameworkWorker(framework);
                        result = BaseType.Instantiate<T>(framework, null);
                        worker.StartWork();
                    });
                    while (result == null)
                        Thread.Sleep(1);
                }
                return result;
            }
            return null;
        }

        public static void Shutdown(bool _fast = false)
        {
            if (IsWork)
            {
                IsWork = false;
                if (_fast == true)
                    System.Environment.FailFast("FastShutdown");
                lock (FrameworkWorker.ListWorkerThreads)
                {
                    foreach (var pair in FrameworkWorker.ListWorkerThreads)
                    {
                        lock (pair.Value.ListTypeCreated)
                        {
                            for (var i = 0; i < pair.Value.ListTypeCreated.Count; i++)
                            {
                                BaseType.Destroy(pair.Value.ListTypeCreated[i]);
                            }
                        }
                    } 
                }
                System.Environment.Exit(0);
            }
        }

        internal FrameworkWorker WorkerManager { get; set; }
        public float IntervalUpdate { get; set; } = 0.010f;
        public float IntervalFixedUpdate { get; set; } = 0.033f;
        public float IntervalDraw { get; set; } = 0.033f;
        public bool IntervalTotal { get; set; } = false;

        public float DeltaTime { get; internal set; } = 0.01f;
        public DateTime StartedTime { get; } = DateTime.Now;

        public void CallInThread(Action callback)
        {
            lock (this.WorkerManager.ListCallInThread)
                this.WorkerManager.ListCallInThread.Enqueue(callback);
        }

        private Framework()
        {
        }

    }
}