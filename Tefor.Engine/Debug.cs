using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;

namespace Tefor.Engine
{
    public class Debug 
    {
        public static Boolean HasWriteFile { get; set; } = true;
        public static String OutputPath { get; set; } = "./output.log";
        
        public static Boolean Enable_LOG { get; set; } = true;
        public static Boolean Enable_WARNING { get; set; } = true;
        public static Boolean Enable_ERROR { get; set; } = true;
        public static Boolean Enable_SUCCESS { get; set; } = true;
        public static Boolean Enable_DEBUG { get; set; } = true;

        public static Action<DebugLine> OnOutputLog { get; set; }

        private static readonly Queue<DebugLine> ListOutputLines = new Queue<DebugLine>();
        private static ConsoleColor LastConsoleColor = ConsoleColor.Gray;
        
        public static void Log(string message)
        {
            if (Enable_LOG)
            {
                DebugLine line = Pool.Get<DebugLine>();
                line.Color = ConsoleColor.Gray;
                line.Date = DateTime.Now;
                line.Prefix = "LOG";
                line.Message = message;
                OnOutputLog?.Invoke(line);
                
                if (Framework.MainFramework == null)
                    LogOutput(line);
                else
                {
                    lock (ListOutputLines)
                        ListOutputLines.Enqueue(line);
                }
            }
        }
        public static void LogWarning(string message)
        {
            if (Enable_WARNING)
            {
                DebugLine line = Pool.Get<DebugLine>();
                line.Color = ConsoleColor.Yellow;
                line.Date = DateTime.Now;
                line.Prefix = "WARNING";
                line.Message = message;
                OnOutputLog?.Invoke(line);
                
                if (Framework.MainFramework == null)
                    LogOutput(line);
                else
                {
                    lock (ListOutputLines)
                        ListOutputLines.Enqueue(line);
                }
            }
        }
        
        public static void LogError(string message)
        {
            if (Enable_ERROR)
            {
                DebugLine line = Pool.Get<DebugLine>();
                line.Color = ConsoleColor.Red;
                line.Date = DateTime.Now;
                line.Prefix = "ERROR";
                line.Message = message;
                OnOutputLog?.Invoke(line);
                
                if (Framework.MainFramework == null)
                    LogOutput(line);
                else
                {
                    lock (ListOutputLines)
                        ListOutputLines.Enqueue(line);
                }
            }
        }
        
        public static void LogException(Exception ex)
        {
            DebugLine line = Pool.Get<DebugLine>();
            line.Color = ConsoleColor.DarkRed;
            line.Date = DateTime.Now;
            line.Prefix = "EXCEPTION";
            line.Message = "Message: " + ex.Message + "\nStack Trace:\n" + ex.StackTrace;
            OnOutputLog?.Invoke(line);
                
            if (Framework.MainFramework == null)
                LogOutput(line);
            else
            {
                lock (ListOutputLines)
                    ListOutputLines.Enqueue(line);
            }
        }

        public static void LogSuccessful(string message)
        {
            if (Enable_SUCCESS)
            {
                DebugLine line = Pool.Get<DebugLine>();
                line.Color = ConsoleColor.Green;
                line.Date = DateTime.Now;
                line.Prefix = "SUCCESS";
                line.Message = message;
                OnOutputLog?.Invoke(line);
                
                if (Framework.MainFramework == null)
                    LogOutput(line);
                else
                {
                    lock (ListOutputLines)
                        ListOutputLines.Enqueue(line);
                }
            }
        }

        public static void LogDebug(string message)
        {
            if (Enable_DEBUG)
            {
                DebugLine line = Pool.Get<DebugLine>();
                line.Color = ConsoleColor.Cyan;
                line.Date = DateTime.Now;
                line.Prefix = "DEBUG";
                line.Message = message;
                OnOutputLog?.Invoke(line);
                
                if (Framework.MainFramework == null)
                    LogOutput(line);
                else
                {
                    lock (ListOutputLines)
                        ListOutputLines.Enqueue(line);
                }
            }
        }

        private static void LogOutput(DebugLine line)
        {
            string endMessage = $"[{line.Date.ToString("HH:mm:ss.ffff")}] [{line.Prefix}]: " + line.Message;
            if (LastConsoleColor != line.Color)
            {
                Console.ForegroundColor = line.Color;
                LastConsoleColor = line.Color;
            }
            Console.WriteLine(endMessage);
            if (HasWriteFile)
                File.AppendAllText(OutputPath, "\n" + endMessage, Encoding.UTF8);
            Pool.Free(ref line);
        }

        static Debug()
        {
            ThreadPool.QueueUserWorkItem(_ =>
            {
                Thread.CurrentThread.Name = "Output-Worker";
                while (Framework.IsWork)
                {
                    if (ListOutputLines.Count > 0)
                    {
                        lock (ListOutputLines)
                        {
                            while (ListOutputLines.Count != 0)
                            {
                                DebugLine line = ListOutputLines.Dequeue();
                                LogOutput(line);
                            }
                        }
                    }
                    else
                        Thread.Sleep(10);
                }
            });
        }
    }
}