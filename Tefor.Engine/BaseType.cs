using System;
using System.Collections.Generic;
using System.Threading;

namespace Tefor.Engine
{
    public abstract class BaseType : IDisposable
    {
        internal static uint LastUID = 0;
        internal BaseTypeMemberInformation Information { get; set; } = null;

        internal float m_internalUpdate = 0f;
        internal float m_internalFixedUpdate = 0f;
        internal float m_internalDraw = 0f;
        internal bool m_destroyed = false;

        public uint BaseTypeUID { get; private set; }
        public List<BaseType> ListChild { get; } = new List<BaseType>();
        public BaseType Parent { get; private set; }
        public Framework FrameworkManager { get; private set; }

        public static T Instantiate<T>(Framework frameworkManager, BaseType parent) where T : BaseType
        {
            if (parent != null && parent.m_destroyed == true)
                throw new Exception("Parent object from instantiating is destroyed!");
            if (Thread.CurrentThread.ManagedThreadId != frameworkManager.WorkerManager.ThreadID)
                throw new Exception("You execute from outher thread!");
            Type type = typeof(T);
            BaseTypeMemberInformation info = null;
            
            lock (BaseTypeMemberInformation.ListMemberInfoOfTypes)
                BaseTypeMemberInformation.ListMemberInfoOfTypes.TryGetValue(type, out info);
            if (info == null)
                info = new BaseTypeMemberInformation(type);
            
            T obj = (T)Activator.CreateInstance(type, true);
            obj.FrameworkManager = frameworkManager;
            obj.Parent = parent;
            obj.Information = info;
            obj.BaseTypeUID = ++LastUID;
            parent?.ListChild.Add(obj);
            if (obj.Information.HasOnAwakeMethod)
                obj.Information.CallMethod(obj, obj.Information.OnAwake);
            frameworkManager.WorkerManager.ListTypeNeedStart.Add(obj);
            return obj;
        }

        public static void Destroy(BaseType type)
        {
            if (Thread.CurrentThread.ManagedThreadId != type.FrameworkManager.WorkerManager.ThreadID)
                throw new Exception("You execute from outher thread!");
            
            if (type.m_destroyed == false)
            {
                type.m_destroyed = true;
                if (type.ListChild.Count != 0)
                {
                    List<BaseType> listDestroying = new List<BaseType>(type.ListChild);
                    for (var i = 0; i < listDestroying.Count; i++)
                        Destroy(listDestroying[i]);
                    listDestroying.Clear();
                }
                if (type.Information.HasOnDestroyMethod)
                    type.Information.CallMethod(type, type.Information.OnDestroy);
                
                type.Parent?.ListChild.Remove(type);
                type.Parent = null;
                type.FrameworkManager.WorkerManager.ListTypeNeedDestroy.Add(type);
            }
        }

        public T AddType<T>() where T : BaseType => Instantiate<T>(this.FrameworkManager, this);

        public T FindTypeOf<T>() where T : BaseType
        {
            if (Thread.CurrentThread.ManagedThreadId != this.FrameworkManager.WorkerManager.ThreadID)
                throw new Exception("You execute from outher thread!");
            
            if (this.ListChild.Count == 0)
                return null;
            Type type = typeof(T);
            for (var i = 0; i < this.ListChild.Count; ++i)
                if (this.ListChild[i].Information.TargetType == type)
                    return (T) this.ListChild[i];
            return null;
        }
        
        public T[] FindTypesOf<T>() where T : BaseType
        {
            if (Thread.CurrentThread.ManagedThreadId != this.FrameworkManager.WorkerManager.ThreadID)
                throw new Exception("You execute from outher thread!");
            
            if (this.ListChild.Count == 0)
                return new T[0];
            Type type = typeof(T);
            List<T> result = new List<T>();
            for (var i = 0; i < this.ListChild.Count; ++i)
                if (this.ListChild[i].Information.TargetType == type)
                    result.Add((T)this.ListChild[i]);
            return result.ToArray();
        }
        
        public T FindTypeOfInChild<T>() where T : BaseType
        {
            if (Thread.CurrentThread.ManagedThreadId != this.FrameworkManager.WorkerManager.ThreadID)
                throw new Exception("You execute from outher thread!");
            if (this.ListChild.Count == 0)
                return null;
            Type type = typeof(T);
            for (var i = 0; i < this.ListChild.Count; ++i)
            {
                if (this.ListChild[i].Information.TargetType == type)
                    return (T) this.ListChild[i];
                T resultInChild = this.ListChild[i].FindTypeOfInChild<T>();
                if (resultInChild != null)
                    return resultInChild;
            }

            return null;
        }
        
        public T[] FindTypesOfInChild<T>() where T : BaseType
        {
            if (Thread.CurrentThread.ManagedThreadId != this.FrameworkManager.WorkerManager.ThreadID)
                throw new Exception("You execute from outher thread!");
            if (this.ListChild.Count == 0)
                return null;
            Type type = typeof(T);
            List<T> result = new List<T>();
            for (var i = 0; i < this.ListChild.Count; ++i)
            {
                if (this.ListChild[i].Information.TargetType == type)
                    result.Add((T) this.ListChild[i]);
                result.AddRange(this.ListChild[i].FindTypesOfInChild<T>());
            }

            return result.ToArray();
        }
        
        public void Dispose()
        {
            if (this.m_destroyed == false)
            {
                GC.SuppressFinalize(this);
                throw new Exception("Not use Dispose in BaseType example!");
            }
        }
    }
}