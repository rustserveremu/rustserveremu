using System;
using System.Collections.Generic;
using System.Reflection;

namespace Tefor.Engine
{
    internal class BaseTypeMemberInformation
    {
        public static Dictionary<Type, BaseTypeMemberInformation> ListMemberInfoOfTypes { get; } = new Dictionary<Type, BaseTypeMemberInformation>();

        public Type TargetType { get; private set; }

        public FastMethodInfo OnAwake { get; private set; }
        public FastMethodInfo OnStart { get; private set; }
        public FastMethodInfo OnUpdate { get; private set; }
        public FastMethodInfo OnFixedUpdate { get; private set; }
        public FastMethodInfo OnDraw { get; private set; }
        public FastMethodInfo OnDestroy { get; private set; }

        public Boolean HasOnAwakeMethod { get; private set; }
        public Boolean HasOnStartMethod { get; private set; }
        public Boolean HasOnUpdateMethod { get; private set; }
        public Boolean HasOnFixedUpdateMethod { get; private set; }
        public Boolean HasOnDrawMethod { get; private set; }
        public Boolean HasOnDestroyMethod { get; private set; }
        
        
        public BaseTypeMemberInformation(Type type)
        {
            this.TargetType = type;
            
            this.OnAwake = this.GetMethod("OnAwake");
            this.HasOnAwakeMethod = this.OnAwake != null;
            
            this.OnStart = this.GetMethod("OnStart");
            this.HasOnStartMethod = this.OnStart != null;
            
            this.OnUpdate = this.GetMethod("OnUpdate");
            this.HasOnUpdateMethod = this.OnUpdate != null;
            
            this.OnFixedUpdate = this.GetMethod("OnFixedUpdate");
            this.HasOnFixedUpdateMethod = this.OnFixedUpdate != null;
            
            this.OnDraw = this.GetMethod("OnDraw");
            this.HasOnDrawMethod = this.OnDraw != null;
            
            this.OnDestroy = this.GetMethod("OnDestroy");
            this.HasOnDestroyMethod = this.OnDestroy != null;
            
            lock(ListMemberInfoOfTypes)
                ListMemberInfoOfTypes.Add(type, this);
        }

        private FastMethodInfo GetMethod(string _methodName)
        {
            MethodInfo mi = null;
            Type currentType = this.TargetType;
            while (mi == null && currentType != null && currentType != typeof(BaseType))
            {
                mi = currentType.GetMethod(_methodName, BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.CreateInstance);
                if (mi == null)
                    currentType = currentType.BaseType;
            }
            
            if (mi != null)
                return new FastMethodInfo(mi);
            return null;
        }

        public void CallOnAwake(BaseType instance)
        {
            try
            {
                this.OnAwake.Invoke(instance);
            }
            catch (Exception ex)
            {
                Debug.LogException(ex);
            }
        }

        public void CallMethod(BaseType instance, FastMethodInfo method, params object[] args)
        {
            try
            {
                method.Invoke(instance, args);
            }
            catch (Exception ex)
            {
                Debug.LogException(ex);
            }
        }
    }
}